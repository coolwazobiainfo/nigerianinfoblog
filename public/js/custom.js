(function() {

    "use strict";

	/* Event: inview;*/
	(function(f) {
		var v, b, e, m = {}, g = document, y = window, w = g.documentElement, i = f.expando;
	    function n() {
	        var t, e, i, n, s = f(), o = 0;
	        if (f.each(m, function(t, e) {
	            var i = e.data.selector
	              , n = e.$element;
	            s = s.add(i ? n.find(i) : n)
	        }),
	        t = s.length)
	            for (v = v || ((n = {
	                height: y.innerHeight,
	                width: y.innerWidth
	            }).height || !(e = g.compatMode) && f.support.boxModel || (n = {
	                height: (i = "CSS1Compat" === e ? w : g.body).clientHeight,
	                width: i.clientWidth
	            }),
	            n),
	            b = b || {
	                top: y.pageYOffset || w.scrollTop || g.body.scrollTop,
	                left: y.pageXOffset || w.scrollLeft || g.body.scrollLeft
	            }; o < t; o++)
	                if (f.contains(w, s[o])) {
	                    var a, r, p, l = f(s[o]), c = l.height(), h = l.width(), d = l.offset(), u = l.data("inview");
	                    if (!b || !v)
	                        return;
	                    d.top + c > b.top && d.top < b.top + v.height && d.left + h > b.left && d.left < b.left + v.width ? (p = (a = b.left > d.left ? "right" : b.left + v.width < d.left + h ? "left" : "both") + "-" + (r = b.top > d.top ? "bottom" : b.top + v.height < d.top + c ? "top" : "both"),
	                    u && u === p || l.data("inview", p).trigger("inview", [!0, a, r])) : u && l.data("inview", !1).trigger("inview", [!1])
	                }
	    }
	    f.event.special.inview = {
	        add: function(t) {
	            m[t.guid + "-" + this[i]] = {
	                data: t,
	                $element: f(this)
	            },
	            e || f.isEmptyObject(m) || (e = setInterval(n, 250))
	        },
	        remove: function(t) {
	            try {
	                delete m[t.guid + "-" + this[i]]
	            } catch (t) {}
	            f.isEmptyObject(m) && (clearInterval(e),
	            e = null)
	        }
	    },
	    f(y).bind("scroll resize scrollstop", function() {
	        v = b = null
	    }),
	    !w.addEventListener && w.attachEvent && w.attachEvent("onfocusin", function() {
	        b = null
	    });
	})(jQuery);

    /* Addon: magnific-popup;*/
    jQuery(function(i) {
        i(document).on("click", ".sppb-magnific-popup", function(t) {
            t.preventDefault();
            var e = i(this);
            e.magnificPopup({
                type: e.data("popup_type"),
                mainClass: e.data("mainclass")
            }).magnificPopup("open")
        })
    });

    (function($) {
        function sppbanimateNumbers(stop, commas, duration, ease) {
            return this.each(function() {
                var $this = $(this),
                    tpl = $this.attr('data-tpl'),
                    reg_c = tpl == 'comma' ? /,/g : / /g,
                    reg_r = tpl == 'comma' ? "$1," : "$1 ";
                var start = parseInt($this.text().replace(reg_c, ""), 10);
                commas = (commas === undefined) ? true : commas;
                commas = tpl == '' ? false : true;

                $({
                    value: start
                }).animate({
                    value: stop
                }, {
                    duration: duration == undefined ? 1000 : duration,
                    easing: ease == undefined ? "swing" : ease,
                    step: function() {
                        $this.text(Math.floor(this.value));
                        if (commas) {
                            $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, reg_r));
                        }
                    },
                    complete: function() {
                        if (parseInt($this.text(), 10) !== stop) {
                            $this.text(stop);
                            if (commas) {
                                $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, reg_r));
                            }
                        }
                    }
                });
            });
        }
        $.fn.sppbanimateNumbers = sppbanimateNumbers;
        $(document).on("inview", ".sppb-animated-number", function(t, e, i, n) {
            var s = $(this);
            e && (s.sppbanimateNumbers(s.data("digit"), !1, s.data("duration")),
                s.unbind("inview"));
        });
    })(jQuery);

    // if sticky header
    $(function() {
        if ($("body.sticky-header").length > 0) {
            var fixedSection = $('#sp-header');
            // sticky nav
            var headerHeight = fixedSection.outerHeight();
            var stickyNavTop = fixedSection.offset().top;
            fixedSection.addClass('animated');
            fixedSection.before('<div className="nav-placeholder"></div>');
            $('.nav-placeholder').height('inherit');
            //add class
            fixedSection.addClass('menu-fixed-out');
            var stickyNav = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > stickyNavTop) {
                    fixedSection.removeClass('menu-fixed-out').addClass('menu-fixed');
                    $('.nav-placeholder').height(headerHeight);
                } else {
                    if (fixedSection.hasClass('menu-fixed')) {
                        fixedSection.removeClass('menu-fixed').addClass('menu-fixed-out');
                        $('.nav-placeholder').height('inherit');
                    }
                }
            };
            stickyNav();
            $(window).scroll(function () {
                stickyNav();
            });
        }
    });

    /* Addon: spp-addon-feature.overlay;*/
    $(function() {
        $( '.sppb-addon-feature.overlay' ).each(function() {
            var e = $( this );
            e.css( 'background-image', 'url(' + e.find( 'img.post-thumb' ).attr( 'src' ) + ')' );
        } );
    } );

    /* Addon: sppb-offcanvas;*/
    jQuery( function( $ ) {
      console.log('fixed');
        // $( 'body' ).addClass( 'off-canvas-menu-init' );
        $('#offcanvas-toggler').on('click', function (event) {
            console.log('click here!');
            event.preventDefault();
            console.log('click here!');
            $('.off-canvas-menu-init').toggleClass('offcanvas');
        });

        $('<div className="offcanvas-overlay"></div>').insertBefore('.offcanvas-menu');

        $('.close-offcanvas, .offcanvas-overlay').on('click', function (event) {
            event.preventDefault();
            $('.off-canvas-menu-init').removeClass('offcanvas');
        });

    } );


    /* Addon: animation;*/
    // jQuery(function() {
    //     WOW && new WOW( { boxClass: 'wow', animateClass: 'animated' } ).init();
    // });

})();













