import React, { Component } from 'react';
import Menu from './menu';
import Footer     from './footer';
import HelmetMeta from './helmet';
import settings from './config';
import axios from "axios";


export default class Posts extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			department: '',
			message: '',
			departments: [
				{ name: 'manager'}, 
				{ name: 'business unit'},
				{ name: 'partnership'},
				{ name: 'media kit'},
				{ name: 'artist'},
			],
			status: '',
			sending: false,
		}
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleChange = (e) => {
		let name = e.target.name;
		let value = e.target.value;
		this.setState( prevState => ({
			...prevState, [name]: value
		}));
	}

	handleSubmit = (e) => {
		e.preventDefault();
		this.setState({sending: true})

		axios
			.post(settings.api_url+'sendmail', {
				email: this.state.email,
				department: this.state.department,
				message: this.state.message,
			})
			.then(response => {
				console.log(response);
				this.setState({
					status: response.data.message,
					sending: false
				});
			})
			.catch(error => {
				console.log(error);
				this.setState({
					sending: false
				});
			});

		// fetch(settings.api_url+'sendmail', {
		// 	method: 'POST',
		// 	headers: { 'Accept': 'application/json' },
		// 	body: JSON.stringify({
		// 	  email: this.state.email,
		// 	  department: this.state.department,
		// 	  message: this.state.message,
		// 	})
		// })
		// .then((response) => response.json())
		// .then((response) => {
		// 	this.setState({
		// 		status: response.message,
		// 		sending: false
		// 	});
		// })
		// .catch((error) => {
		// 	console.log(error);
		// });
	}

	componentDidMount() {
			window.performance.now();
		}

    render() {

		
       return (	
			
			<div>
				<HelmetMeta 
					title="Nigeria Info FM | News, Talk & Sports Station! - Contact Us" 
					description="Nigeria Info FM | News, Talk & Sports Station" 
					canonical={settings.app_url+`contact`}
				/>
				<Menu/>	         
		        
		        <section className="font-sans h-screen w-full bg-cover text-center flex flex-col items-center justify-center" style={{background:'url(/img/3173.jpg) no-repeat center', backgroundSize: 'cover'}}>
				  <h3 className="text-white mx-auto max-w-sm mt-4 font-thin text-2xl leading-normal font-bold text-5xl ">Inquiries</h3>
				</section>
                
                <section className="font-sans text-center py-8 px-4 lg:px-0"> 
                  
					  <div className="container flex flex-col sm:flex-row max-w-xl m-auto border">
					    <div className="w-full sm:w-1/1 px-6 pt-6 text-left flex flex-col justify-center">
					      <div style={{ paddingBottom: '20px' }}>
						  	{this.state.status}
						  </div>
					  	<form onSubmit={this.handleSubmit}>
							<div className="relative border rounded mb-4   appearance-none label-floating">
								<input className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
									id="Email" 
									type="text" 
									name="email" 
									value={this.state.email} 
									onChange={this.handleChange} 
									placeholder="Email"
									/>
								<label className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal" htmlFor="Email">
									Email
								</label>
							</div>

							<div className="relative border rounded mb-4   appearance-none label-floating">
								<select className="w-full py-2 px-3 text-grey-darker leading-normal rounded" 
										id="Department" 
										name="department" 
										value={this.state.department} 
										onChange={this.handleChange}
								>
									<option value="" disabled>Select department</option>
									{this.state.departments.map(department => {
										return (
											<option key={department.name} value={department.name} label={department.name}>{department.name}</option>
										)
									})}
								</select>
								<label className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal" htmlFor="Email">
								</label>
							</div>

							<div className="relative border rounded mb-4   appearance-none label-floating">
								<textarea className="w-full py-2 px-3 h-screen  text-grey-darker leading-normal rounded" 
									id="message" 
									name="message"
									value={this.state.message}
									onChange={this.handleChange} 
									placeholder="Type your message here"
								/>
								<label className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal" htmlFor="message">		     
								</label>
							</div>

							<div className="flex items-center justify-between">
								<button className="bg-black hover:bg-black text-white py-2 px-4 mb-8 rounded-full" type="submit" value="Submit">
									{this.state.sending ? <i style={{paddingLeft: 20, paddingRight: 20}} className="fa fa-circle-o-notch fa-spin"></i>: 'Send message'}
								</button>
							</div>
						</form>
                    </div>
				</div>

				</section>

           
		   </div>

        );
    }
}

