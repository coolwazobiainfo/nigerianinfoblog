import React, { Component } from 'react';
import settings from './config';


class Slider extends Component {
  
  constructor(props) {
           
            super(props);

		    this.state = {
		      shows:     [],
		    };
		    
       }

	  componentDidMount( ) {
      
	    fetch(settings.api_url + 'schedules')
	      .then(response => response.json())
	      .then(data => this.setState({ shows: data }));
	  }


  render() {
    
    const imgstyle = {
    	height : '50px', width : '275px', marginLeft: '28%', marginTop: '15%', fontSize: '25px', color: 'white', fontFamily: 'FuturaBold', textAlign: 'center'

    }
    return (
       
        <div className="bg-grey-lightest p-3">
			<section className="font-sans container max-w-xl m-auto  justify-center  ">

		    <div className="carousel" data-flickity='{ "autoPlay": true, "freeScroll": true, "contain": true,  "wrapAround": true  }'  >
				
			     
			 <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/gmnig.png") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/Good-Morning-Nigeria"> <p style={imgstyle}><h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  The Good Morning Nigeria show</p> </a></div>

			 <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/mdoasis.png") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/Mid-day-oasis"> <p style={imgstyle}><h5 style={{fontFamily: "muli"}}>10:00am - 3:00pm</h5>The Midday Oasis</p> </a></div>
		    
		    <div style={{background: 'url("https://res.cloudinary.com/coolfm/image/upload/v1539705556/The_Truth_webbanner.jpg") no-repeat center',
                    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/after-dark"> <p style={imgstyle}><h5 style={{fontFamily: "muli"}}>07:00pm - 9:00pm on Weekdays</h5>After Dark</p> </a></div>


		     <div style={{background: 'url("https://res.cloudinary.com/coolfm/image/upload/v1539705556/night.png") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/Night"><p style={imgstyle}><h5 style={{fontFamily: "muli"}}>9:00pm - 11:59pm</h5>The Night Cafe</p></a></div>

		     <div style={{background: 'url("https://res.cloudinary.com/coolfm/image/upload/v1539781047/roadddd.jpg") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/The-Road-Show"><p style={imgstyle}><h5 style={{fontFamily: "muli"}}>3:00pm - 8:00pm</h5>The Road Show</p></a></div>
		   
					
			<div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760398/thesantuary.png") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/The-Santuary" ><p style={imgstyle}><h5 style={{fontFamily: "muli"}}>Sundays 6:00am - 1:00pm</h5>The Santuary</p></a></div>	    
		
		<div style={{background: 'url("https://res.cloudinary.com/cool-fm/image/upload/v1542973335/overlo.jpg") no-repeat center',
		    backgroundSize: 'auto'}} className="carousel-cell"><a href="/show/Overnight-Lounge" ><p style={imgstyle}><h5 style={{fontFamily: "muli"}}>Weekdays 1:00am - 5:00am</h5>The Overnight Lounge</p></a></div>	    
		

			</div> 
			</section>
		</div>
       
    //   owl carousel code
    //   <div className="bg-grey-lightest p-3">
    //         <section className="font-sans container max-w-xl m-auto  justify-center  ">
    //             <div class="owl-carousel owl-loaded owl-drag">
    //                 <div class="owl-stage-outer">
    //                     <div class="owl-stage" style="transform: translate3d(-3266px, 0px, 0px); transition: all 0.25s ease 0s; width: 7514px;">
                            
    //                         <div class="owl-item cloned" style="width: 316.667px; margin-right: 10px;">
    //                             <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/gmnig.png") no-repeat center',
    //                                 backgroundSize: 'auto'}} className="carousel-cell" class="item">
    //                                 <a href="/show/Good-Morning-Nigeria"> 
    //                                     <p style={imgstyle}>
    //                                         <h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  
    //                                         The Good Morning Nigeria show
    //                                     </p>
    //                                 </a>
    //                             </div>
    //                         </div>

    //                         <div class="owl-item cloned" style="width: 316.667px; margin-right: 10px;">
    //                             <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/mdoasis.png") no-repeat center',
    //                                 backgroundSize: 'auto'}} className="carousel-cell" class="item">
    //                                 <a href="/show/Good-Morning-Nigeria"> 
    //                                     <p style={imgstyle}>
    //                                         <h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  
    //                                         The Good Morning Nigeria show
    //                                     </p>
    //                                 </a>
    //                             </div>
    //                         </div>

    //                         <div class="owl-item cloned" style="width: 316.667px; margin-right: 10px;">
    //                             <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/night.png") no-repeat center',
    //                                 backgroundSize: 'auto'}} className="carousel-cell" class="item">
    //                                 <a href="/show/Good-Morning-Nigeria"> 
    //                                     <p style={imgstyle}>
    //                                         <h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  
    //                                         The Good Morning Nigeria show
    //                                     </p>
    //                                 </a>
    //                             </div>
    //                         </div>

    //                         <div class="owl-item cloned" style="width: 316.667px; margin-right: 10px;">
    //                             <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/roadshw.png") no-repeat center',
    //                                 backgroundSize: 'auto'}} className="carousel-cell" class="item">
    //                                 <a href="/show/Good-Morning-Nigeria"> 
    //                                     <p style={imgstyle}>
    //                                         <h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  
    //                                         The Good Morning Nigeria show
    //                                     </p>
    //                                 </a>
    //                             </div>
    //                         </div>

    //                         <div class="owl-item cloned" style="width: 316.667px; margin-right: 10px;">
    //                             <div style={{background: 'url("https://res.cloudinary.com/dyfbozvzn/image/upload/v1534760396/thesantuary.png") no-repeat center',
    //                                 backgroundSize: 'auto'}} className="carousel-cell" class="item">
    //                                 <a href="/show/Good-Morning-Nigeria"> 
    //                                     <p style={imgstyle}>
    //                                         <h5 style={{fontFamily: "muli"}}>5:00am - 10:00am</h5>  
    //                                         The Good Morning Nigeria show
    //                                     </p>
    //                                 </a>
    //                             </div>
    //                         </div>
    //                     </div>
    //                 </div>
    //             </div>
    //         </section>
    //     </div>

    );
  }
}

export default Slider;



