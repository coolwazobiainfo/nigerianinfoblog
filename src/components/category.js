import React, { Component } from 'react';

import Menu from './menu';
import Newsletter from './newsletter';
import Mobileswitch from './mobileswitch';
import Footer from './footer';
import moment from 'moment';
import './catbg.css';
import settings from './config';
import HelmetMeta from './helmet';
import { slug } from './../helpers';
import  CategoryPreloader  from './preloaders/categoryloader';

export default class category extends Component {


	constructor(props) {

		super(props);

		this.state = {
			cats: [],
			visible: 12,
			catname: '',
			loading: true
		};

		this.loadMore = this.loadMore.bind(this);
	}

	loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 4 };
		});
	}


	componentDidMount() {
			
		window.performance.now();
		const { id } = this.props.match.params;
		//cats is actually posts
		fetch(settings.api_url + 'category/' + id)
			.then(response => response.json())
			.then(data => this.setState({ cats: data, catname: id, loading: false }));
	}


	render() {

		const imgg = this.state.cats[0] ? this.state.cats[0].image : "";

		const posted_at = this.state.cats[0] ? this.state.cats[0].created_at : "";

		const timme = moment(posted_at).fromNow();
		const category_name = slug(this.state.catname,'-',' ');

		const colorCode = this.state.catname.toLowerCase() == 'entertainment' ? '#e6007e' : this.state.catname.toLowerCase() === 'sports' ? '#fcea0e' : this.state.catname.toLowerCase() === 'business' ? '#f26d00' : '#652483';
		const textColor = this.state.catname.toLowerCase() == 'sports' ? 'black' : 'white';

	return (

			<div className="body-wrapper">
				<div className="body-innerwrapper">

				<HelmetMeta 
					title={'Nigeria Info FM | ' + category_name}
					description="Nigeria Info FM | News, Talk & Sports Station" 
					canonical={settings.app_url+'category/'+category_name}
				/>

			<Menu /> 

 
		 <div id="latest" className="sppb-section">
			<div className="container">
				<div className="sppb-section-title">
					<h4 style={{ background: `${colorCode}`, textAlign: 'center', color: `${textColor}`, borderRadius: '4px 1px 11px 0px', paddingLeft: '.8em' }}><strong>{this.state.catname}</strong> </h4>
					<p className="sppb-title-subheading"><a href="#">View all</a></p>
				</div>
				
			{this.state.loading ?

				<CategoryPreloader /> :

				<div className="row">
                    <div className="col-md-full">

				{ this.state.cats.slice(0, this.state.visible).map((post, index) => 
                       
					<div className="col-md-3 col-sm-6" key={index}>
						<div className="sppb-addon-feature mb30-sm">
						<a href={`post/${post.slug}`}>

							<img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt="" className="img-responsive post-thumb" />
						</a>
				 			<div className="sppb-addon-content">
								<a href={`/post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
								<div className="meta">									
									<span className="date">{moment(post.created_at).fromNow()}</span>
								</div>
							</div>
						</div>
					</div>
			
			   )}
			   		</div>
				</div>  

			}
			</div>				
		</div>
	</div>
</div>

		);
	}
}