import React, { Component } from 'react';
import {
  isMobile
} from "react-device-detect";

import './adstyle.css';

export default class Mobileswitch extends Component {

	   constructor(){
        super();

        this.state = {
           display: true
        }
    }
    

    changedisplay(){
       this.setState({display: !this.state.display})
    }

     renderContent = () => {
     	let should_display = this.state.display ? "block" : "none";

	    if (isMobile) {
	        return <div className="go-popup ng-scope" ng-hide="dismissGoPopup" style={{display:should_display,width: "90%",margin: "0 auto"}}>
<div className="go-popup-content animated fadeInUp" style={{
    width: "95%",
    marginBottom: "1em",
    marginLeft: ".5em",
    border: "solid green 3px",
    WebkitBoxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
    MozBoxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
    boxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
    border: 'solid 2px #00A365'
}}>
<div className="go-popup-icon"><i className="ion ion-ios-phone-portrait" style={{opacity: "0.4",fontSize: "1.3em"}}></i>
<button onClick={this.changedisplay.bind(this)} className=" ion-ios-close-circle-outline" style={{fontSize: "1.3em",float: "right", color: "green"}}></button></div>
<p className="mt-4 text-black" style={{lineHeight: "1.3",fontSsize: "1em", fontFamily: 'FreightSansProMedium-Regular'}}>
Discover a spectrum of exciting content on the move with our apps. Available on iOS and Android </p>
<a href="https://radio.nigeriainfo.fm" target="new" className="shadow text-white mt-4 p-2 bg-green" style={{float: "left", fontFamily: 'FreightSansProMedium-Regular'}}>
<span  className="ion ion-ios-play  "> </span> iPlayer</a>
<a href="https://nigeriainfo.fm" className="shadow text-green mt-4 p-2" style={{float: "right", paddingRight: "1em", fontFamily: 'FreightSansProMedium-Regular'}}>
<span className="ion ion-ios-download .font-lora"></span> Download</a></div></div>

	    }
	    return <div>  </div>
	 }
     
     render() {
      return this.renderContent();
     }

  }

  
