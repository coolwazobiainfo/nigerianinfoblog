import React, { Component } from 'react';
import './../App.css';

export default class NotFound extends Component {

     componentDidMount() {
      window.performance.now();
    }
    
    render() {

        return (


            <div id="notfound">
                <div className="text-center">
                    <img className="notfound-image" width="250px" src="/img/music-player.png" alt="headphone" />
                    <h2 id="notfound-message">Sorry we couldn't find that page</h2>
                    <a href="/"><span className="go-home"> Go Home </span></a>
                </div>
            </div>

        );
    }
}

