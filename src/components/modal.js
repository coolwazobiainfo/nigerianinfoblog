import React, { Component } from 'react';
import './style.css'
class Modal extends Component {

componentDidMount() {
            window.performance.now();
        }
	render() {

    return (
        <div className="modal" style={{display: this.props.display}}>
            <div className="go-popup ng-scope" ng-hide="dismissGoPopup" style={{display: 'flex', alignSelf: 'center', justifyContent: 'center', alignItems: 'center', width: '50%'}}>
                <div className="go-popup-content animated fadeInUp" style={{
                    marginBottom: "1em",
                    marginLeft: ".5em",
                    position: 'relative',
                    border: "3px solid #1250bb",
                    // webkitBoxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
                    // mozBoxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
                    boxShadow: "10px 10px 5px 0px rgba(0,0,0,0.2)",
                }}>
                <div className="go-popup-icon">
                    
                    <button onClick={this.props.closeModal} className=" ion-ios-close-circle-outline" style={{fontSize: "1.3em",float: "right", color: "rgb(18, 80, 187)"}}></button></div>
                    <p className="mt-4 text-grey-darkest" style={{lineHeight: "1.3",fontSsize: "1em"}}>
                        Switch Location to {this.props.location}? Your selection will be remembered.
                    </p>
                    <br></br>
                    <a href="#yes" onClick={this.props.changeLocation} className="shadow text-white mt-4 p-2 bg-blue" style={{float: "left"}}>
                        Yes
                    </a>
                    <a href="#no" onClick={this.props.abort} className="shadow text-grey-darker mt-4 p-2" style={{float: "right", padding: 10}}>
                    No</a>
                    </div>
			</div>
        </div>
    	)

	}

}

export default Modal;