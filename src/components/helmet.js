import React, { Component } from 'react';
import { Helmet } from 'react-helmet';



class HelmetMeta extends Component {


  render() {
      
    return (
      <div> 
        <Helmet>
          
          	<title>{this.props.title}</title>
            <meta name="keywords" content="music, videos, blogs, writing, latest-news, radio, entertainment, best-music, aux, best talk, celebrity-interviews, coolfm, hitmusic, news, breaking-news etc." />
  			   <meta name="description" content={this.props.description ? this.props.description : "Nigeria Info FM | News, Talk & Sports Station" } />
  			   <meta name="og:title" property="og:title" content={this.props.title ? this.props.title : "Nigeria Info FM | News, Talk & Sports Station" } />
           <meta property="og:image" content={this.props.image ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.props.image : "https://res.cloudinary.com/xyluz/image/upload/v1550096120/nir_whyowr.jpg" } />
  			   <meta name="robots" content="index, follow" />      
          	<meta name="theme-color" content="#008f68" />
            <link rel="canonical" href={this.props.canonical} />
        </Helmet>
        
      </div>
    );
  }
}

export default HelmetMeta;