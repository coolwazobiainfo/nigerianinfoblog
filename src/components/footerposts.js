import React, { Component } from 'react';



export default class Footerposts extends Component {

	 componentDidMount() {
      window.performance.now();
    }
    
    render() {

        return (
            <div>
			   <section className="bg-more py-4 font-sans mt-8 ">
					 <div className="container max-w-xl m-auto flex flex-wrap items-center justify-start">

					    <div className="w-full md:w-1/2 lg:w-1/4 flex flex-col mb-8 px-3">
					      <div className="overflow-hidden bg-black rounded-lg border-grey-darkest border-2 hover:shadow-raised hover:translateY-2px transition">
					        <img className="w-full" src="https://res.cloudinary.com/dyfbozvzn/image/upload/v1534759572/1.jpg" alt="Sunset in the mountains"/>
					        <div className="p-4 flex flex-col justify-between ">
<p className="inline-flex items-center mb-1"><a className="cate-sr"><span className="mdi-podcast" style={{fontSize: '1.2px', display: 'block'}}></span> GO DIGITAL </a></p>

					          <h3 className="font-semibold text-white mb-4 leading-normal">Take advantage of our digital community of over 7 million fans.</h3>
					         
					        </div>
					      </div>
					    </div>

					    

					     <div className="w-full md:w-1/2 lg:w-1/4 flex flex-col mb-8 px-3">
					      <div className="overflow-hidden bg-black rounded-lg border-grey-darkest border-2 hover:shadow-raised hover:translateY-2px transition">
					        <img className="w-full" src="https://res.cloudinary.com/dyfbozvzn/image/upload/v1534759588/5.jpg" alt="Sunset in the mountains"/>
					        <div className="p-4 flex flex-col justify-between ">
<p className="inline-flex items-center mb-1"><a  className="cate-sr"><span className="mdi-podcast" style={{fontSize: '1.2px', display: 'block'}}></span>AUX</a></p>
					          <h3 className="font-semibold text-white mb-4 leading-normal">Lets connect you to an endless stream of music with unique sounds.</h3>
					         
					        </div>
					      </div>
					    </div>

					     <div className="w-full md:w-1/2 lg:w-1/4 flex flex-col mb-8 px-3">
					      <div className="overflow-hidden bg-black rounded-lg border-grey-darkest border-2 hover:shadow-raised hover:translateY-2px transition">
					        <img className="w-full" src="https://res.cloudinary.com/dyfbozvzn/image/upload/v1534759563/4.jpg" alt="Sunset in the mountains"/>
					        <div className="p-4 flex flex-col justify-between ">
<p className="inline-flex items-center mb-1"><a  className="cate-sr"><span className="mdi-podcast" style={{fontSize: '1.2px', display: 'block'}}></span> COOL TICKETS </a></p>
					          <h3 className="font-semibold text-white mb-4 leading-normal">Discover tickets to your favourite events at affordable prices.</h3>
					         
					        </div>
					      </div>
					    </div>
                        

                        <div className="w-full md:w-1/2 lg:w-1/4 flex flex-col mb-8 px-3">
					      <div className="overflow-hidden bg-black rounded-lg border-grey-darkest border-2 hover:shadow-raised hover:translateY-2px transition">
					        <img className="w-full" src="https://res.cloudinary.com/dyfbozvzn/image/upload/v1534759557/3.jpg" alt="Sunset in the mountains"/>
					        <div className="p-4 flex flex-col justify-between ">
<p className="inline-flex items-center mb-1"><a  className="cate-sr"><span className="mdi-podcast" style={{fontSize: '1.2px', display: 'block'}}></span> GO DIGITAL </a></p>
					          <h3 className="font-semibold text-white mb-4 leading-normal">We make music magic, we dream it and then do it together.</h3>
					         
					        </div>
					      </div>
					    </div>
					    
					 </div>
				 </section>
           </div>
        );
    }
}



