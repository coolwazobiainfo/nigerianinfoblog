import React, { Component } from 'react';

import Menu from './menu';
import Newsletter from './newsletter';
import Mobileswitch from './mobileswitch';
import Footer from './footer';
import moment from 'moment';
import './catbg.css';
import settings from './config';
import HelmetMeta from './helmet';
import { slug } from './../helpers';
import  CategoryPreloader  from './preloaders/categoryloader';

export default class local extends Component {


	constructor(props) {

		super(props);

		this.state = {
			cats: [],
			visible: 12,
			catname: '',
			loading: true
		};

		this.loadMore = this.loadMore.bind(this);
	}

	loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 4 };
		});
	}


	componentDidMount() {
			
		window.performance.now();
		const { id } = this.props.match.params;
		//cats is actually posts
		fetch(settings.api_url + 'group/local')
			.then(response => response.json())
			.then(data => this.setState({ cats: data, catname: id, loading: false }));
	}


	render() {

		const imgg = this.state.cats[0] ? this.state.cats[0].image : "";

		const posted_at = this.state.cats[0] ? this.state.cats[0].created_at : "";

		const timme = moment(posted_at).fromNow();
		const category_name = slug(this.state.catname,'-',' ');

		return (

			<div>
				<HelmetMeta 
					title={'Nigeria Info FM | ' + category_name}
					description="Nigeria Info FM | News, Talk & Sports Station" 
					canonical={settings.app_url+'shows/'+category_name}
				/>

				<Menu />

				<div className="bg-grey-lightest  text-center">
					<p className="inline-flex font-bold text-sm  uppercase">
						<span style={{ backgroundColor: "#FFD105", borderRadius: "0px 0px 13px 13px", zIndex: "2", border: "none", marginTop: '69px', color:'black', lineHeight: '1.2em', padding: '0.5em' }} className="font-bold tracking-wide uppercase ">
							{category_name}
						</span>
					</p>
				</div>

				<div style={{ height: "auto", marginTop: "-2em" }} className="font-sans max-100 m-auto flex flex-col md:flex-row sm:items-center">

					<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + imgg}) 0% 0% / cover`, height: "700px", boxShadow: "inset 0px 42px 13px 354px #2f2f2f8a", margin: "0 auto", backgroundSize: "cover", backgroundPosition: "center" }} className="w-full text-center flex flex-col justify-center items-start px-6 py-0 md:py-8 md:px-8 lg:items-start">

					{/*	<label style={{ opacity: ".8", textAlign: "center" }} htmlFor="tagline" className="uppercase text-center mx-auto tracking-wide  text-white font-bold"> {category_name}
						</label> */}
						<h1 className="mt-2 mb-4 uppercase tracking-wid  topik m-auto text-center text-white font-druk" style={{fontFamily: "Termina-Demi"}}> {this.state.cats[0] ? this.state.cats[0].title : "category"}   </h1>
						<p className="inline-flex text-white font-normal text-lg text-center mx-auto time " >
							<span style={{fontFamily: 'FreightSansProMedium-Regular'}}>
								<span className="ion ion-md-time"></span> {timme}
							</span>
						</p>

					</div>
				</div>
 

				<div className="bg-grey-lightest ">
					<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center my-8">
						<div className="w-full  max-100 m-auto mb-6 lg:mb-0">
							<p className="text-center uppercase p-2 mt-2 text-xl  text-black font-druk tracking-wid mb-4" style={{fontFamily: "Termina-Demi"}}>  all posts</p>


							<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">

							{this.state.loading ?
                        		<CategoryPreloader /> :

								this.state.cats.slice(0, this.state.visible).map((cat, index) =>


									<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
										<div style={{ height: "620px" }} className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
											<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + cat.image})`, height: "350px", backgroundSize: "cover" }}></div>

											<div className="p-6 flex flex-col justify-between">
												<p className="inline-flex text-blue font-bold text-sm mb-4 uppercase" style={{ marginTop: "-7em" }}>
													
													{!cat.review_score ?
														""
														: <span className="font-bold text-gren but-rw ">{cat.review_score}</span>
													}
												</p>

												<div className="bg-white" style={{ padding: "1em", borderRadius: "7px" }}>
													<h3 className="text-black uppercase text-xl mb-2 font-bold leading-normal mb-4"><a style={{ color: "black" }} href={"/post/" + cat.slug}>{cat.title}</a></h3>
													<p className="text-grey-darker text-m mb-2 font-lora leading-normal">{cat.excerpt}</p>
												</div>
											</div>
										</div>
									</div>

								)}

							</div>
						</div>
					</section>
				</div>

				<div className="text-center">
					<div className="m-auto mb-8">
						{this.state.visible < this.state.cats.length &&
							<button onClick={this.loadMore} className="bg-black p-3 rounded-lg  w-100 font-bold  text-white uppercase tracking-wide">Load More</button>
						}
					</div>
				</div>
				<Mobileswitch />
				<Newsletter />
				<Footer />
			</div>

		);
	}
}