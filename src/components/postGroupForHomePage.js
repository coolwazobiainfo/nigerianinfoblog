import React, { Component } from 'react';
import Ad from './ad';
import ReactGA from 'react-ga';
import Homemobileswitch from './homemobileswitch';
import settings from './config';
import  PostPreloader from './preloaders/postloader';
import moment       from 'moment';

export default class PostGroupForHomePage extends Component {

	constructor(props) {

		super(props);

		this.state = {
			l_loading: true,
			s_loading: true,
			f_loading: true,
			l_visible: 6,
			s_visible: 6,
			f_visible: 6,
			localPosts: [],
			sportPosts: [],
			foreignPosts: []
		};

		fetch(settings.api_url+'group/local')
			.then(response => response.json())
			.then(data => this.setState({ localPosts: data, l_loading:false }));

		fetch(settings.api_url+'group/sport')
			.then(response => response.json())
			.then(data => this.setState({ sportPosts: data, s_loading:false }));

		fetch(settings.api_url+'group/foreign')
			.then(response => response.json())
			.then(data => this.setState({ foreignPosts: data, f_loading:false }));

		// this.loadMore = this.loadMore.bind(this);
	}

	// loadMore() {
	// 	this.setState((prev) => {
	// 		return { visible: prev.visible + 3 };
	// 	});
	// }


	componentDidMount() {

			window.performance.now();
		
		
	}


	handleClick() {

		ReactGA.initialize('UA-103305032-1');

		ReactGA.pageview(window.location.pathname);

		ReactGA.event({
			category: 'Navigation',
			action: 'Clicked Link',
		});
	}


	render() {

		

		return (

			<div style={{ margin: "0 auto" }} className="bg-white">
				<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center">

					<div className="w-full  max-100 m-auto mb-4 lg:mb-0"> 

						<p className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2" style={{fontFamily: 'Termina-Demi' }}>
						<a href="local" className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2 cattt-title" style={{fontFamily: 'Termina-Demi' }}>LOCAL  NEWS</a>
						</p>
					
						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.l_loading && this.state.localPosts ? 
								
								<PostPreloader />
								

							: this.state.localPosts.slice(0, this.state.l_visible).map((post, index) =>

								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-s" key={index} style={{height:'650px'}}>
									<div className="overflow-hidden bg-white news-bottom shadow hover:shadow-raised hover:translateY-2px transition" style={{height:'650px'}}>
										<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}) 0% 0% / cover`, height: "300px" }}></div>

										 
										<div className="p-6 flex flex-col justify-between">

										 <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
                                                                    <span className="font-sans text-green uppercase"> <a className='font-sans text-green' href={"shows/" + post.category.name.toLowerCase().replace(/ /g, "-")}>{post.category.name}</a></span> 
                                                                  </p>

												<h3 className="text-black  text-lg mb-2 font-bold leading-normal mb-4"><a className="text-black uppercase" href={"post/" + (post.slug == null ? post.id : post.slug)}>{post.title}</a></h3>
												<p style={{ fontSize: "1em" }} className="text-grey-darker text-m mb-2 font-lora leading-normal">{post.excerpt}</p>

												<p className="text-black font-normal text-lg  time mt-2">
                                                                            <span style={{opacity: '.4', fontFamily: 'FreightSansProMedium-Regular'}} >
                                                                              <span  className="ion ion-md-time"></span> {moment(post.created_at).fromNow()}
                                                                                              
                                                                            </span>
                                                                          </p>
											
										</div>
									</div>
								</div>


							)} 
						
						
					
				</div>
			
					

				<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center">
					<div className="w-full  max-100 m-auto mb-4 lg:mb-0">

						<p className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2 cattt-title " style={{fontFamily: 'Termina-Demi' }}>FOREIGN  NEWS</p>
					

						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.f_loading && this.state.foreignPosts ? 
								
								<PostPreloader />
								

							: this.state.foreignPosts.slice(0, this.state.f_visible).map((post, index) =>


								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-s" key={index} style={{height:'650px'}}>
									<div className="overflow-hidden bg-white news-bottom shadow hover:shadow-raised hover:translateY-2px transition" style={{height:'650px'}}>
										<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}) 0% 0% / cover`, height: "300px" }}></div>

										<div className="p-6 flex flex-col justify-between">
										 <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
                                                                    <span className="font-sans text-green uppercase">  <a className='font-sans text-green' href={"shows/" + post.category.name.toLowerCase().replace(/ /g, "-")}>{post.category.name}</a></span> 
                                                                  </p>

										

											
												<h3 className="text-black  text-lg mb-2 font-bold leading-normal mb-4"><a className="text-black uppercase" href={"post/" + (post.slug == null ? post.id : post.slug)}>{post.title}</a></h3>
												<p style={{ fontSize: "1em" }} className="text-grey-darker text-m mb-2 font-lora leading-normal">{post.excerpt}</p>

												<p className="text-black font-normal text-lg  time mt-2">
                                                                            <span style={{opacity: '.4', fontFamily: 'FreightSansProMedium-Regular' }} >
                                                                              <span  className="ion ion-md-time"></span>  {moment(post.created_at).fromNow()}
                                                                                              
                                                                            </span>
                                                                          </p>
											
										</div>
									</div>
								</div>
							)}
						
						
					</div>
				</div>
</section>
<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center">
					<div className="w-full  max-100 m-auto mb-4 lg:mb-0">

						<p className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2 cattt-title-red " style={{fontFamily: 'Termina-Demi'}} >SPORTS</p>
					

						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.s_loading && this.state.sportPosts ? 
								
								<PostPreloader />
								

							: this.state.sportPosts.slice(0, this.state.s_visible).map((post, index) =>


								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-s" key={index} style={{height:'650px'}}>
									<div className="overflow-hidden bg-white news-bottom shadow hover:shadow-raised hover:translateY-2px transition" style={{height:'650px'}}>
										<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}) 0% 0% / cover`, height: "300px"}}></div>

										<div className="p-6 flex flex-col justify-between">
										 <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-4 uppercase  " >
                                                                    <span className="font-sans text-red uppercase">

                                                                    <a className='font-sans' style={{color:'red'}} href={"shows/" + post.category.name.toLowerCase().replace(/ /g, "-")}>{post.category.name}</a>

                                                                  

                                                                     </span> 
                                                                  </p>

											

											
												<h3 className="text-black  text-lg mb-2 font-bold leading-normal mb-4"><a className="text-black uppercase" href={"post/" + (post.slug == null ? post.id : post.slug)}>{post.title}</a></h3>
												<p style={{ fontSize: "1em" }} className="text-grey-darker text-m mb-2 font-lora leading-normal">{post.excerpt}</p>

												<p className="text-black font-normal text-lg  time mt-2">
                                                                            <span style={{opacity: '.4', fontFamily: 'FreightSansProMedium-Regular'}} >
                                                                              <span  className="ion ion-md-time"></span>  {moment(post.created_at).fromNow()}
                                                                                              
                                                                            </span>
                                                                          </p>
											
										</div>
									</div>
								</div>
							)}
						
						
					</div>

				</div>

				</section>
				</div>
				<Ad />
			</section>	
				
				<Homemobileswitch />
			</div>
		);
	}
}

