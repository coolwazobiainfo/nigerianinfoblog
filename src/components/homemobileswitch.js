import React, { Component } from 'react';
import {
  isMobile
} from "react-device-detect";

import './adstyle.css';

export default class Homemobileswitch extends Component {

	   constructor(){
        super();

        this.state = {
           display: true
        }
    }

    changedisplay(){
       this.setState({display: !this.state.display})
    }

     renderContent = () => {
     	let should_display = this.state.display ? "block" : "none";

	    if (isMobile) {
	        return <div style={{
                    textAlign: 'center',
                    background: '#84848408',
                    marginBottom: '1em',
                    padding: '.5em',
                    color: 'black', 
                    display: should_display }}> <a href="#"><span style={{
                    textTransform: 'capitalize', fontFamily: 'Archivo,sans-serif',    color: '#e80000',
                    textDecoration: 'solid', paddingRight: '.7em',    borderRight: '1px solid #bcbdbd' }}>
                      <span className="ion ion-ios-volume-high"></span> Listen Now</span></a> <span style={{
                    textTransform: 'capitalize',     fontFamily: 'Archivo,sans-serif',  marginLeft: '.5em'}}>Morning Crossfire</span>  </div>

	    }
	    return <div>  </div>
	 }
     
     render() {
      return this.renderContent();
     }

  }

  
