import React, { Component } from 'react';


export default class Footer extends Component {
    render() {
        return (
            <div className="footer">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 col-sm-6 wow zoomIn">
                                <h4 className="title">Popular Categories</h4>
                                <div className="row">
                                    <div className="col-md-6 col-sm-6 col-xs-6">
                                        <ul className="sppb-menu">
                                            <li><a href="https://themeforest.net/user/saihoai">Features News</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">World News</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Fashion Style</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Entertainment</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Sport News</a></li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6 col-sm-6 col-xs-6">
                                        <ul className="sppb-menu">
                                            <li><a href="https://themeforest.net/user/saihoai">Tecth Reviews</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Trending</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Business</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Politics</a></li>
                                            <li><a href="https://themeforest.net/user/saihoai">Markets</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 col-sm-6 mb30-sm wow zoomIn" data-wow-delay=".1s">
                                
                                <div className="border text-center">
                                    <a href="#" className="logo">
                                        <img src="" alt="" />
                                       
                                    </a>

                                    <div className="sppb-addon-tags">
                                        <a href="#">About Us</a>
                                        <a href="#">Contact Us</a>
                                        <a href="#">Privacy Policy</a>
                                        <a href="#">Ad Choice</a>
                                        <a href="#">Terms of Use</a>
                                        <a href="#">Help Center</a>
                                    </div>

                                    <div className="sppb-addon-social">
                                        <a href="#"><i className="zmdi zmdi-facebook"></i></a>
                                        <a href="#"><i className="zmdi zmdi-twitter"></i></a>
                                        <a href="#"><i className="zmdi zmdi-instagram"></i></a>
                                        <a href="#"><i className="zmdi zmdi-pinterest"></i></a>
                                        <a href="#"><i className="zmdi zmdi-youtube"></i></a>
                                    </div>

                                    <div className="copyright">&copy; Copyright 2019, Nigeria Info FM</div>
                                </div>

                            </div>
                            <div className="col-md-4 text-center wow zoomIn" data-wow-delay=".2s">
                                <h4 className="title">Newsletter</h4>
                                <p className="subtitle">Subscribe my Newsletter for new blog posts,<br />tips. Let's stay updated!</p>
                                <form className="sppb-optin-form">
                                    <div className="sppb-form-group email-wrap">
                                        <input type="email" name="email" className="form-control" placeholder="Email" required="required" />
                                    </div>
                                    <div className="button-wrap">
                                        <button type="submit" className="btn btn-primary btn-block"><i className="fa"></i> Subscribe</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

        );
    }
}
