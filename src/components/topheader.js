import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import './style.css';
import settings from './config';

class Topheader extends Component {
  
   constructor (props) {
     super(props);

     this.state = {
        schedule: '',
        later: '',
        location: localStorage.getItem('location'), 
    }
     
     this.openradio = this.openradio.bind(this);
   }

 
  openradio = (e) =>  {
    e.preventDefault();
    window.open ("https://radio.nigeriainfo.fm","mywindow","menubar=1,resizable=1,width=550,height=950");
  }

  between = (start, end) => {
        let format = 'hh:mm:ss'
        // var time = moment() gives you current time. no format required.
        let time = moment(),
            beforeTime = moment(start, format),
            afterTime = moment(end, format);
  
        if (time.isBetween(beforeTime, afterTime)) {
          return 'NOW';
        } else if(beforeTime.isAfter(time)) {
  
          return 'Later';
  
        }else{
          return 'Ended';
        }
  }

  now_playing = (schedule) => {

    if (schedule[`${this.state.location}`]) {
            let myDate = new Date();
            let schedule_arry = schedule[`${this.state.location}`];
            if (myDate.getDay() === 6) {
              //saturday
              //filter by saturday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "saturday";
              });
    
            } else if (myDate.getDay() === 0) {
              //sunday
              //filter by sunday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "sunday";
              });
    
            } else {
              //otherdays
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period !== "sunday" && sch.period !== "saturday";
              });
            }
            for (let i = 0; i < schedule_arry.length; i++) {
              let schedu = schedule_arry[i];
              if (this.between(schedu.start, schedu.end) === "NOW") {
                return schedu;
              }
            }
         }
  }

    next = (schedule) => {

    if (schedule[`${this.state.location}`]) {
           
            let schedule_arry = schedule[`${this.state.location}`];

            
            for (let i = 0; i < schedule_arry.length; i++) {
              let schedu = schedule_arry[i];
              if (this.between(schedu.start, schedu.end) === "Later") {
               
                return schedu;
              }
            }
         }
  }
   componentDidMount () {


      window.performance.now();


      fetch(settings.api_url + 'schedules') 
        .then(response => response.json())
        .then(data => { let sch = _.groupBy(data, function(city) {
              return city.state;
            }); this.setState({ schedule: this.now_playing(sch) }); });  

        fetch(settings.api_url + 'schedules')
        .then(response => response.json())
        .then(data => { let sch = _.groupBy(data, function(city) {
          // console.log('--->sch--->',sch);
              return city.state;
            }); this.setState({ later: this.next(sch) }); });  


   }

  render() {
    

    return (

      <div className="" style={{width: '100%'}}>


      <div className="flex items-start w-1/1">
      <div style={{backgroundImage:`url(${"https://res.cloudinary.com/xyluz/image/upload/v1550096120/nir_whyowr.jpg"})`,width: '100%', backgroundSize: 'cover',  backgroundPosition: 'center', height: '250px'}} className="overflow-hidden bg-white shadow hover:shadow-raised hover:translateY-2px transition ">

        <div className="p-6 flex flex-col justify-between ">
          <p className="inline-flex items-center"></p>
          <p style={{textAlign: 'center', marginBottom: '1em', fontSize: '0.8em', paddingTop: '30%'}} >
              <span style={{marginTop: '2em'}} className="clock text-center tracking-wide  text-white font-orb text-3xl"></span>
          </p>
         
       
          
        </div>
      </div>
    </div>
 
    <div style={{height: 'auto' }} className="bg-white p-6  shadow">
        <p style={{textAlign: 'left', marginBottom: '1em', fontSize: '.85em'}} >
         <label  htmlFor="tagline" className="uppercase  mx-auto  text-white font-bold on-air">
                <span className="mdi-radio-tower"></span> NOW ON AIR 
                                
              </label>
        </p>
        <h3 style={{fontSize: '1.5em', lineHeight: '1.3', fontFamily: 'Termina-Demi'}}   className="text-black font-druk text-xl mb-2  mt-2 uppercase  ">{this.state.schedule ? this.state.schedule.title : "No Show"}  </h3>

         <p className="inline-flex text-grey-dark font-sans text-lg ">
                        <span> {this.state.schedule ? this.state.schedule.description : "--"} </span>
          </p> 

          <p className=" text-black font-normal text-lg  time mt-2 " style={{fontFamily: 'FreightSansProMedium-Regular'}}>
            <span style={{fontFamily: 'FreightSansProMedium-Regular'}}>
              
              <span className="ion ion-md-time" style={{paddingRight: '10px'}}></span>{this.state.schedule ? this.state.schedule.start : '0.00' } - {this.state.schedule?this.state.schedule.end : '0.00'}   
                              
            </span>
          </p>

             <p onClick={this.openradio} style={{cursor: 'pointer'}} className="list-em text-white font-bold text-lg text-left   h-auto mt-6 text-center ">
              <span>
                <span className="ion-md-play"> LISTEN </span>
              </span>
            </p>
          
        </div>

        </div>
   
    );
  }
}

export default Topheader;

  
    