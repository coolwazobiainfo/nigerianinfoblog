import React from "react";
import ContentLoader from "react-content-loader";

const CategoryPreloader = props => (
	<div style={{width: '100%'}}>

		 <ContentLoader>
		    <rect x="20" y="0" rx="0" ry="0" width="80" height="100" />
		    <rect x="110" y="0" rx="0" ry="0" width="80" height="100" />
		    <rect x="200" y="0" rx="0" ry="0" width="80" height="100" />
		    <rect x="290" y="0" rx="0" ry="0" width="80" height="100" />
  		</ContentLoader>
	</div>
)


export default CategoryPreloader;