import React from "react"
import ContentLoader from "react-content-loader";

const HeroPreloader = props => (
	<div style={{width: '100%'}}>

		 <ContentLoader>

		    <rect x="0" y="0" rx="0" ry="0" width="100%" height="100%" />
		   
  		</ContentLoader>
  		
	</div>
)


export default HeroPreloader;