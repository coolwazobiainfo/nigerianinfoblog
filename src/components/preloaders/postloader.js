import React from "react"
import ContentLoader from "react-content-loader";

const PostPreloader = props => (
	<div style={{width: '100%', paddingBottom: '60px', paddingTop: '40px'}}>

		 <ContentLoader>
		    <rect x="10" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="140" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="270" y="0" rx="0" ry="0" width="120" height="150" />
		 
  		</ContentLoader>
  		<ContentLoader>
		    <rect x="10" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="140" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="270" y="0" rx="0" ry="0" width="120" height="150" />
		 
  		</ContentLoader>
  		<ContentLoader>
		    <rect x="10" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="140" y="0" rx="0" ry="0" width="120" height="150" />
		    <rect x="270" y="0" rx="0" ry="0" width="120" height="150" />
		 
  		</ContentLoader>
	</div>
)


export default PostPreloader;