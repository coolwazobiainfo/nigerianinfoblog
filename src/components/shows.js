import React, { Component } from 'react';
import Menu from './menu';
import Interviews from './interviews';
import settings from './config';
import HelmetMeta from './helmet';


import Footer from './footer';

class Shows extends Component {

componentDidMount() {
      window.performance.now();
    }

  render() {
    return (
      <div>
        <HelmetMeta 
          title="Nigeria Info FM | News, Talk & Sports Station!" 
          description="Nigeria Info FM | News, Talk & Sports Station" 
          canonical={settings.app_url}
        />
        <Menu />
        <Interviews />
        <Footer />
      </div>
    );
  }
}

export default Shows;
