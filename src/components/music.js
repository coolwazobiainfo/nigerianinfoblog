import React, { Component } from "react";
import Menu from "./menu";
import Footer from "./footer";
import settings from "./config";
import axios from "axios";

export default class Music extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      showSearchBar: false,
      email: "",
      musicLink: "",
      description: "",
      art: "",
      phone: "",
      title:"",
      name: "",
      errors: {},
      loading: false
    };
    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleSubmitMusic = this.handleSubmitMusic.bind(this);
  }

  handleTextChange(e) {
    this.setState({ [e.target.name]: e.target.value, errors: {} });
  }

  handleSubmitMusic(e) {
    e.preventDefault();
    this.setState({ loading: true, errors: {} });
    const { email, musicLink, description, name, art, phone, title } = this.state;
    let formdata = { email, link: musicLink, description, name, art, phone, title };

    axios
      .post(settings.api_url + "music/submit", formdata)
      .then(res => {
        this.setState({
          loading: false,
          email: "",
          description: "",
          musicLink: "",
          art: "",
          phone: "",
          name: "",
          title: ""
        });
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false, errors: err.response.data.errors });
      });
  }
  componentDidMount() {
      window.performance.now();
    }

  render() {
    console.log(this.state);
    return (
      <div>
        <Menu />

        <section
          className="font-sans h-screen w-full bg-cover text-center flex flex-col items-center justify-center"
          style={{
            background: "url(/img/3173.jpg) no-repeat center",
            backgroundSize: "cover"
          }}
        >
          <h3 className="text-white mx-auto max-w-sm mt-4 font-thin text-2xl leading-normal font-bold text-5xl ">
            Submit Your Music
          </h3>
        </section>

        <section className="font-sans text-center py-8 px-4 lg:px-0 bg-grey-lightest">
          <div className="container flex flex-col sm:flex-row bg-white max-w-xl m-auto border">
            <div className="w-full sm:w-1/1 px-6 pt-6 text-left flex flex-col justify-center">
              <form onSubmit={this.handleSubmitMusic}>
                <span className="error">
                  {this.state.errors.hasOwnProperty("email")
                    ? this.state.errors.email[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="Email"
                    type="text"
                    name="email"
                    onChange={this.handleTextChange}
                    value={this.state.email}
                    placeholder="Email"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="Email"
                  >
                    Email
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("name")
                    ? this.state.errors.name[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="name"
                    type="text"
                    name="name"
                    onChange={this.handleTextChange}
                    value={this.state.name}
                    placeholder="Name"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="name"
                  >
                    Name
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("title")
                    ? this.state.errors.title[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="musictitle"
                    type="text"
                    name="title"
                    onChange={this.handleTextChange}
                    value={this.state.title}
                    placeholder="Music Title"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="title"
                  >
                    Music Title
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("link")
                    ? this.state.errors.link[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="musicLink"
                    type="text"
                    name="musicLink"
                    onChange={this.handleTextChange}
                    value={this.state.musicLink}
                    placeholder="Music Link"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="musicLink"
                  >
                    Music Link
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("art")
                    ? this.state.errors.art[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="art"
                    type="text"
                    name="art"
                    onChange={this.handleTextChange}
                    value={this.state.art}
                    placeholder="Art Link"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="art"
                  >
                    Art Link
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("phone")
                    ? this.state.errors.phone[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <input
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="phone"
                    type="number"
                    name="phone"

                    onChange={this.handleTextChange}
                    value={this.state.phone}
                    placeholder="Phone"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="phone"
                  >
                    Phone
                  </label>
                </div>

                <span className="error">
                  {this.state.errors.hasOwnProperty("description")
                    ? this.state.errors.description[0]
                    : ""}
                </span>
                <div className="relative border rounded mb-4   appearance-none label-floating">
                  <textarea
                    className="w-full py-2 px-3 text-grey-darker leading-normal rounded"
                    id="description"
                    name="description"
                    rows="3"
                    onChange={this.handleTextChange}
                    value={this.state.description}
                    placeholder="Tell us about you and the music"
                  />
                  <label
                    className="absolute block text-grey-darker pin-t pin-l w-full px-3 py-2 leading-normal"
                    htmlFor="description"
                  />
                </div>

                <div className="flex items-center justify-between">
                  <button
                    className="bg-black hover:bg-black text-white py-2 px-4 mb-8 rounded-full"
                    type="submit"
                  >
                    {this.state.loading ? (
                      <i
                        style={{ paddingLeft: 20, paddingRight: 20 }}
                        className="fa fa-circle-o-notch fa-spin"
                      />
                    ) : (
                      "Submit"
                    )}
                  </button>
                </div>
              </form>
            </div>
          </div>
        </section>

        <Footer />
      </div>
    );
  }
}
