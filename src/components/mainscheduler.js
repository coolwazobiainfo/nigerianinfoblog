import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import './style.css';
import settings from './config';


class Mainscheduler extends Component {
  
   constructor (props) {
     super(props);

     this.state = {
        schedule: '',
        later: '', 
        location: localStorage.getItem('location'),      
    }
     
     this.openradio = this.openradio.bind(this);
   }

 
  openradio = (e) =>  {
    e.preventDefault();
    window.open ("https://radio.nigeriainfo.fm","mywindow","menubar=1,resizable=1,width=550,height=950");
  }

  between = (start, end) => {
        let format = 'hh:mm:ss'
        // var time = moment() gives you current time. no format required.
        let time = moment(),
            beforeTime = moment(start, format),
            afterTime = moment(end, format);
  
        if (time.isBetween(beforeTime, afterTime)) {
          return 'NOW';
        } else if(beforeTime.isAfter(time)) {
  
          return 'Later';
  
        }else{
          return 'Ended';
        }
  }

  now_playing = (schedule) => {

    if (schedule[this.state.location]) {
            let myDate = new Date();
            let schedule_arry = schedule['Lagos'];
            if (myDate.getDay() === 6) {
              //saturday
              //filter by saturday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "saturday";
              });
    
            } else if (myDate.getDay() === 0) {
              //sunday
              //filter by sunday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "sunday";
              });
    
            } else {
              //otherdays
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period !== "sunday" && sch.period !== "saturday";
              });
            }
            for (let i = 0; i < schedule_arry.length; i++) {
              let schedu = schedule_arry[i];
              if (this.between(schedu.start, schedu.end) === "NOW") {
                return schedu;
              }
            }
         }
  }

   componentDidMount () {

      window.performance.now();

      fetch(settings.api_url +'schedules')
        .then(response => response.json())
        .then(data => { let sch = _.groupBy(data, function(city) {
              return city.state;
            }); this.setState({ schedule: this.now_playing(sch) }); });  



   }

  render() {

    return ( 
       <div className="font-sans container max-100 m-auto flex flex-col md:flex-row sm:items-center">
         <div style={{backgroundImage:`url(${"https://res.cloudinary.com/xyluz/image/upload/v1549181097/bg_ywjliv.jpg"})`, backgroundRepeat: "no-repeat",backgroundSize: "100%", backgroundPosition: "top center", height: "700px", margin: "0 auto"}} className="w-full text-center  flex flex-col justify-center items-start px-6 py-0 md:py-8 md:px-8 lg:items-start">
            <label style={{opacity: ".8", textAlign: "center"}} htmlFor="tagline" className="uppercase text-center mx-auto  text-white font-bold on-air">
              <span className="ion-md-wifi"></span> NOW ON AIR                   
            </label>
            <h1  className="mt-2 mb-4 font-druk topik uppercase m-auto text-center text-white mt-6" style={{fontFamily: 'Termina-Demi'}}>{this.state.schedule ? this.state.schedule.description : "No Show"}</h1>
            <p className="inline-flex text-white font-normal text-lg text-center mx-auto ">
              <span style={{fontFamily: 'FreightSansProBold-Regular'}} className="uppercase">{this.state.schedule ? this.state.schedule.title : ""}</span>
            </p>
            <p className="inline-flex text-white font-normal text-lg text-center mx-auto time mt-6 ">
              <span style={{fontFamily: 'FreightSansProBold-Regular'}}>
                <span className="ion ion-md-time"></span> {this.state.schedule?this.state.schedule.start : '0.00'} - {this.state.schedule?this.state.schedule.end:"0.00"}                 
              </span>
            </p>
        
            <p style={{paddingLeft: "2em",paddingRight: "2em"}} className="inline-flex text-white font-bold text-lg text-center mx-auto h-auto mt-6 btn-fr ">
              <span>
                <span className="ion-md-play" style={{cursor: "pointer"}} onClick={this.openradio}> LISTEN LIVE</span>  
              </span>
            </p>
          </div>
          
      </div>   
    )
  }
}

export default Mainscheduler;

  
    
