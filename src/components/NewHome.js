import React, { Component } from 'react';

import data from './../data';

import settings from './config';
import ReactGA from 'react-ga';
import  PostPreloader from './preloaders/postloader';
import moment from 'moment';
import Menu from './menu';
import Footer from './footer';
import { slug } from './../helpers';
import HeroPreloader  from './preloaders/heroloader';
import  CategoryPreloader  from './preloaders/categoryloader';

class NewHome extends Component {

    constructor(props) {

		super(props);

		this.state = {
			posts: [],
			visible: 4,
			visible_2: 10,
            loading: true,
            trending: [],
            foreignPosts: [],
            f_visible: 7,
            sportPosts: [],
            s_visible: 8,
            localPosts: [],
            l_visible: 3,
            trending_visible: 8
		};

		this.loadMore = this.loadMore.bind(this);
	}

	loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 3 };
		});
	}


  componentDidMount() {
      window.performance.now();

      fetch(settings.api_url + 'leadingconversation')
        .then(response => response.json())
        .then(data =>  this.setState({ leading_conversation: data }) ); 
        
        fetch(settings.api_url+'allposts')
			.then(response => response.json())
			.then(data => {
                
                 this.setState({ posts: data, loading:false });

                //  console.log(this.state.posts);
            });

            fetch(settings.api_url+'group/sport')
			.then(response => response.json())
            .then(data => this.setState({ sportPosts: data }));
            
            fetch(settings.api_url+'group/local')
			.then(response => response.json())
			.then(data => this.setState({ localPosts: data }));
            
            fetch(settings.api_url+'group/foreign')
			.then(response => response.json())
            .then(data => this.setState({ foreignPosts: data }));
            
            fetch(settings.api_url+'trending')
			.then(response => response.json())
			.then(data => this.setState({ trending: data }));
    }

    handleClick() {

		ReactGA.initialize('UA-103305032-1');

		ReactGA.pageview(window.location.pathname);

		ReactGA.event({
			category: 'Navigation',
			action: 'Clicked Link',
		});
	}


  render() {

    const article0_img = this.state.posts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.posts[0].image : "..."; 
    const article0_cat = this.state.posts[0] ? this.state.posts[0].category.name : "..."; 
    const article0_title = this.state.posts[0] ? this.state.posts[0].title : "..."; 
    const article0_time = this.state.posts[0] ? moment(this.state.posts[0].created_at).fromNow() : "...";
    const article0_slug = this.state.posts[0] ? "post/" + this.state.posts[0].slug : '#';

    const article1_img = this.state.posts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" +this.state.posts[1].image : "..."; 
    const article1_cat = this.state.posts[1] ? this.state.posts[1].category.name : "..."; 
    const article1_title = this.state.posts[1] ? this.state.posts[1].title : "..."; 
    const article1_time = this.state.posts[1] ? moment(this.state.posts[1].created_at).fromNow() : "...";
    const article1_slug = this.state.posts[1] ? "post/" + this.state.posts[1].slug : '#';


    const article2_img = this.state.posts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.posts[2].image : "..."; 
    const article2_cat = this.state.posts[2] ? this.state.posts[2].category.name : "..."; 
    const article2_title = this.state.posts[2] ? this.state.posts[2].title : "..."; 
    const article2_time = this.state.posts[2] ? moment(this.state.posts[2].created_at).fromNow() : "...";
    const article2_slug = this.state.posts[2] ? "post/" + this.state.posts[2].slug : '#';


    const article3_img = this.state.sportPosts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.sportPosts[0].image : "..."; 
    const article3_cat = this.state.sportPosts[0] ? this.state.sportPosts[0].category.name : "..."; 
    const article3_title = this.state.sportPosts[0] ? this.state.sportPosts[0].title : "..."; 
    const article3_time = this.state.sportPosts[0] ? moment(this.state.sportPosts[0].created_at).fromNow() : "...";
    const article3_slug = this.state.sportPosts[0] ? "post/" + this.state.sportPosts[0].slug : '#';


    const article4_img = this.state.sportPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.sportPosts[1].image : "..."; 
    const article4_cat = this.state.sportPosts[1] ? this.state.sportPosts[1].category.name : "..."; 
    const article4_title = this.state.sportPosts[1] ? this.state.sportPosts[1].title : "..."; 
    const article4_time = this.state.sportPosts[1] ? moment(this.state.sportPosts[1].created_at).fromNow() : "...";
    const article4_slug = this.state.sportPosts[1] ? "post/" + this.state.sportPosts[1].slug : '#';


    const article5_img = this.state.localPosts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[0].image : "..."; 
    const article5_cat = this.state.localPosts[0] ? this.state.localPosts[0].category.name : "..."; 
    const article5_title = this.state.localPosts[0] ? this.state.localPosts[0].title : "..."; 
    const article5_time = this.state.localPosts[0] ? moment(this.state.localPosts[0].created_at).fromNow() : "...";
    const article5_slug = this.state.localPosts[0] ? "post/" + this.state.localPosts[0].slug : '#';


    const article6_img = this.state.localPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[1].image : "..."; 
    const article6_cat = this.state.localPosts[1] ? this.state.localPosts[1].category.name : "..."; 
    const article6_title = this.state.localPosts[1] ? this.state.localPosts[1].title : "..."; 
    const article6_time = this.state.localPosts[1] ? moment(this.state.localPosts[1].created_at).fromNow() : "...";
    const article6_slug = this.state.localPosts[1] ? "post/" + this.state.localPosts[1].slug : '#';

    // const article7_img = this.state.localPosts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[2].image : "..."; 
    // const article7_cat = this.state.localPosts[2] ? this.state.localPosts[2].category.name : "..."; 
    // const article7_title = this.state.localPosts[2] ? this.state.localPosts[2].title : "..."; 
    // const article7_time = this.state.localPosts[2] ? moment(this.state.localPosts[2].created_at).fromNow() : "...";
    // const article7_slug = this.state.localPosts[2] ? "post/" + this.state.localPosts[2].slug : '#';


    const article8_img = this.state.posts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.posts[0].image : "..."; 
    const article8_cat = this.state.posts[0] ? this.state.posts[0].category.name : "..."; 
    const article8_title = this.state.posts[0] ? this.state.posts[0].title : "..."; 
    const article8_time = this.state.posts[0] ? moment(this.state.posts[0].created_at).fromNow() : "...";
    const article8_slug = this.state.posts[0] ? "post/" + this.state.posts[0].slug : '#';


    // const article9_img = this.state.foreignPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.foreignPosts[1].image : "..."; 
    // const article9_cat = this.state.foreignPosts[1] ? this.state.foreignPosts[1].category.name : "..."; 
    // const article9_title = this.state.foreignPosts[1] ? this.state.foreignPosts[1].title : "..."; 
    // const article9_time = this.state.foreignPosts[1] ? moment(this.state.foreignPosts[1].created_at).fromNow() : "...";
    // const article9_slug = this.state.foreignPosts[1] ? "post/" + this.state.foreignPosts[1].slug : '#';    


    // const article10_img = this.state.foreignPosts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.foreignPosts[2].image : "..."; 
    // const article10_cat = this.state.foreignPosts[2] ? this.state.foreignPosts[2].category.name : "..."; 
    // const article10_title = this.state.foreignPosts[2] ? this.state.foreignPosts[2].title : "..."; 
    // const article10_time = this.state.foreignPosts[2] ? moment(this.state.foreignPosts[2].created_at).fromNow() : "...";
    // const article10_slug = this.state.foreignPosts[2] ? "post/" + this.state.foreignPosts[2].slug : '#';

    return (
        
      <div className="body-wrapper">
          <div className="body-innerwrapper">

            <Menu />
           
                {/* <div id="hightlight" className="sppb-section hightlight">
                    <div className="row no-gutter"> */}

                        {/* <div className="col-md-6">
                            <div className="sppb-addon-feature overlay" style={{backgroundImage: `url(${article0_img})`}}>
                                 <img src={article0_img} alt={article0_time} className="img-responsive post-thumb" />
                                <div className="sppb-addon-content">
                                    <span className="tags bg-nt">{article0_cat}</span>
                                    <a href={`${article0_slug}` }><h2 className="sppb-addon-title h1">{article0_title}</h2></a>
                                    <div className="meta">
                                        <span className="author">by <a href="#">Amin</a></span>
                                        <span className="date">{article0_time}</span>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* <div className="col-md-3">
                            <div className="sppb-addon-feature overlay" style={{backgroundImage: `url(${article1_img})`}}>
                                <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + article1_img}`} alt="" className="img-responsive post-thumb" />
                                <div className="sppb-addon-content">
                                    <span className="tags bg-nt">{article1_cat}</span>
                                    <a href={article1_slug}><h2 className="sppb-addon-title h1">{article1_title}</h2></a>
                                    <div className="meta">
                                        <span className="author">by <a href="#">Admin</a></span>
                                        <span className="date">{article1_time}</span>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        {/* <div className="col-md-3">
                            <div className="sppb-addon-feature overlay" style={{backgroundImage: `url(${article2_img})`}}>
                                <img src={article2_img} alt="" className="img-responsive post-thumb" /> 
                                <div className="sppb-addon-content">
                                    <span className="tags bg-nt">{article2_cat}</span>
                                    <a href={article2_slug}><h2 className="sppb-addon-title h1">{article2_title}</h2></a>
                                    <div className="meta">
                                        <span className="author">by <a href="#">Admin</a></span>
                                        <span className="date">{article2_time}</span>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                    {/* </div>
                </div> */}

        <div id="sp-main-body">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-full">
                                
                                <div id="world" className="sppb-section">

                                    <div className="sppb-section-title">
                                        <a href={`/category/${slug("news",'-',' ')}`}>
                                            <h4 style={{  background: '#00b3ff', textAlign: 'center', color: 'white', borderRadius: '4px 1px 11px 0px', paddingLeft: '.8em'}}><strong>News</strong></h4>
                                        </a>
                                        <p className="sppb-title-subheading"><a href="#">View all</a></p>
                                    </div>
{this.state.loading ?

    <HeroPreloader /> :
                                    <div className="row">

                                        <div className="col-md-7 hidden-sm">
                                            <div className="sppb-addon-feature mb30-xs">
                                            <a href={`${article8_slug}`}>
                                                <img src={article8_img} alt={article8_title} className="img-responsive post-thumb img-fitw" />
                                            </a>
                                                <div className="sppb-addon-content">
                                                    <a href={`${article8_slug}`}><h3 className="sppb-addon-title">{article8_title}</h3></a>
                                                    <div className="meta">
                                                        <span className="author">In <a href={`/category/${slug(article8_cat,'-',' ')}`}>{article8_cat}</a>&nbsp;</span>
                                                        <span className="date">{article8_time}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-5">

                                        { this.state.posts.slice(1, this.state.f_visible).map((post, index) => 
                     
                                        <div className="sppb-addon-feature icon-right" key={index}>
                                            <a href={`post/${post.slug}`}>
                                                <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt={post.title} className="img-responsive post-thumb" />
                                            </a>
                                            <div className="sppb-addon-content">
                                                <a href={`post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                                                <div className="meta">
                                                    <span className="author">In <a href={`/category/${slug(post.category.name,'-',' ')}`}>{post.category.name}</a>&nbsp;</span>
                                                    <span className="date">{moment(post.created_at).fromNow()}</span>
                                                </div>
                                            </div>
                                        </div>
                                            
                                        )}            
                                            
                                           
                         
                                        </div>
                                    </div> 

}
                                      {/* inner row */}
                                </div>


                <div id="latest" className="sppb-section">
                    <div className="container">
                        <div className="sppb-section-title">
                        <a href={`/category/${slug("talk",'-',' ')}`}>
                            <h4 style={{ background: '#652483', textAlign: 'center', color: 'white', borderRadius: '4px 1px 11px 0px', paddingLeft: '.8em' }}><strong>Talk</strong> </h4>
                        </a>
                            <p className="sppb-title-subheading"><a href="#">View all</a></p>
                        </div>
            {this.state.loading ?

                <CategoryPreloader /> :
                        <div className="row">
                            <div className="col-md-full">
                       { this.state.trending.slice(0, this.state.trending_visible).map((post, index) => 
                       
                            <div className="col-md-3 col-sm-6" key={index}>
                                <div className="sppb-addon-feature mb30-sm">
                                    <a href={`post/${post.slug}`}>   
                                        <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt="" className="img-responsive post-thumb" />
                                    </a>
                                    <div className="sppb-addon-content">
                                        <a href={`post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                                        <div className="meta">
                                            <span className="author">In <a href={`/category/${slug(post.category.name,'-',' ')}`}>{post.category.name}</a>&nbsp;</span>
                                            <span className="date">{moment(post.created_at).fromNow()}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    
                       )}                            
                       
                        </div>
                    </div>
                     }
                </div>
           
                {/* Trending */}

                
                    <div id="entertainment" className="sppb-section">
                        <div className="sppb-section-title wow fadeInUp">
                            <a href={`/category/${slug("sports",'-',' ')}`}>
                                <h4 style={{background: '#fcea0e', textAlign: 'center', color: 'black',  borderRadius: '4px 1px 11px 0px',paddingLeft: '.8em'}}><strong>Sports</strong></h4>
                            </a>
                            <p className="sppb-title-subheading"><a href="#">View all</a></p>
                        </div>

                        {/* <div className="row"> */}
                            {/* <div className="col-md-8">
                                <div className="sppb-addon-feature overlay mb20 wow fadeInUp" style={{backgroundImage: `url(${article3_img})`}}>
                                    <img src={`post/${article3_img}`} alt={article3_time} className="img-responsive post-thumb w570x370" />
                                    <div className="sppb-addon-content">
                                        <a href={article3_slug}><h2 className="sppb-addon-title">{article3_title}</h2></a>
                                        <div className="meta">
                                            <span className="author">In <a href="#">{article3_cat}</a></span>
                                            <span className="date">{article3_time}</span>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                            {/* <div className="col-md-4 hidden-sm">
                                <div className="sppb-addon-feature mb30-xs wow fadeInUp" data-wow-delay=".3s"> */}
                                    {/* <img src={article4_img} alt="" className="img-responsive post-thumb img-fitw" />
                                    <div className="sppb-addon-content">
                                        <a href={`post/${article4_slug}`}><h4 className="sppb-addon-title">{article4_title}</h4></a>
                                        <div className="meta">
                                            <span className="author">In <a href="#">{article4_cat}</a></span>
                                            <span className="date">{article4_time}</span>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                        {/* </div> */}
                        {this.state.loading ?

<CategoryPreloader /> :
                        <div className="row">
                            <div className="col-md-full">
                        { this.state.sportPosts.slice(0, this.state.s_visible).map((post, index) => 
                     

                        // <div className="col-md-4 col-sm-4" key={index}>
                        //     <div className="sppb-addon-feature mb30-xs wow fadeInUp">
                        //         <a href={`post/${post.slug}`}>

                        //             <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt={post.title} className="img-responsive post-thumb img-fitw" />
                        //         </a>
                        //         <div className="sppb-addon-content">
                        //             <a href={`post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                        //             <div className="meta">
                        //                 <span className="author">In <a href={`/category/${slug(post.category.name,'-',' ')}`}>{post.category.name}</a>&nbsp;</span>
                        //                 <span className="date">{moment(post.created_at).fromNow()}</span>
                        //             </div>
                        //         </div>
                        //     </div>
                        // </div>

                        <div className="col-md-3 col-sm-6" key={index}>
                        <div className="sppb-addon-feature mb30-sm">
                            <a href={`post/${post.slug}`}>   
                                <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt="" className="img-responsive post-thumb" />
                            </a>
                            <div className="sppb-addon-content">
                                <a href={`post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                                <div className="meta">
                                    <span className="author">In <a href={`/category/${slug(post.category.name,'-',' ')}`}>{post.category.name}</a>&nbsp;</span>
                                    <span className="date">{moment(post.created_at).fromNow()}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                         
                        )}     
                       
                            </div>
                        </div>  
                        }
                        {/* <div id="technology" className="sppb-section">
                                    <div className="sppb-section-title wow fadeInUp">
                                        <h4><strong>Entertainment</strong></h4>
                                        <p className="sppb-title-subheading"><a href="#">View all</a></p>
                                    </div> */}

                                    {/* <div className="row"> */}
                                        {/* <div className="col-md-6">
                                            <div className="sppb-addon-feature overlay mb30 wow fadeInUp" style={{backgroundImage: `url(${article5_img})`}}>
                                                <img src={article5_img} alt={article5_title} className="img-responsive post-thumb w420x280" />
                                                <div className="sppb-addon-content">
                                                    <a href={`post/${article5_slug}`}><h3 className="sppb-addon-title">{article5_title}</h3></a>
                                                    <div className="meta">
                                                        <span className="author">In <a href="#">{article5_cat}</a></span>
                                                        <span className="date">{article5_time}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}
                                        {/* <div className="col-md-6">
                                            <div className="sppb-addon-feature overlay mb30-sm wow fadeInUp" data-wow-delay=".1s" style={{backgroundImage: `url(${article6_img})`}}>
                                                <img src={article6_img} alt="" className="img-responsive post-thumb w420x280" />
                                                <div className="sppb-addon-content">
                                                    <a href={`post/${article6_slug}`}><h3 className="sppb-addon-title">{article6_title}</h3></a>
                                                    <div className="meta">
                                                        <span className="author">In <a href="#">{article6_cat}</a></span>
                                                        <span className="date">{article6_time}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> */}
                                    {/* </div> */}

                                    {/* <div className="row">
                                    <div className="col-md-full">
                                    { this.state.localPosts.slice(0, this.state.l_visible).map((post, index) => 
                     
                                    <div className="col-md-4 col-sm-4" key={index}>
                                        <div className="sppb-addon-feature mb30-xs wow fadeInUp">
                                            <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt={post.title} className="img-responsive post-thumb img-fitw" />
                                            <div className="sppb-addon-content">
                                                <a href={`post/${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                                                <div className="meta">
                                                    <span className="author">In<a href="#">{post.category.name}</a>&nbsp;</span>
                                                    <span className="date">{moment(post.created_at).fromNow()}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      
                                    )}               
                                       </div>
                                    </div>   */}
                                {/* </div>*/}

                            </div> 

                            {/* <div id="articles" className="sppb-section sppb-list">

                                <div className="sppb-section-title wow fadeInUp">
                                    <h4>Latest <strong>Articles</strong></h4>
                                    <p className="sppb-title-subheading"><a href="#">View all Articals</a></p>
                                </div>

                                <div className="sppb-addon-features">

                                { this.state.posts.slice(0, this.state.visible_2).map((post, index) => 
                     
                                <div className="sppb-addon-feature icon-left wow fadeInUp" key={index}>
                                    <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt={post.title} className="img-responsive post-thumb" />
                                    <div className="sppb-addon-content">
                                        <a href={`post/${post.slug}`}><h3 className="sppb-addon-title">{post.title}</h3></a>
                                        <div className="meta">
                                            <span className="author">In <a href="#">{post.category.name}</a></span>
                                            <span className="date">{moment(post.created_at).fromNow()}</span>
                                        </div>
                                    </div>
                                </div>
                               
                
                                )} 

                                </div>

                            </div> */}


                        </div>

                        </div> {/*row*/}
                    </div>{/*container*/}
                </div>
                {/* World News */}


          

          </div>
          </div>
      </div>
    );
  }
}

export default NewHome;
