import React, { Component } from 'react';
import settings from './config';


class Interviews extends Component {

    constructor(props) {

        super(props);
           
        this.state = {

           leading_conversation: []        
    
        }; 

     }

   componentDidMount() {

      window.performance.now();

      fetch(settings.api_url + 'leadingconversation')
        .then(response => response.json())
        .then(data =>  this.setState({ leading_conversation: data }) );    

    }
      

  render() { 
       
      let leading_img_0 = this.state.leading_conversation[0] ? this.state.leading_conversation[0].image : '';
      let leading_img_1 = this.state.leading_conversation[1] ? this.state.leading_conversation[1].image : '';
      let leading_img_2 = this.state.leading_conversation[2] ? this.state.leading_conversation[2].image : '';

      let leading_slug_0 = this.state.leading_conversation[0] ? this.state.leading_conversation[0].category.name.toLowerCase().replace(/ /g, "-") : '';
      let leading_slug_1 = this.state.leading_conversation[1] ? this.state.leading_conversation[1].category.name.toLowerCase().replace(/ /g, "-") : '';
      let leading_slug_2 = this.state.leading_conversation[2] ? this.state.leading_conversation[2].category.name.toLowerCase().replace(/ /g, "-") : '';


      let leading_post_slug_0 = this.state.leading_conversation[0] ? this.state.leading_conversation[0].slug : "#";
      let leading_post_slug_1 = this.state.leading_conversation[1] ? this.state.leading_conversation[1].slug : "#";
      let leading_post_slug_2 = this.state.leading_conversation[2] ? this.state.leading_conversation[2].slug : "#";
          
    return (
      <section className="bg-grey-darkest py-4 font-sans">
<p className="text-center uppercase p-2  text-white font-druk tracking-wid text-xl mb-8 mt-4" style={{fontFamily: 'Termina-Demi'}}>leading conversations</p>
  <div className=" max-w-xl m-auto flex flex-wrap items-center justify-start">
         


    <div style={{ height:"655px"}} className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3  height-sm">
      <div className="overflow-hidden bg-white hover:shadow-raised hover:translateY-2px transition" style={{ height:"575px"}}>
        <div style={{background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + leading_img_0}) 0% 0% / cover`, height: '250px'}}></div><div className="p-6 flex flex-col justify-between">  
        <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
              <span className="font-sans  uppercase "> 

              <a className='font-sans uppercase text-black' href={'shows/' + leading_slug_0}>
                    
              {this.state.leading_conversation[0]?this.state.leading_conversation[0].category.name : ''}
              
                  </a>
                  
              </span> 
            </p>

                                                                 

          <h3 className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase">
          
          <a className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase" href={"post/" + leading_post_slug_0}>{this.state.leading_conversation[0] ? this.state.leading_conversation[0].title : ''}</a>
          
          </h3>
         <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
                                                                  <span className="font-bold uppercase "> <span className="talk-play ion-md-play-circle"> </span> <span className="uppercase watch-now"> 
                                                              
                                                              <a href={"post/" + leading_post_slug_0} style={{ color: "green"}}>Watch Now </a>

                                                                  
                                                                  
                                                                  </span> </span>
                                                                  </p>
        </div>
      </div>
    </div>

    <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-sm" style={{ height:"655px"}}> 
      <div className="overflow-hidden bg-white  hover:shadow-raised hover:translateY-2px transition" style={{ height:"575px"}} >
         <div style={{background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + leading_img_1}) 0% 0% / cover`, height: '250px'}}></div>
        <div className="p-6 flex flex-col justify-between ">          

          <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
            <span className="font-sans uppercase "> 

              <a className='font-sans uppercase text-black' href={'shows/' + leading_slug_1}>
                    
                    {this.state.leading_conversation[1]?this.state.leading_conversation[1].category.name : ''}
                    
                </a>

            </span> 
          </p>


          <h3 className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase">
          
          <a className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase" href={"post/" + leading_post_slug_1}>{this.state.leading_conversation[1] ? this.state.leading_conversation[1].title : ''}</a>
          
          </h3>
       <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
                                                                  <span className="font-bold uppercase "> <span className="talk-play ion-md-play-circle"> </span> <span className="uppercase watch-now"> 


                                                              <a href={"post/" + leading_post_slug_1} style={{ color: "green"}}>Watch Now </a>


                                                                  </span> </span>
                                                                  </p>
        </div>
      </div>
    </div>

    <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-sm" style={{ height:"655px"}}>
      <div className="overflow-hidden bg-white hover:shadow-raised hover:translateY-2px transition" style={{ height:"575px"}}>
      <div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + leading_img_2}) 0% 0% / cover` ,height: '250px'}}></div>
        <div className="p-6 flex flex-col justify-between ">   <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
              <span className="font-sans uppercase  "> 
              <a className='font-sans uppercase text-black' href={'shows/' + leading_slug_2}>
                    
                    {this.state.leading_conversation[2]?this.state.leading_conversation[2].category.name : ''}
                    
                        </a>
              </span> 
            </p>

          <h3 className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase">
          
          <a className="text-black  text-xl mb-2 font-bold leading-normal mb-4 uppercase" href={"post/" + leading_post_slug_2}>{this.state.leading_conversation[2] ? this.state.leading_conversation[2].title : ''}</a>
          
          </h3>
   <p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase  " >
                                                                  <span className="font-bold uppercase "> <span className="talk-play ion-md-play-circle"> </span> <span className="uppercase watch-now"> 

                                                                   <a href={"post/" + leading_post_slug_2} style={{ color: "green"}}>Watch Now </a>

                                                                  </span> </span>
                                                                  </p>
        </div>
      </div>
    </div>

  

  </div>
</section>

    );
  }
}

export default Interviews;
