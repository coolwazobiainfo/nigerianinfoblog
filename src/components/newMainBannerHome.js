import React, { Component } from 'react';
import settings from './config';
import Ad from './ad';
import ReactGA from 'react-ga';
import Homemobileswitch from './homemobileswitch';
import  PostPreloader from './preloaders/postloader';
import moment from 'moment';

class NewMainBannerHome extends Component {

    constructor(props) {

        super(props);
           
        this.state = {

           leading_conversation: [],
           posts: [],
		   visible: 12,
           loading: true,  
           l_loading: true,
		   s_loading: true,
		   f_loading: true,
		   l_visible: 6,
		   s_visible: 6,
		   f_visible: 6,
		   localPosts: [],
		   sportPosts: [],
		   foreignPosts: []      
    
        }; 

        this.loadMore = this.loadMore.bind(this);

     }

     loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 3 };
		});
	}

   componentDidMount() {

      window.performance.now();

      fetch(settings.api_url + 'leadingconversation')
        .then(response => response.json())
        .then(data =>  this.setState({ leading_conversation: data }) ); 
        
        fetch(settings.api_url+'allposts')
			.then(response => response.json())
			.then(data => {
                
                 this.setState({ posts: data, loading:false });

                 console.log(this.state.posts);
            });

            fetch(settings.api_url+'group/sport')
			.then(response => response.json())
            .then(data => this.setState({ sportPosts: data, s_loading:false }));
            
            fetch(settings.api_url+'group/local')
			.then(response => response.json())
			.then(data => this.setState({ localPosts: data, l_loading:false }));
            
            fetch(settings.api_url+'group/foreign')
			.then(response => response.json())
			.then(data => this.setState({ foreignPosts: data, f_loading:false }));

    }
    handleClick() {

		ReactGA.initialize('UA-103305032-1');

		ReactGA.pageview(window.location.pathname);

		ReactGA.event({
			category: 'Navigation',
			action: 'Clicked Link',
		});
	}
      

  render() {

    const article0_img = this.state.posts[0] ? this.state.posts[0].image : "..."; 
    const article0_cat = this.state.posts[0] ? this.state.posts[0].category.name : "..."; 
    const article0_title = this.state.posts[0] ? this.state.posts[0].title : "..."; 
    const article0_time = this.state.posts[0] ? moment(this.state.posts[0].created_at).fromNow() : "...";
    const article0_slug = this.state.posts[0] ? "post/" + this.state.posts[0].slug : '#';

    const article1_img = this.state.posts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.posts[1].image : "..."; 
    const article1_cat = this.state.posts[1] ? this.state.posts[1].category.name : "..."; 
    const article1_title = this.state.posts[1] ? this.state.posts[1].title : "..."; 
    const article1_time = this.state.posts[1] ? moment(this.state.posts[1].created_at).fromNow() : "...";
    const article1_slug = this.state.posts[1] ? "post/" + this.state.posts[1].slug : '#';


    const article2_img = this.state.posts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.posts[2].image : "..."; 
    const article2_cat = this.state.posts[2] ? this.state.posts[2].category.name : "..."; 
    const article2_title = this.state.posts[2] ? this.state.posts[2].title : "..."; 
    const article2_time = this.state.posts[2] ? moment(this.state.posts[2].created_at).fromNow() : "...";
    const article2_slug = this.state.posts[2] ? "post/" + this.state.posts[2].slug : '#';


    const article3_img = this.state.sportPosts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.sportPosts[0].image : "..."; 
    const article3_cat = this.state.sportPosts[0] ? this.state.sportPosts[0].category.name : "..."; 
    const article3_title = this.state.sportPosts[0] ? this.state.sportPosts[0].title : "..."; 
    const article3_time = this.state.sportPosts[0] ? moment(this.state.sportPosts[0].created_at).fromNow() : "...";
    const article3_slug = this.state.sportPosts[0] ? "post/" + this.state.sportPosts[0].slug : '#';


    const article4_img = this.state.sportPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.sportPosts[1].image : "..."; 
    const article4_cat = this.state.sportPosts[1] ? this.state.sportPosts[1].category.name : "..."; 
    const article4_title = this.state.sportPosts[1] ? this.state.sportPosts[1].title : "..."; 
    const article4_time = this.state.sportPosts[1] ? moment(this.state.sportPosts[1].created_at).fromNow() : "...";
    const article4_slug = this.state.sportPosts[1] ? "post/" + this.state.sportPosts[1].slug : '#';


    const article5_img = this.state.localPosts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[0].image : "..."; 
    const article5_cat = this.state.localPosts[0] ? this.state.localPosts[0].category.name : "..."; 
    const article5_title = this.state.localPosts[0] ? this.state.localPosts[0].title : "..."; 
    const article5_time = this.state.localPosts[0] ? moment(this.state.localPosts[0].created_at).fromNow() : "...";
    const article5_slug = this.state.localPosts[0] ? "post/" + this.state.localPosts[0].slug : '#';


    const article6_img = this.state.localPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[1].image : "..."; 
    const article6_cat = this.state.localPosts[1] ? this.state.localPosts[1].category.name : "..."; 
    const article6_title = this.state.localPosts[1] ? this.state.localPosts[1].title : "..."; 
    const article6_time = this.state.localPosts[1] ? moment(this.state.localPosts[1].created_at).fromNow() : "...";
    const article6_slug = this.state.localPosts[1] ? "post/" + this.state.localPosts[1].slug : '#';

    const article7_img = this.state.localPosts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.localPosts[2].image : "..."; 
    const article7_cat = this.state.localPosts[2] ? this.state.localPosts[2].category.name : "..."; 
    const article7_title = this.state.localPosts[2] ? this.state.localPosts[2].title : "..."; 
    const article7_time = this.state.localPosts[2] ? moment(this.state.localPosts[2].created_at).fromNow() : "...";
    const article7_slug = this.state.localPosts[2] ? "post/" + this.state.localPosts[2].slug : '#';


    const article8_img = this.state.foreignPosts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.foreignPosts[0].image : "..."; 
    const article8_cat = this.state.foreignPosts[0] ? this.state.foreignPosts[0].category.name : "..."; 
    const article8_title = this.state.foreignPosts[0] ? this.state.foreignPosts[0].title : "..."; 
    const article8_time = this.state.foreignPosts[0] ? moment(this.state.foreignPosts[0].created_at).fromNow() : "...";
    const article8_slug = this.state.foreignPosts[0] ? "post/" + this.state.foreignPosts[0].slug : '#';


    const article9_img = this.state.foreignPosts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.foreignPosts[1].image : "..."; 
    const article9_cat = this.state.foreignPosts[1] ? this.state.foreignPosts[1].category.name : "..."; 
    const article9_title = this.state.foreignPosts[1] ? this.state.foreignPosts[1].title : "..."; 
    const article9_time = this.state.foreignPosts[1] ? moment(this.state.foreignPosts[1].created_at).fromNow() : "...";
    const article9_slug = this.state.foreignPosts[1] ? "post/" + this.state.foreignPosts[1].slug : '#';    


    const article10_img = this.state.foreignPosts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi" + this.state.foreignPosts[2].image : "..."; 
    const article10_cat = this.state.foreignPosts[2] ? this.state.foreignPosts[2].category.name : "..."; 
    const article10_title = this.state.foreignPosts[2] ? this.state.foreignPosts[2].title : "..."; 
    const article10_time = this.state.foreignPosts[2] ? moment(this.state.foreignPosts[2].created_at).fromNow() : "...";
    const article10_slug = this.state.foreignPosts[2] ? "post/" + this.state.foreignPosts[2].slug : '#';

          
      
    return (
        <div style={{margin: "0 auto", width: "85%"}} className=" bg-white shadow"> 

        <section className="font-sans flex flex-col lg:flex-row mt-2 p-6 m-auto">

        <div className="boxx w-full lg:w-2/3 h-auto" style={{ background:  `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + article0_img }) top center / cover`, height: "441.5px", boxShadow: "rgba(0, 0, 0, 0.46) -173px -603px 56px -219px inset" }}>
            
            <div style={{ background:"#009ee6"}} className="p-4">
                <p className="text-white  text-lg">News </p>
            </div>

            <div className=" p-6 hedd mb-8" >
                <p className="mb-4 inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase">
                    <span className="font-sans uppercase text-white ">
                        <a  className="font-sans uppercase text-white font-serif font-smooth text-base" href={"shows/" + article0_cat.toLowerCase().replace(/ /g, "-")}>{article0_cat}</a>
                    </span>
                </p>
                <h1 className="text-3xl text-white leading-tight antialiased">
                    <a className=" word-clap font-mont" href={article0_slug} style={{color: "white", fontStyle: "italic",
    fontWeight: "900" }}>{article0_title}</a></h1>
                <p className=" text-white font-seg text-lg  time mt-4  antialiased"><span><span className="ion ion-md-time"></span> {article0_time}</span>
                </p>
            </div>  

            <div className="container max-w-xl m-auto flex flex-wrap items-center justify-start disa">

    <div className="w-full md:w-1/2 lg:w-1/2  mb-8  px-3">
      <div className="overflow-hidden ">
        <img className="w-full" src={article1_img}  alt="Sunset in the mountains" />
          <div className="flex flex-col justify-between ">
        <span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article1_cat.toLowerCase().replace(/ /g, "-")}>{article1_cat}</a></span>
                    <a href={article1_slug}  className="text-grey-darkest no-underline">
                        <p for="" className=" no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> {article1_title}

                        </p>
                    </a>  <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4">
                        <span style={{opacity: "0.4" }}>
                    
                        <span className="ion ion-md-time"></span> {article1_time}</span>
                </p>
        </div>
      </div>
    </div>

    <div className="w-full  md:w-1/2 lg:w-1/2  mb-8 px-3" style={{marginTop: "45px"}}>
      <div className="overflow-hidden  ">
        <img className="w-full" src={article2_img} alt="Sunset in the mountains" />
        <div className="flex flex-col justify-between ">
        <span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article2_cat.toLowerCase().replace(/ /g, "-")}>{article2_cat}</a></span>
                                <a href={article2_slug}  className="text-grey-darkest no-underline">
                                    <p for="" className=" no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> {article2_title}

                                    </p>
                                </a>
                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{opacity: "0.4"}}><span className="ion ion-md-time"></span>{article2_time}</span>
                            </p>

        </div>
      </div>
    </div>

  </div>
        </div>
        <br />
        <br />
        <div style={{background:"#e6e6e6"}}  className="w-full lg:w-1/3 flex flex-col  justify-center text-left pt-0  ml-4 move-left mb-8  ">
            <div style={{background:"#661f85"}} className=" p-4">
                <p className="text-white  text-lg"> Talk </p>
            </div>
            <div className=" max-w-xl m-auto  mt-4">
                <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex flex-col px-3  height-smt ">
                    <div className="overflow-hidden  hover:shadow-raised hover:translateY-2px transition">
                       
                        <div className="flex flex-col justify-between ">  
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase  antialiased font-serif" href={"shows/" + article5_cat.toLowerCase().replace(/ /g, "-")}>{article5_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href={article5_slug}  style={{color: "black", fontWeight: '900'}}>{article5_title}</a></h3>

                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{opacity: "0.4"}}><span className="ion ion-md-time"></span> {article5_time}</span>
                            </p>
                        </div>
                    </div>
                </div>



     <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase  antialiased font-serif" href={"shows/" + article6_cat.toLowerCase().replace(/ /g, "-")}>{article6_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href={article6_slug}  style={{color: "black", fontWeight: '900'}}>{article6_title}</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> {article6_time}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article7_cat.toLowerCase().replace(/ /g, "-")}>{article7_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href={article7_slug}  style={{color: "black", fontWeight: '900'}}>{article7_title}</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> {article7_time}</span>
                            </p>
                        </div>
                    </div>
                </div>

                 <div  style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article8_cat.toLowerCase().replace(/ /g, "-")}>{article8_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href={article8_slug}  style={{color: "black", fontWeight: '900'}}>{article8_title}</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> {article8_time}</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div  className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article9_cat.toLowerCase().replace(/ /g, "-")}>{article9_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href={article9_slug} style={{color: "black", fontWeight: '900'}}>{article9_title}</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> {article9_time}</span>
                            </p>
                        </div>
                    </div>
                </div>

            
            </div>


        </div>
         
        <div style={{borderBottom: "3px solid #fff"}} className="w-full lg:w-1/3 flex flex-col  justify-center text-left pl-2 pt-0 ">
            <div className=" max-w-xl m-auto  ">
                <div className="w-full  flex flex-col px-3  height-smt"><div style={{background: "#fdec00", marginTop: '-15px'}} className="p-4"><p className="text-black text-lg">
                Sports </p></div>
                    <div className="overflow-hidden  hover:shadow-raised hover:translateY-2px transition">
                        <div style={{background: `url(${article3_img}) 0% 0% / cover`, height: "250px"}}></div>
                        <div className="mt-4 flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article3_cat.toLowerCase().replace(/ /g, "-")}>{article3_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-mont" href={article3_slug} style={{color: "black"}}>{article3_title}</a></h3>

                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{ opacity: "0.4" }}><span className="ion ion-md-time"></span> {article3_time}</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div style={{borderBottom: "3px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                        <div style={{background: `url(${article4_img}) 0% 0% / cover`, height: "250px" }}></div>
                        <div className="mt-4 flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + article4_cat.toLowerCase().replace(/ /g, "-")}>{article4_cat}</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-mont" href={article4_slug} style={{color: "black"}}>{article4_title}</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> {article4_time}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          
        
    </section>
    <section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center">

					<div className="w-full  max-100 m-auto mb-4 lg:mb-0">
					

						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.loading ? 
								
								<PostPreloader />
								

							: this.state.posts.slice(0, this.state.visible).map((post, index) =>
						
								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
									<div className="overflow-hidden bg-white h-auto  hover:shadow-raised hover:translateY-2px transition">
										<div className="h-64" style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image})`, backgroundSize: "cover", backgroundPosition: "top" }}></div>

										<div className="p-2 flex flex-col justify-between">
											
											<span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href={"shows/" + post.category.name.toLowerCase().replace(/ /g, "-")}>{post.category.name ? post.category.name : "null"}</a></span>

											<a href={"post/" + (post.slug == null ? post.id : post.slug)} className="text-grey-darkest no-underline">
												<p for="" className="no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> {post.title}	
												</p>
											</a>
																						

											
										</div>
									</div>
								</div>
							)}
						</div>
						<div className="text-center">
							<div className="m-auto">
								{this.state.visible < this.state.posts.length &&
									<button onClick={this.loadMore} className="bg-black p-3 rounded-lg  w-100 font-bold  text-white uppercase tracking-wide">Load More</button>
								}
							</div>
                            <br />
                            <br />
						</div>
					</div>
                            
					<Ad />

				</section>
</div>
    );
  }
}

export default NewMainBannerHome;
