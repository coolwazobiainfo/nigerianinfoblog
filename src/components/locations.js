import React, { Component } from 'react';

import './style.css';
import Menu from './menu';
import Footer from './footer';
import Modal from './modal';

class Locations extends Component {

	constructor(props){
		super(props);
		this.state = {
			location: localStorage.getItem('location'),
			display: 'none'
		}
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(e){
		this.setState({display: 'flex', location: e.target.value});
	}

	changeLocation(){

		let location = this.state.location;
		localStorage.setItem('location', location);
		this.setState({location});
		// this.props.history.push('/');
		window.location.href = '/';
	}
	componentDidMount() {
			window.performance.now();
		}


	render() {
			

    return (
    	<div>
        <Menu />

        <section
          className="font-sans h-screen w-full bg-cover text-center flex flex-col items-center justify-center"
          style={{
            // background: "url(/img/3173.jpg) no-repeat center",
			backgroundSize: "cover",
			marginBottom: 150,
          }}
        >
          <h3 style={{marginTop: 100}} className="mx-auto max-w-sm mt-4 font-thin text-2xl leading-normal font-bold text-5xl ">
            Switch Location<br />
          
          </h3><br/>
					<button className="btn btn-primary" disabled={this.state.location === `Lagos`} onClick={this.handleClick} value="Lagos">Lagos</button>
					<button className="btn btn-primary" disabled={this.state.location === `Abuja`} onClick={this.handleClick} value="Abuja">Abuja</button>
					<button className="btn btn-primary" disabled={this.state.location === `Ph`} onClick={this.handleClick} value="Ph">Portharcourt</button>
					
        </section>

		<Modal 
			closeModal={() => this.setState({display: 'none'})} 
			changeLocation={(e) => { e.preventDefault(); this.changeLocation()}}
			abort={(e) => {e.preventDefault(); this.setState({display: 'none'})}} 
			display={this.state.display}
			location={this.state.location}
		/>

        <Footer />
      </div>
    	)

	}

}

export default Locations;