import React, { Component } from 'react';
import './../App.css';

export default class Maintenance extends Component {

componentDidMount() {
            window.performance.now();
        }
    render() {

        return (

            <div id="notfound">
                <div className="text-center">
                    <img className="notfound-image" width="250px" src="/img/pie-chart.png" alt="pie-chart" />
                    <h2 id="notfound-message" style={{fontSize: 22}}>Site is under Maintenance</h2>
                    
                </div>
            </div>

        );
    }
}

