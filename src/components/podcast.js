import React, { Component } from 'react';
import settings from './config';


class Podcast extends Component {
  
		   constructor(props) {

		            super(props);
		         
				    this.state = {
				      podcasts: [],
				    };

		    }

		  componentDidMount() {

			window.performance.now();
		
		    fetch(settings.api_url + '/allpodcasts')
		      .then(response => response.json())
		      .then(data => this.setState({ podcasts: data }));
		  }


		  renderPodcasts() {

		       const styles = { fontSize: 1.2, }

		  	   const { podcasts } = this.state;

		  	    if (this.state.podcasts === null || [] || '') {

		  	    	   <p> No Podcasts are available right now </p>

		        
			      }
			      else{
                      
                      {podcasts.map(podcast =>

		                  
		                             <div className="w-full md:w-1/2 lg:w-1/4 flex flex-col mb-8 px-3">
							            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
							              <img className="w-full" src="img/3173.jpg" alt="Sunset in the mountains"/>
							                    <div className="p-6 flex flex-col justify-between ">

										          <p className="inline-flex items-center">
										             <a href="" className=" cate-sp"><span style={styles} className="mdi-podcast"></span>  Podcast</a>
										          </p>
										          <h3 className="font-medium text-grey-darkest mb-4 leading-normal">Desktop Publishing Software like Aldus PageMaker</h3>
										          <p className="inline-flex items-center">
										             <button className="btn btn-3 btn-3b ion-md-play-speed">LISTEN</button>
										          </p>
								        		</div>
								      	</div>
								   </div>

		                      
		  	    	  )}
		           
			    }

		  }

  render() {
	
    return (
       
        <div> 

	        <section className="bg-white py-4 font-sans">

			  <div className="container m-auto max-w-xl flex items-baseline justify-start border-b-2 border-grey-light mb-10">
				  <h2 className="text-grey-dark text-base font-bold tracking-wide uppercase py-4 px-6"><span className="mdi-video"></span> Videos</h2>
				  <h2 className="text-base font-bold tracking-wide uppercase py-4 px-6 border-b-2 border-black -mb-4"><span className="mdi-podcast"></span>  Podcasts</h2>
			  </div>

	       </section>

	      
			<section className="bg-white py-4 font-sans">
			  <div className="container max-w-xl m-auto flex flex-wrap items-center justify-start">

                   { this.renderPodcasts() }      

			  </div>
			</section>

      </div>

    );
  }
}

export default Podcast;






