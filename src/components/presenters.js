import React, { Component } from 'react';
import settings from './config';
import { slug } from '../helpers';


class Presenters  extends Component {
    constructor(props) {
        super(props);

        this.state = {
            presenters: []
        }

    }

    componentDidMount() {	

        window.performance.now();
        
	    fetch(settings.api_url + 'oaps/')
	      .then(response => response.json())
	      .then(data =>  this.setState({ presenters: data }));

	 }


    render() {
        return (
            <div>
                <section style={{marginTop: '-3.7em'}} class="bg-white p-2 m-auto  ">
                    <p class="text-center uppercase p-2 mt-8 text-xl text-black tracking-wid" style={{paddingTop: '30px',fontFamily: 'Termina-Demi'}}>  PRESENTERS</p>
                    <p class="text-center   font-bold text-grey-darker  mb-8"> Your Favourite OAPs</p>
                    <div class="container m-auto max-100">
                        <section class="  flex flex-col  "  >
                            <div class="row">
                                <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                    <div class="container max-100  m-auto  flex flex-wrap items-center justify-start">

                                    {this.state.presenters.map((presenter) => 
                                        
                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div class="overflow-hidden ">
                                                <img class="w-full" src={`https://office.coolfm.ng/${presenter.image}`} alt="Sunset in the mountains"/>
                                                <div class="p-6 flex flex-col justify-between ">
                                                    <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">{ presenter.name }</h3>
                                                    <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                        <span class="font-blue mb-2">
                                                                                                                      
                                                        </span>
                                                    </p>
                                                    <a href={`${"/oap/"+slug(presenter.name)}`} class="inline-flex text-grey-dark font-normal text-m   ">
                                                        <span >
                                                        read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    )}

                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </section>

            </div>
        )
    }
}

export default Presenters;
