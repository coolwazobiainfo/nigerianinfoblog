import React, { Component } from 'react';
import MailchimpSubscribe from "react-mailchimp-subscribe";

import './style.css';

const CustomForm = ({ status, message, onValidated }) => {

  let email;

  const submit = () =>
    email &&
    email.value.indexOf("@") > -1 &&
    onValidated({
      EMAIL: email.value
    });


  return (

    <div>
      {status === "sending" && <div style={{ color: "blue" }}>sending...</div>}
      {status === "error" && (
        <div
          style={{ color: "red" }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}
      {status === "success" && (
        <div
          style={{ color: "green" }}
          dangerouslySetInnerHTML={{ __html: message }}
        />
      )}

      <section style={{ background: '#000000',
    color: '#fff'}} className=" container max-100 p-6 m-auto antialiased font-sans w-full bg-white border  text-left text-black ">
		  <div  className="container mx-auto w-48 max-w-xl py-8 font-normal leading-normal">
		    <h3 className="font-druk tracking-wid " style={{fontFamily: 'Termina-Demi'}}>SUBSCRIBE TO OUR NEWSLETTER</h3>
		   
		    <div className="max-w-sm mt-4 sm:flex">      
		      <input type="email" style={{fontFamily: 'FreightSansProMedium-Regular'}} className="block w-full focus:outline-0 bg-white py-3 px-6 mb-2 sm:mb-0" ref={node => (email = node)} name="email" placeholder="Enter your email" required=""/>
		      <button style={{background: '#ffd300'  , color: 'black'}} className="uppercase text-sm text-white focus:outline-0 w-full font-bold sm:w-auto bg-black hover:bg-grey-darkest focus:bg-grey-light tracking-wide px-6 h-16" onClick={submit}>Subscribe</button>
		    </div>
		  </div>
      </section>

    </div>
    
  );
};

class Newsletter extends Component {
  
  render() {
  	
  	const url = "https://coolfm.us16.list-manage.com/subscribe/post?u=11b9dec20b5f95351d12ef4ff&amp;id=95379d4170";
        
    return (
       
       <MailchimpSubscribe
          url={url}
          render={({ subscribe, status, message }) => (
            <CustomForm
              status={status}
              message={message}
              onValidated={formData => subscribe(formData)}
            />
          )}
        />
         

    );
  }
}

export default Newsletter;

