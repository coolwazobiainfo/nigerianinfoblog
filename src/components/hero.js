import React, { Component } from 'react';

import './herostyle.css'; 
import moment       from 'moment';
import settings from './config';
import HeroPreloader  from './preloaders/heroloader';

class Hero extends Component {
  
     constructor(props) {

            super(props);
           
            this.state = {
              posts: [],
              loading: true,
          };       
     }
   

    componentDidMount() {
        window.performance.now();
      fetch(settings.api_url + 'allposts')
        .then(response => response.json())
        .then(data =>  this.setState({ posts: data, loading: false }) );
    }
      
   
  render() {

     const slug_0 = this.state.posts[0] ? this.state.posts[0].slug : "#"
     const slug_1 = this.state.posts[1] ? this.state.posts[1].slug : "#"
     const slug_2 = this.state.posts[2] ? this.state.posts[2].slug : "#"
     const slug_3 = this.state.posts[3] ? this.state.posts[3].slug : "#"

     const posted_at_0 = this.state.posts[0] ? this.state.posts[0].created_at : "";
     const posted_at_1 = this.state.posts[1] ? this.state.posts[1].created_at : "";
     const posted_at_2 = this.state.posts[2] ? this.state.posts[2].created_at : "";
     const posted_at_3 = this.state.posts[3] ? this.state.posts[3].created_at : "";

     const timme_0 = moment(posted_at_0).fromNow();
     const timme_1 = moment(posted_at_1).fromNow();
     const timme_2 = moment(posted_at_2).fromNow();
     const timme_3 = moment(posted_at_3).fromNow();
       
 
    return (     
       <div style={{margin: "0 auto", width: "85%", marginTop: "73px"}} className=" bg-white shadow">

       { this.state.loading ? 

          <HeroPreloader /> :
        <div>
          <section className="font-sans flex flex-col lg:flex-row mt-2 p-6 m-auto">

            <div className="boxx w-full lg:w-2/3 h-auto" style={{ background: "url(https://ik.imagekit.io/hypb55a1e/uploads/folder_1/f95e1e90e21eca9656b57e0b405f7909.jpg) top center / cover", height: "441.5px", boxShadow: "rgba(0, 0, 0, 0.46) -173px -603px 56px -219px inset" }}>
            
            <div style={{ background:"#009ee6"}} className="p-4">
                <p className="text-white font-serif text-lg"> News </p>
            </div>
            <div className=" p-6 hedd mb-8" >
                <p className="mb-4 inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase">
                    <span className="font-sans uppercase text-white ">
                        <a className="font-sans uppercase text-white font-serif font-smooth text-base" href="shows/news">News</a>
                    </span>
                </p>
                <h1 className="text-3xl text-white leading-tight antialiased">
                    <a className=" word-clap font-mont" href="#" style={{color: "white", fontStyle: "italic",
    fontWeight: "900" }}>Aniedi Effiong emerges winner of $120,000-worth TFC & Cool FM-sponsored MBA scholarship</a></h1>
                <p className=" text-white font-seg text-lg  time mt-4  antialiased"><span><span className="ion ion-md-time"></span> 4 hours ago</span>
                </p>
            </div> 

            <div className="container max-w-xl m-auto flex flex-wrap items-center justify-start disa">

              <div className="w-full md:w-1/2 lg:w-1/2  mb-8  px-3">
                <div className="overflow-hidden ">
                  <img className="w-full" src="https://stitches.hyperyolo.com/images/demo-bg.png" alt="Sunset in the mountains" />
                  <div className="flex flex-col justify-between ">
        <span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href="#">News</a></span>
                    <a className="text-grey-darkest no-underline">
                        <p for="" className=" no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> The 1975 Were a “Jeopardy! Teen Tournament” Answer But Nobody Got It Right

                        </p>
                    </a>  <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4">
                        <span style={{opacity: "0.4" }}>
                    
                        <span className="ion ion-md-time"></span> 4 hours ago</span>
                </p>
                  </div>
                </div>
              </div>

              <div className="w-full  md:w-1/2 lg:w-1/2  mb-8 px-3">
      <div className="overflow-hidden  ">
        <img className="w-full" src="https://stitches.hyperyolo.com/images/demo-bg.png" alt="Sunset in the mountains" />
        <div className="flex flex-col justify-between ">
        <span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href="#">News</a></span>
                                <a className="text-grey-darkest no-underline">
                                    <p for="" className=" no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> The 1975 Were a “Jeopardy! Teen Tournament” Answer But Nobody Got It Right

                                    </p>
                                </a>
                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{opacity: "0.4"}}><span className="ion ion-md-time"></span> 4 hours ago</span>
                            </p>

        </div>
      </div>
    </div>
            </div>

          </div>

        <br />
        <br />
        <br />
        <div style={{background:"#e6e6e6;"}}  className="w-full lg:w-1/3 flex flex-col  justify-center text-left pt-0  ml-4 move-left mb-8  "><div style={{background:"#661f85"}} className=" p-4"><p className="text-white font-serif text-lg"> Talk </p></div>
            <div className=" max-w-xl m-auto  mt-4">
                <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex flex-col px-3  height-smt ">
                    <div className="overflow-hidden  hover:shadow-raised hover:translateY-2px transition">
                       
                        <div className="flex flex-col justify-between ">  
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase  antialiased font-serif" href="#">News</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href="#" style={{color: "black"}}>Hole, Soundgarden, Estates of Tupac and Tom Petty Sue Universal Music Group Over Recordings Lost in Warehouse Fire</a></h3>

                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{opacity: "0.4"}}><span className="ion ion-md-time"></span> 4 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>
                </div>
                <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase  antialiased font-serif" href="">Music</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href="" style={{color: "black"}}>Pitchfork Music Festival 2019 Set Times Revealed</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> 7 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href="">Music</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href="" style={{color: "black"}}>Pitchfork Music Festival 2019 Set Times Revealed</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> 7 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>

                 <div  style={{borderBottom: "2px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href="">Music</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href="" style={{color: "black"}}>Pitchfork Music Festival 2019 Set Times Revealed</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> 7 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div  className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                   
                        <div className=" flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a style={{color: "#661f85"}} className="font-sans uppercase text-blue antialiased font-serif" href="">Music</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-seg" href="" style={{color: "black"}}>Pitchfork Music Festival 2019 Set Times Revealed</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> 7 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>
                </div>
                <div style={{borderBottom: "3px solid #fff"}} className="w-full lg:w-1/3 flex flex-col  justify-center text-left pl-2 pt-0 ">
            <div className=" max-w-xl m-auto  ">
                <div className="w-full  flex flex-col px-3  height-smt"><div style={{background: "#fdec00" }} className="p-4"><p className="text-black font-serif text-lg">
                Sports </p></div>
                    <div className="overflow-hidden  hover:shadow-raised hover:translateY-2px transition">
                        <div style={{background: "url(https://ik.imagekit.io/hypb55a1e/uploads/folder_1/23cb09253d8e0191d3d7a7369a35e238.jpg) 0% 0% / cover; height: 250px;"}}></div>
                        <div className="mt-4 flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a className="font-sans uppercase text-blue antialiased font-serif" href="">News</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-mont" href="" style={{color: "black"}}>Hole, Soundgarden, Estates of Tupac and Tom Petty Sue Universal Music Group Over Recordings Lost in Warehouse Fire</a></h3>

                            <p className=" text-black font-normal text-base  time antialiased font-seg mt-2 pb-4"><span style={{opacity: "0.4"}}><span className="ion ion-md-time"></span> 4 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div style={{borderBottom: "3px solid #fff"}} className="w-full  flex-col px-3  height-smt">
                    <div className="overflow-hidden hover:shadow-raised hover:translateY-2px transition mt-6">
                        <div style={{background: "url(https://ik.imagekit.io/hypb55a1e/uploads/folder_1/0454a2531769a84d097b15d340741420.jpg) 0% 0% / cover; height: 250px;" }}></div>
                        <div className="mt-4 flex flex-col justify-between ">
                            <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  "><span className="font-sans uppercase  text-green"><a className="font-sans uppercase text-blue antialiased font-serif" href="">Music</a></span></p>
                            <h3 className="text-black  text-xl  font-bold leading-normal "><a className=" word-cla antialiased font-mont" href="" style={{color: "black"}}>Pitchfork Music Festival 2019 Set Times Revealed</a></h3>

                            <p className=" text-black font-normal mt-2   time  antialiased font-seg  pb-4 text-base   "><span style={{opacity: "0.4"}}><span className="ion ion-md-time "></span> 7 hours ago</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
 
          </section>

          <section className="font-sans  m-auto flex flex-col lg:flex-row justify-center my-8">
            <div className="w-full   m-auto mb-6 lg:mb-0">
              <div id="full-on" className="container max-100 m-auto  flex flex-wrap items-center justify-start">
                <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                        <div className="overflow-hidden bg-white h-auto  hover:shadow-raised hover:translateY-2px transition">
                            <div className="h-64" style={{ background: "url('https://ik.imagekit.io/hypb55a1e/uploads/folder_1/23cb09253d8e0191d3d7a7369a35e238.jpg')", backgroundSize: "cover", backgroundPosition: "top"}}>

                            </div>

                            <div className="p-2 flex flex-col justify-between ">
                                <span className="font-termina-l text-grey-dark mb-2 mt-2 uppercase text-sm tracking-mid"><a className="font-sans uppercase text-blue antialiased font-serif" href="">News</a></span>
                                <a href="text-grey-darkest no-underline">
                                    <p for="" className=" no-underline font-mont text-left text-xl leading-normal text-black block font-smooth  leading-zero word-clad "> The 1975 Were a “Jeopardy! Teen Tournament” Answer But Nobody Got It Right

                                    </p>
                                </a>
                         

                            </div>
                        </div>

                    </div>

              </div>
            </div>
          </section>
       </div>
      }
      </div>

    );
   
  }
  
}

export default Hero;

