import React, { Component } from 'react';
import axios from 'axios';
import settings from './config';
import { slug } from '../helpers';
import _ from 'lodash';

class Search extends Component {

    constructor() {
        super();
        this.state = { 
            query: '',
            result: {}
        }
        this.handleQueryChange = this.handleQueryChange.bind(this);
        this.search = _.debounce(this.search, 500);
    }
    
    handleQueryChange(e){
        e.preventDefault();
        let query = e.target.value;
        this.search(query);
        this.setState({query});
    } 

    search = (query) => {
        if(query.trim().length === 0){
            this.setState({result: {}});
            return;
        }
        axios.get(settings.api_url+'search',{
            params:{
                query
            }
        })
            .then(res => {
                this.setState({result: res.data});
                console.log(res.data);
            })
            .catch(err => {
                console.log(err.response);
            })
    }


    closeSearch(e) {
        e.preventDefault();
        this.props.fromChildToParentCallback(false);
        this.setState({ query: '' })
        document.querySelector('body').style.overflow = 'auto';
    }

    

    renderPosts(){
        let posts = [];
        if(this.state.result.posts){
            posts = this.state.result.posts.data.map((item, index) => {
                return (
                    <li key={index} className="search-result-text">
                    <a href={`${'/post/'+ item.slug}`}>{item.title}</a>
                </li>
                )
            })
        }
        return posts.length === 0 ? (<li className="search-result-text">No Matches Found</li>) : posts;
    }

    

    renderSearch() {
        if (this.props.showSearchBar) {
            document.querySelector('body').style.overflow = 'hidden';
            return (
                <div className="search-container" style={{overflow: 'auto', fontSize: '1.5em'}}>
                    <a onClick={(e) => this.closeSearch(e)} href="#cancel" className="cancel">
                        <i className="ion-ios-close"></i>
                    </a>
                    <div className="input-container text-center">

                        <input onChange={this.handleQueryChange} placeholder="Search" />
                        <span className="all-result">{this.state.query.trim().length > 0 ? <span> All Result <i className="fa fa-angle-right"></i></span> : ""}</span>
                    </div>
                    <div style={{display: this.state.query.trim() === 0 ? 'none' : 'flex'}} className="container max-100 m-auto  flex flex-wrap justify-start">                         
                       
                        <div className="w-full flex flex-col mb-8 px-3">
                        
                            <ul className="search-result">
                                {this.renderPosts()}
                            </ul>

                        </div> 
                       
                    </div>
                </div>
            );
        }
    }

    render() {
        return (

            <div>
                {this.renderSearch()}
            </div>

        );

    }

}

export default Search;

