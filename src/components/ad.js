import React, { Component } from 'react';
import Topheader  from './topheader';
import data from './../data';

class Ad extends Component {

componentDidMount() {
      window.performance.now();
    }

  render() {

    
    return (

       
    <div className="order-last w-full lg:w-1/3 flex-col lg:text-left p-2 ">
        <Topheader/>
        <div style={{height: "auto"}} className="shadow p-3 bg-white mt-2">
          <p style={{fontSize: "1.7em"}} className="mdi-podcast p-3 text-grey-light"></p>
          <p style={{fontSize: "1.7em", fontFamily: 'FreightSansProMedium-Regular'}} className="p-3 text-lg font-bold text-black">Missed any of your favourite shows?</p>
          <p style={{paddingRight: "1px !important", fontFamily: 'FreightSansProMedium-Regular'}} className="p-3 text-lg mb-4">No problem. All our podcasts are on soundcloud</p>
          <p style={{background: '#f50'}} className="uppercase p-3 bg-black  text-white font-bold text-center ">Listen                             
            
          </p>
        </div>
        <div style={{height: "auto"}} className=" shadow p-3 bg-white mt-2">
         
          <p style={{fontSize: "2em", color: 'black'}} className="p-3 text-lg font-mont antialiased italic">Follow us on social media</p>
          <p className="p-3 text-lg mb-2 font-mont antialiased" style={{color: 'black'}} >Follow our journey and stay updated with the latest</p>
          <p style={{background: "#e4405f", fontSize: "1em"}} className="p-3 bg-black text-white font-bold text-center mb-4">
            <span className="ion ion-logo-instagram"></span><a href={data.location[data.currentLocation].socials.instagram} style={{color: "white", paddingLeft: "3px"}}> Follow</a>                           
          </p>
          <p style={{background: "#3b5999", fontSize: "1em"}} className="p-3 bg-black text-white font-bold text-center mb-4">
            <span className="ion ion-logo-facebook"></span><a href={data.location[data.currentLocation].socials.facebook} style={{color: "white", paddingLeft: "3px" }}> Like</a>                    
          </p>
          <p style={{background: "#cd201f", fontSize: "1em"}} className="p-3 bg-black text-white font-bold text-center mb-4">
            <span className="ion ion-logo-youtube"></span> <a href={data.location[data.currentLocation].socials.youtube} style={{color: "white" }}> Subscribe</a>                        
          </p>
          <p style={{background: "#55acee", fontSize: "1em"}} className="p-3 bg-black text-white font-bold text-center mb-4">
            <span className="ion ion-logo-twitter"></span> <a href={data.location[data.currentLocation].socials.twitter} style={{color: "white"}}> Follow</a>                         
          </p>
          <p style={{background: "#25D366", fontSize: "1em"}} className="p-3 bg-black text-white font-bold text-center mb-4">
            <span className="ion ion-logo-whatsapp"></span> <a style={{color: "white" }} href="#"> Chat</a>                       
          </p>
      </div>
    </div>
    );
  }
}

export default Ad;

