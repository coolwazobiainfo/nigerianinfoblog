import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import settings from './config';
import loc from './../data';

class ShowSchedule  extends Component {

  constructor(props){
    super(props);
    this.state = {
      schedules: {},
      activities: {}
    }
  }
  componentDidMount () {

      window.performance.now();
  

       fetch(settings.api_url +'schedules')
      .then(response => response.json())
      .then(data => { 
        let schedules = _.groupBy(data, function(city) {
            return _.capitalize(city.state);
          });
        let currentLocation = loc.currentLocation;
        let activities = _.groupBy(schedules[currentLocation], function(city){
          return city.period == null ? 'Weekday' : _.capitalize(city.period);
        })
        this.setState({ schedules, activities}); });  

 }

 formatTime(time){
   return moment(time, 'hh:mm').format("hh:mm A");
   
 }

  render() {
    return (
          
      <div className="tab_container tab" >
      <input id="tab1" type="radio" name="tabs" defaultChecked/>
      <label htmlFor="tab1"><span style={{fontFamily: 'Termina-Demi'}}>Monday - Friday</span></label>
      <input id="tab2" type="radio" name="tabs"/>
      <label htmlFor="tab2"><span style={{fontFamily: 'Termina-Demi'}}>Saturday</span></label>
      <input id="tab3" type="radio" name="tabs"/>
      <label htmlFor="tab3"><span style={{fontFamily: 'Termina-Demi'}}>Sunday</span></label>
      <section  id="content1" className="tab-content">

      <div className="row">
        <div className="w-full w-1/2  m-auto mb-2 lg:mb-0">
          <div className="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
            {this.state.activities.Weekday ? this.state.activities.Weekday.map((item, index) => (
              <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
                <div style={{ backgroundSize: '100%', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'top', backgroundImage: `url(${item.image})`}} className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                  <div className="p-6 flex flex-col justify-between ">
                    <p className="inline-flex items-center"></p>
                    <h3 style={{borderBottom: "1px solid #fff", paddingBottom: "1em"}}  className="text-white text-sm mb-2 f-m uppercase tracking-wid ">{item.title}</h3>
                    <span style={{opacity: ".8"}} className="text-white">Weekdays</span>
                    <h3 style={{fontSize: "1.5em"}}   className="text-white text-xl mb-2 f-m mt-2 ion ion-md-time">  {this.formatTime(item.start)} - {this.formatTime(item.end)} </h3>
                  </div>
                </div>
              </div>
            )): ''
            
            }
            
            </div></div></div>
          </section>
          <section id="content2" className="tab-content">
            <div className="row">
              <div className="w-full w-1/2  m-auto mb-2 lg:mb-0">
                <div className="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
                {this.state.activities.Saturday ? this.state.activities.Saturday.map((item, index) => (
                  <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
                    <div style={{background: `url(${item.image})`, backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: "center"}} className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                      <div className="p-6 flex flex-col justify-between ">
                        <p className="inline-flex items-center"></p>
                        <h3 style={{borderBottom: "1px solid #fff", paddingBottom: "1em"}}  className="text-white text-sm mb-2 f-m uppercase tracking-wid ">{item.title}</h3>
                        <span style={{opacity: ".8"}} className="text-white">Saturdays</span>
                        <h3 style={{fontSize: "1.5em"}} className="text-white text-xl mb-2 f-m mt-2  ">{this.formatTime(item.start)} - {this.formatTime(item.end)} </h3>
                      </div>
                      <span style={{opacity: ".8", paddingRight: "1em",  float: "right"}} className="text-white"></span>
                    </div>
                  </div>)) : ''}
                  
                  </div></div></div>
                </section>
                <section id="content3" className="tab-content">
                  <div className="row">
                    <div className="w-full w-1/2  m-auto mb-2 lg:mb-0">
                      <div className="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
                      {this.state.activities.Sunday ? this.state.activities.Sunday.map((item, index) => (
                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
                          <div style={{background: `url(${item.image})`, backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: "center"}} className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                            <div className="p-6 flex flex-col justify-between ">
                              <p className="inline-flex items-center"></p>
                              <h3 style={{borderBottom: "1px solid #fff", paddingBottom: "1em"}}  className="text-white text-sm mb-2 f-m uppercase tracking-wid ">{item.title}</h3>
                              <span style={{opacity: ".8"}} className="text-white">Saturdays</span>
                              <h3 style={{fontSize: "1.5em"}} className="text-white text-xl mb-2 f-m mt-2  ">{this.formatTime(item.start)} - {this.formatTime(item.end)} </h3>
                            </div>
                            <span style={{opacity: ".8", paddingRight: "1em",  float: "right"}} className="text-white"></span>
                          </div>
                        </div>)) : ''}                     
                    </div>
                  </div>
                </div>
             </section>
         </div>
    )
  }
}

export default ShowSchedule;

  
    

