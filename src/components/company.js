import React, { Component } from 'react';

import Menu from './menu';
import Newsletter from './newsletter';
import settings from './config';
import HelmetMeta from './helmet';
import Footer from './footer';

class Company extends Component {

  componentDidMount() {
      window.performance.now();
    }


  render() {
    return (
       
      <div>
        <HelmetMeta 
          title="Nigeria Info FM | News, Talk & Sports Station!" 
          description="Nigeria Info FM | News, Talk & Sports Station" 
          canonical={settings.app_url}
        />
        <Menu />
        
           <div role="main">

              <section>
                <div className="region region-content">
                 
                  <span property="schema:name" content="Careers" className="hidden"></span>
                    <div className="content">
                        <div>
                          <div className="field--item">
                            <div>
                              <section className="hero-block">
                                <div className="hero-gradient"></div>
                                  <div className="container " >
                                    <div className="inner-line">
                                      <div className="row">
                                        <div className="col-sm-8">
                                          <p className="sub-heading sub-heading visible-lg visible-md uppercase tracking-wide font-bold">Our History</p>
                                          <h1 style={{fontSize: '3em', textTransform: 'capitalize'}}>Almost a dacade of delivering masterclass in radio broadcasting and beyond</h1>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </section>
                                <div className="field--item">
                                  <div className="map-bg">
                                    <div className="field field--name-field-flip-flop-block field--type-entity-reference-revisions field--label-hidden field--item">
                                      <div className="flip-flop-block-module" >
                                        <div className="module-top">
                                          <div className="inner-line">
                                            <div className="row ">
                                              <div className="col-md-5 left-text-module ">
                                                <p className="sub-heading mb-2 font-blue sub-heading  uppercase tracking-wide font-bold">Start your journey</p>
                                                  <h3>
                                                    <div style={{lineHeight: '1.2'}} className="mt-4 field field--name-field-flip-flop-top-left-heading field--type-string field--label-hidden field--item font-2 ">Jumpstart your journey into radio broadcasting by applying for our graduate internship program</div>
                                                  </h3>
                                                  <p style={{fontSize: '1.3em', lineHeight: '1.3'}} className="mt-4 dd-sliding-u-l-r font-bold text-black">Apply here </p>
                                              </div>
                                              <div className="col-md-7 right-wide-module bg_cover" style={{background:"url('https://res.cloudinary.com/xyluz/image/upload/v1550975866/cfm_gnlm6q.jpg') no-repeat right center"}}>
                                                <div className="module-gradient"></div>
                                                  <div>
                                                    <h3 className="content white col-md-8">
                                                      <p className="sub-heading mb-4 text-grey-lightest sub-heading  uppercase tracking-wide font-bold">BEHIND THE RADIO</p>
                                                      <div style={{lineHeight: '1.2'}} className="field field--name-field-flip-flop-top-r-heading field--type-string field--label-hidden field--item font-2">Discover interesting facts about the faces behind the voice. Get to know our award-winning presenters</div>
                                                      <p>
                                                        <a className="btn btn-link gray" href="working-at-ge/sales.html">
                                                          <span style={{color: '#f9f9f9', fontSize: '1.3em', textTransform: 'initial'}} className="mt-4 dd-sliding-u-l-r font-bold text-white">Learn more</span>
                                                        </a>
                                                      </p>
                                                    </h3>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div className="field field--name-field-facts field--type-entity-reference-revisions field--label-hidden field--item">
                                          <section  className="paragraph facts-module module paragraph--type--paragraph-facts paragraph--view-mode--default">
                                            <div className="container">
                                              <div className="inner-line">
                                                <div className="row">
                                                  <div className="field field--name-field-fact field--type-entity-reference-revisions field--label-visually_hidden">
                                                    <div className="field__items">
                                                      <div className="field--item">
                                                        <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
                                                          <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
                                                            <div className="row">
                                                              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2 ">
                                                                <span style={{fontSize: '2.5em', color: '#c1bfbf'}} className="ion-ios-cellular"></span>
                                                              </div>
                                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
                                                                    <p className="copy-cta">Cities where we have stations (in Nigeria)</p>
                                                                  </div>
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
                                                                    <span className="text-blue">4</span>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div className="field--item">
                                                          <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
                                                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
                                                              <div className="row">
                                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2">
                                                                 <span style={{fontSize: '2.5em', color: '#c1bfbf'}} className="ion-ios-people"></span>
                                                                </div>
                                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
                                                                    <p className="copy-cta">Our national monthly listeners (in millions)</p>
                                                                  </div>
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
                                                                    <span className="text-blue">7.8M</span>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <div className="field--item">
                                                          <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
                                                            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
                                                              <div className="row">
                                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2 ">
                                                                 <span className="ion-ios-globe" style={{fontSize: '2.5em', color: '#c1bfbf'}} ></span>
                                                                  </div>
                                                                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
                                                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
                                                                      <p className="copy-cta">Total Digital Audience (in millions)</p>
                                                                    </div>
                                                                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
                                                                      <span className="text-blue">3.5M</span>
                                                                    </div>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div className="m-auto text-center">
                                                  <button className="bg-blue text-white p-4 uppercase font-bold mt-8 rounded"><span className="ion-ios-download"></span> Download brand profile</button>
                                                </div>
                                              </section>
                                            </div>
                                          </div>
                                        </div>
                                        <br />
                                        <section  className="font-sans flex flex-col lg:flex-row mt-8">
                                          <div style={{background:'url(https://res.cloudinary.com/xyluz/image/upload/v1550976027/serge_jz5mut.jpg)', height: '700px', backgroundSize: 'cover', backgroundPosition: 'center'}} className="w-full lg:w-1/2"></div>
                                          <div className="w-full lg:w-1/2 flex flex-col  justify-center text-left p-8 bg-blue">
                                        
                                            <p style={{fontFamily: 'Lusitana', fontSize: '2.5em', lineHeight: '1.2'}} className="leading-normal mb-4  mr-6 text-white">"One thing i have learnt is with willpower, discipline, an excellent spirit and a great team nothing is impossible" </p>

                                              <h1 className="my-4 mr-6 uppercase text-grey-light leading-normal" style={{fontFamily: 'FreightSansProMedium-Regular'}}>Serge NOUJAIM, CHIEF EXECUTIVE OFFICER</h1>
                                              <h1 className="font-normal mr-6 font-bold uppercase text-white "><a target="new" href="https://www.linkedin.com/in/serge-noujaim-0ba08b154/"> <span style={{fontSize: '2.5em'}} className="text-white ion-logo-linkedin"></span> </a></h1>
                                          </div>
                                        </section>
                                        <div className="field--item bg-grey-lightest">
                                          <div className="field--item">
                                            <section  className="paragraph content-block list-with-images-module module paragraph--type--list-w-image paragraph--view-mode--default">
                                              <div style={{width: '90%', margin: '0 auto'}}>
                                                <div className="inner-line">
                                                  <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                                    <div className="list-with-images-module-item">
                                                      <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                                        <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976122/femi_plpvhj.jpg" width="500" height="222" alt="img "  />
                                                      </div>
                                                      <div className="list-with-images-item-details">
                                                        <div className=" content-container ">
                                                          <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                                            <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Femi Obong-Daniels</div>
                                                          </h3>
                                                          <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                                           <p className="p-4">
                                                              <a className="uppercase font-bold" href="#">
                                                                <span className="sliding-u-l-r " style={{color: '#ffffff',marginLeft: '1.08em'}}>Head of stations/Sports </span>
                                                              </a>
                                                            </p>
                                                          </div>
                                                          <p>
                                                            <a target="new" href="https://www.linkedin.com/in/olufemi-obong-daniels-610917b6/">
                                                              <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                                            </a>
                                                          </p>                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images">
                                                  <div className="list-with-images-module-item">
                                                    <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                                      <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976123/alero_duqujs.jpg" width="500" height="222" alt="img "  />
                                                    </div>
                                                    <div className="list-with-images-item-details">
                                                      <div className="content-container ">
                                                        <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                                          <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Alero Eghagha</div>
                                                        </h3>
                                                         <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                                           <p className="p-4">
                                                              <a className="uppercase font-bold" href="#">
                                                                <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Deputy H.O.S / Head of Music </span>
                                                              </a>
                                                            </p>
                                                          </div>
                                                          <p>
                                                            <a target="new" href="https://www.linkedin.com/in/alero-eghagha-010a758/">
                                                              <span style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                                            </a>
                                                          </p>                                                        
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                                  <div className="list-with-images-module-item">
                                                    <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                                      <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976132/esther_gvuv7b.jpg" width="500" height="222" alt="img "  />
                                                    </div>
                                                    <div className="list-with-images-item-details">
                                                      <div className=" content-container ">
                                                        <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                                          <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Esther Oyegue</div>
                                                        </h3>
                                                      <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                                        <p className="p-4">
                                                          <a className="uppercase font-bold" href="#">
                                                            <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Deputy H.O.S / News Manager </span>
                                                          </a>
                                                        </p>
                                                      </div>
                                                      <p>
                                                        <a target="new"  href="https://www.linkedin.com/in/esther-oyegue-b3389730/">
                                                          <span  style={{fontSize: '2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                                        </a>
                                                      </p>                                                    
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            
                                        <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                          <div className="list-with-images-module-item">
                                            <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                              <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976120/bl_f4d8ig.jpg" width="500" height="222" alt="img "  />
                                            </div>
                                            <div className="list-with-images-item-details">
                                              <div className=" content-container ">
                                                <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                                  <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Blessing Olomu</div>
                                                </h3>
                                              <div style={{background: '#0a833d',marginRight: '-2em', marginLeft: '-2em'}}>
                                                <p className="p-4">
                                                  <a className="uppercase font-bold" href="#" font-bold="">
                                                    <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Head of Stations, PH </span>
                                                  </a>
                                                </p>
                                              </div>
                                              <p>
                                                <a href="#">
                                                  <span style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                                </a>
                                              </p>                                                                  
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                   
                                   
                                 
                                    <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                      <div className="list-with-images-module-item">
                                        <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                          <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976130/iso_kh8fma.jpg" width="500" height="222" alt="img "  />
                                        </div>
                                        <div className="list-with-images-item-details">
                                          <div className=" content-container ">
                                            <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                              <div style={{ fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Iso Ijah</div>
                                            </h3>
                                            <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                              <p className="p-4">
                                                <a className="uppercase font-bold" href="#">
                                                  <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Head of OAPs, PH</span>
                                                </a>
                                              </p>
                                            </div>
                                            <p>
                                              <a target="new"  href="https://www.linkedin.com/in/iso-ijah-306bb383/">
                                                <span  style={{fontSize:'2em', color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                              </a>
                                            </p>                                          
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                    <div className="list-with-images-module-item">
                                      <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                        <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976120/eo_vkbmal.jpg" width="500" height="222" alt="img "  />
                                      </div>
                                      <div className="list-with-images-item-details">
                                        <div className=" content-container ">
                                          <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                            <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Emmanuel Oyewole</div>
                                          </h3>                                                                  
                                          <div style={{background: '#0a833d',marginRight: '-2em', marginLeft: '-2em'}}>
                                            <p className="p-4">
                                              <a className="uppercase font-bold" href="#">
                                                <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Head of Administration</span>
                                              </a>
                                            </p>
                                          </div>
                                          <p>
                                            <a Target="new"  href="https://www.linkedin.com/in/emmanuel-oyewole-1416645b/">
                                              <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                            </a>
                                          </p>                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                  <div className="list-with-images-module-item">
                                    <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                      <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976135/kp_x0hwev.jpg" width="500" height="222" alt="img "  />
                                    </div>
                                    <div className="list-with-images-item-details">
                                      <div className=" content-container ">
                                        <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                          <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Kolapo Oladapo</div>
                                        </h3>                      
                                        <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                          <p className="p-4">
                                            <a className="uppercase font-bold" href="">
                                              <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>Head Of Digital</span>
                                            </a>
                                          </p>
                                        </div>
                                        <p>
                                          <a target="new" href="https://www.linkedin.com/in/kolapooladapo/">
                                            <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                          </a>
                                        </p>
                                      
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                <div className="list-with-images-module-item">
                                  <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                    <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976114/alex_koh8er.jpg" width="500" height="222" alt="img "  />
                                  </div>
                                  <div className="list-with-images-item-details">
                                    <div className=" content-container ">
                                      <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                        <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Alex Igene</div>
                                      </h3>                        
                                      <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                        <p className="p-4">
                                          <a className="uppercase font-bold" href="">
                                            <span className="sliding-u-l-r " style={{color: '#ffffff',marginLeft: '1.08em'}}>Traffic Team lead</span>
                                          </a>
                                        </p>
                                      </div>
                                      <p>
                                        <a target="new" href="https://www.linkedin.com/in/alex-igene-b512a3b9/">
                                          <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                        </a>
                                      </p>                      
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                  <div className="list-with-images-module-item">
                                    <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                      <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976107/02_jzrpqa.jpg" width="500" height="222" alt="img "  />
                                    </div>
                                    <div className="list-with-images-item-details">
                                      <div className=" content-container ">
                                        <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                          <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Ibrahim Okubena</div>
                                        </h3>
                                        <div style={{background: '#0a833d',marginRight: '-2em', marginLeft: '-2em'}}>
                                          <p className="p-4">
                                            <a className="uppercase font-bold" href="" font-bold="">
                                              <span className="sliding-u-l-r " style={{color: '#ffffff',marginLeft: '1.08em'}}>Head of Control</span>
                                            </a>
                                          </p>
                                        </div>
                                        <p>
                                          <a target="new" href="https://www.linkedin.com/in/ibrahim-okubena-b8791195/">
                                            <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                          </a>
                                        </p>                                      
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                                <div className="list-with-images-module-item">
                                  <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                    <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976112/ddd_fbo8gi.jpg" width="500" height="222" alt="img "  />
                                  </div>
                                  <div className="list-with-images-item-details">
                                    <div className=" content-container ">
                                      <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                        <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Anjorin Temitope</div>
                                      </h3>
                                      <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                        <p className="p-4">
                                          <a className="uppercase font-bold" href="" font-bold="">
                                            <span className="sliding-u-l-r " style={{color: '#ffffff', marginLeft: '1.08em'}}>HEAD OF RECONCILIATION</span>
                                          </a>
                                        </p>
                                      </div>
                                      <p>
                                        <a className="" target="new" href="https://www.linkedin.com/in/david-oyekunle-64271396/">
                                          <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                        </a>
                                      </p>                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                              <div className="list-with-images-module-item">
                                <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                  <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976126/tj_ivfb7p.jpg" width="500" height="222" alt="img "  />
                                </div>
                                <div className="list-with-images-item-details">
                                  <div className=" content-container ">
                                    <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                      <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Aigbodion Godfrey</div>
                                    </h3>                              
                                    <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                      <p className="p-4">
                                        <a className="uppercase font-bold" href="" font-bold="">
                                          <span className="sliding-u-l-r " style={{color: '#ffffff',marginLeft: '1.08em'}}>Research Executive</span>
                                        </a>
                                      </p>
                                    </div>
                                    <p>
                                      <a className="" target="new" href="https://www.linkedin.com/in/godfrey-aigbodion-049452172/">
                                        <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                      </a>
                                    </p>                                  
                                </div>
                              </div>
                            </div>
                          </div>

                          <div className="col-lg-4 col-md-4  col-sm-4  col-xs-12 list-with-images-wrapper list-with-bottom-images ">
                            <div className="list-with-images-module-item">
                              <div className="field field--name-field-list-w-image-item-image field--type-image field--label-hidden field--item">
                                <img className="img-responsive" src="https://res.cloudinary.com/xyluz/image/upload/v1550976110/com_tsykqp.jpg" width="500" height="222" alt="img "  />
                              </div>
                              <div className="list-with-images-item-details">
                                <div className=" content-container ">
                                  <h3 className="list-with-images-item-heading   list-with-bottom-image-heading ">
                                    <div style={{fontSize: '1.5em'}} className="field field--name-field-list-w-image-item-heading field--type-string field--label-hidden field--item font-bold text-black">Oyekunle David</div>
                                  </h3>
                                  <div style={{background: '#0a833d', marginRight: '-2em', marginLeft: '-2em'}}>
                                    <p className="p-4">
                                      <a className="uppercase font-bold" href="" font-bold="">
                                        <span className="sliding-u-l-r " style={{color: '#ffffff',marginLeft: '1.08em'}}>Team Lead Compliance & Recovery</span>
                                        </a>
                                      </p>
                                    </div>
                                    <p>
                                      <a className="" href="#">
                                        <span  style={{fontSize:'2em',color: '#b3b3b3'}} className="ion-logo-linkedin hover-link"></span>
                                      </a>
                                    </p>
                                  
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>
                      </div></section>
                    </div>
                  </div>                      
                </div>
               </div>
              </div>
            </div>
          </div>                      
        </div>
        <br />
          <br />
            <br />
      </section>

    </div>
  
    
  </div>
    )
  }
}

export default Company;
