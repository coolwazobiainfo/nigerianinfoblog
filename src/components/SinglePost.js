import React, { Component } from 'react';
import { SimpleShareButtons } from "react-simple-share";
import Plyr from 'react-plyr';
import moment       from 'moment';
import settings from './config';
import './style.css';
import Ad from './ad';
import Menu from './menu.js';
import Newsletter from './newsletter';
import Footer     from './footer';
import Interviews      from  './interviews';
import Mobileswitch      from  './mobileswitch';
import HelmetMeta from './helmet';
import HeroPreloader  from './preloaders/heroloader';
import { slug } from './../helpers';


export default class Post extends Component {

    
      constructor(props) {
           
            super(props);

		    this.state = {
		      post:     [],
		      loading: true,

		    };
		    
       }



	  componentDidMount( ) {

	  	window.performance.now();

		const { id } = this.props.match.params;
				
	    fetch(settings.api_url+'posts/' + id)
	      .then(response => response.json())
	      .then(data =>  this.setState({ post: data, loading: false }) );
	  }

	
    render() {


       const { post } = this.state;
    

      
       const createMarkup = () => { 
       	         return {__html: post.content }; 
       	}
        
     
        
        const padd = {
          paddingBottom : 10,
          paddingTop    : 5
        }

    
        const cat_id =  post.category ? post.category.id : post.id 

        const timme = moment(post.created_at).fromNow();
        const category_name = post.category ? post.category.name.toLowerCase().replace(/ /g, "-") : '';

        return ( 

			<div>
					<HelmetMeta 
							title={ post.category ? "Nigeria Info FM | " + post.category.name + " - " + post.title : "Nigeria Info FM | News, Talk & Sports Station - Post" }   
							description={post.excerpt} 
							image={ post.image } 
							canonical={settings.app_url+`post/`+post.slug}
						/>   
	  
			    <Menu/> 
			    
			
                <div className="bg-grey-lightest  m-auto">
                <section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center my-8">
  
			     <div style={{height: "fit-content"}} className="container  bg-white max-100 mt-2 ml-2 border mb-4">
			        <div className="w-full sm:w-1/1  px-6 pt-6 text-left flex flex-col justify-center" style={{paddingTop: '50px'}}>

			          <p className="inline-flex text-grey-dark font-normal text-sm mb-2 ">
			            <span className="font-bold text-green uppercase tracking-wid ">
			              <a style={{fontFamily:'FreightSansProMedium-Regular !important', color: 
			              'green'}} href={"/shows/"+category_name}>{ post.category ? post.category.name : post.id }</a>
			            </span>
			          </p>
			         
			          <h2 style={{fontFamily: "Termina-Demi !important"}} className="Title uppercase text-black">{post.title}</h2>
			        
			          <p className="text-grey-dark leading-normal one-p-1  font-lora mb-6">{post.excerpt}</p> 

			          <div className="inline-flex items-center" style={padd}>
	                    <SimpleShareButtons whitelist={["Facebook", "Twitter", "LinkedIn", "Google+", "Pinterest"]}/>
	                  </div> 

			        <div> 
			          <ul className="font-sans list-reset container  text-grey-darkest shadow p-1 bg-grey-lightest ">
			            <li className="inline-block border-b border-grey-light flex justify-between items-center py-4">
			              <div className="flex items-start w-1/1">
			                   <div className="w-10 h-10 rounded mr-3">
				                  <div className="rounded-full h-10 max-100  p-1">
				                    <img className="rounded-full" src="https://res.cloudinary.com/xyluz/image/upload/v1550573479/admin_fli6yl.png" alt="editor"/>
				                  </div>
			                   </div>
			                <div className="flex-1 overflow-hidden">
			                  <div>
			                    <span className="font-bold">admin</span>
			                    <span className="text-grey-dark text-xs">@admin___</span>
			                  </div>
			                  <p className="text-black leading-normal">Published {timme}</p>
			                </div>
			              </div>    
			            </li>
			          </ul>
					  
			        </div>
					<div> 
					{
						this.state.loading ?

							<HeroPreloader />
						
						: !post.video ?
					
					
					<img width="100%" src={"https://ik.imagekit.io/hypb55a1e/ngi" + post.image } alt={post.title}/>
						:

							<Plyr
							type="youtube" // or "vimeo"
							videoId={post.video}
						/>
						
					
					}
					</div>
			          <p className="text-grey-darkest mb-6 leading-normal one-p-1 mt-4" dangerouslySetInnerHTML={createMarkup()}>

					 

					  </p>

					 
					 
			          <div className="inline-flex items-center" style={padd}>
	                 share: <SimpleShareButtons whitelist={["Facebook", "Twitter", "LinkedIn", "Google+", "Pinterest"]}/>
	              </div>


                { post.review_score ?
			     <div className="bg-grey-lightest mb-4 text-center"> 

						<p className="inline-flex text-blue font-bold text-sm mb-4 uppercase">
					       <span style={{backgroundColor: "#124fba",borderRadius: "0px 0px 13px 13px"}}  className="font-bold text-gren but tracking-wide">
							verdict
							 </span> 
			             </p>
			      <p style={{fontSize: "4em", fontFamily: "'FreightSansProMedium-Regular' !important"}}  className="mb-6 leading-normal one-p-1 mt-4 font-druk text-center text-black">{post.review_score}</p>
			    </div> : "" 
               }
			      </div>    
			    </div>

             <Ad/>

             </section>
             </div>

			 
             <div className="mb-8 bg-white p-6" style={{ width: "64%", margin: "0 auto"}}>
	           <div style={{ width: "100%", margin: "0 auto" }}>
			     <div id="disqus_thread">
			     </div>
               </div>
		    </div>
		    <Mobileswitch/>
		    <Newsletter/>
	        
             <Footer/> 
	       </div>

        );
    }
}

