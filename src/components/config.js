let platform = typeof window !== 'undefined' ? window.location.host : '127.0.0.1';

let settings = {};

settings.api_url = platform.match(/(127.0.0.1|localhost)/) ? 'https://office.nigeriainfo.fm/' : 'https://office.nigeriainfo.fm/';

settings.app_url = platform.match(/(127.0.0.1|localhost)/) ? 'http://127.0.0.1:3000/' : 'http://127.0.0.1:3000/';


export default settings;