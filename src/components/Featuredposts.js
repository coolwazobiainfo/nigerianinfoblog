import React, { Component } from 'react';
import settings from './config';



export default class Featuredposts extends Component {

    
      constructor(props) {
           
            super(props);

		    this.state = {
		      featured: [],
		    };
       }

	  componentDidMount() {
	  	window.performance.now();
	    fetch(settings.api_url + 'promoted')
	      .then(response => response.json())
	      .then(data => this.setState({ 
	      	        featured: data,
	      	}) 
	      );
	  }


    render( ) {


         const { featured } = this.state;
         

        return (

			<section className="bg-white py-4 font-sans">
			          
				          <div className="container container max-w-xl m-auto flex flex-wrap flex-col md:flex-row items-center justify-start">
				            {featured.map(feature =>
				            	
								    <div className="w-full lg:w-1/2 p-3">
								    <a href={"post/" + feature.id }>
									      <div  className="flex flex-col lg:flex-row rounded overflow-hidden h-auto lg:h-32 border ">
									         <a href={"post/" + feature.id }><img className="block h-auto w-full lg:w-48 flex-none bg-cover h-24" src={"https://office.coolfm.ng" + feature.image } alt="featured stuff"/></a>
										        <div className="bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">              
											          <div className="text-black font-semibold text-xl mb-2 leading-tight">{feature.title}</div>
											          
	    								        </div>
									      </div>
									       </a>
								    </div>
							   
				             )}
				          </div>

	        </section>              

        );
    }
}
