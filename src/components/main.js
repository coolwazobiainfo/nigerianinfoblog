import React, { Component } from 'react';
//App specific components

import Menu from './menu';
import Hero from './hero';
import NewMainBannerHome from './newMainBannerHome';
import Posts from './posts';

// import PostGroupForHomePage from './postGroupForHomePage';
import Newsletter from './newsletter';
import settings from './config';
import HelmetMeta from './helmet';
// import Interviews from './interviews';



import Footer from './footer';

class Main extends Component {

  componentDidMount() {
      window.performance.now();
    }


  render() {
    return (
       
      <div>
        <HelmetMeta 
          title="Nigeria Info FM | News, Talk & Sports Station!" 
          description="Nigeria Info FM | News, Talk & Sports Station" 
          canonical={settings.app_url}
        />
        <Menu />        
        <NewMainBannerHome />
        <Newsletter />
        <Footer />
      </div>
    );
  }
}

export default Main;
