import React, { Component } from 'react';

//App specific components

import Menu       from './menu';
import Footer     from './footer';
import settings from './config';
import HelmetMeta from './helmet';


export default class About extends Component {
    componentDidMount() {
            window.performance.now();
        }
  render() {

    return (
      <div> 
        <HelmetMeta 
            title="Nigeria Info FM | News, Talk & Sports Station! - About Us" 
            description="Nigeria Info FM | News, Talk & Sports Station" 
            canonical={settings.app_url+`about`}
        />
         <Menu/>

        <div className="margin-fix"></div>
        <section style={{background:' #fff !important', border: '1px solid #efeaea'}} className="container mx-auto p-3 ">
            <div className="mdl-tabs vertical-mdl-tabs mdl-js-tabs mdl-js-ripple-effect  ">
                <div className="mdl-grid mdl-grid--no-spacing">
                    <div className="mdl-cell mdl-cell--2-col">
                        <div className="mdl-tabs__tab-bar">
                            <a href="#tab1-panel" className="mdl-tabs__tab is-active">
                                   <span className="hollow-circle"></span>
                                   <span className="tab-text"> History</span>
                               </a>
                            <a href="#tab2-panel" className="mdl-tabs__tab">
                                    <span className="hollow-circle"></span>
                                    <span className="tab-text"> Team</span>
                                </a>
                            <a href="#tab3-panel" className="mdl-tabs__tab">
                                    <span className="hollow-circle"></span>
                                     <span className="tab-text">Digital</span>
                                </a>
                            <a href="#tab4-panel" className="mdl-tabs__tab">
                                    <span className="hollow-circle"></span>
                                     <span className="tab-text">Locations</span>
                                </a>
                            <a href="/oaps" className="mdl-tabs__tab">
                                    <span className="hollow-circle"></span>
                                     <span className="tab-text">Presenters</span>
                                </a>
                        </div>
                    </div>
                    <div className="mdl-cell mdl-cell--10-col">
                        <div className="mdl-tabs__panel is-active" id="tab1-panel">
                            <div className="container  bg-white max-w-xl mr-4 mb-4">
                                <div className="w-full sm:w-1/  px-6 pt-6 text-left flex flex-col justify-center">
                                    <div className="container mb-4">
                                        <div style={{position:'relative', overflow:'hidden'}}>
                                            <iframe className="iframe" width="100%" height="415" src="https://www.youtube.com/embed/mDCu6xAd9O0?&showinfo=0&controls=0&autoplay=1&loop=1&playlist=mDCu6xAd9O0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                    <h2 style={{fontSize: '1em', color: '#666'}} className="font-bold">History</h2>
                                    <h2 style={{marginBottom: '10px', fontSize: '3em'}} className="Title ">20 Years of Absolute Masterclass & Impeccable Music</h2>
                                    <p className="text-grey-darkest mb-6 leading-normal one-p-1 mt-1">With 42 million monthly listeners in Nigeria, 7 million digital followers and 1.2 million daily consumers of its local radio within Lagos State, Steam Global Communication has the largest reach of any radio or television outlet in Nigeria. It serves over 150 markets through 13 owned radio stations, and the company’s radio stations and content can be heard on FM, visual radio, streaming and satellite radio, on the company’s radio station websites, on the mobile app, in enhanced auto dashes, on tablets and smartphones. Out of the three major radio stations under this platform, Cool fm is the No. 1 hit music station in Nigeria. Founded in October 1998. it has the frequencies across 4 of the 6 geopolitical zones in nigeria. Cool FM 96.9 Lagos, Cool FM 95.9 Port Harcourt , Cool FM 96.9 Abuja and Cool FM 96.9 Kano. The company’s operations include radio broadcasting, online, mobile, digital and social media, live concerts and events, music research services and independent media representation. </p>
                                </div>
                            </div>
                        </div>
                        <div className="order-last w-full lg:w-1/3 flex-col  lg:text-left  ">
                        </div>
                        <div className="mdl-tabs__panel" id="tab2-panel">
                            <div className="container">
                                <h2 className="Title mb-1 text-center">Our Team</h2></div>
                            <section className="font-sans container max-w-xl m-auto flex flex-col lg:flex-row justify-center my-8">
                                <div className="w-full w-1/2 max-w-lg-2 m-auto mb-6 lg:mb-0">
                                    <div className="container max-w-xl m-auto  flex flex-wrap items-center justify-start">
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/serge.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Serge Noujaim</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">CEO</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/femi.png" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Femi Obong-Daniels</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Station (Lagos) & Group Head Of Sports</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Yough Martha Ebele</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Station (Abuja) </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Blessing Olomu</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Station (PH)</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/aboki.png" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Prince Daniel (Aboki)</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Station (Kano)</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Venus Bande</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Director, Marketing & Business Development. </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal"> Afolaranmi Igwegbe  </h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Events & Public Affairs </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/emm.png" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Emmanuel Oyewole</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Admin</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Esther Oyegue </h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of News</h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Triumph Grandeur</h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Production & Programs  </h3>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <div className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                <img className="w-full" src="/img/3173.jpg" alt="Sunset in the mountains"/>
                                                <div className="p-6 flex flex-col justify-between ">
                                                    <h3 className="text-black font-semibold  text-xl mb-2 leading-normal">Kolapo Oladapo </h3>
                                                    <h3 className="text-black font-medium  text-lg mb-2 leading-normal">Head Of Digital   </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div className="mdl-tabs__panel" id="tab3-panel">
                            <div className="container mb-4">
                                <h2 style={{fontSize: '1em', color: '#666'}} className="font-bold">Go Digital</h2>
                                <h2 style={{marginBottom: '10px', fontSize: '3em', color: '#0053b4'}} className="Title ">Take advantage of our digital community of over 7 million fans.</h2>
                                <h2 style={{fontSize: '1em', color: '#666'}} className="font-bold border-b-2">Solutions</h2>
                                <h2 className="Title  mt-4 " style={{color: '#0053b4'}}>Cool Quickie</h2>
                                <p className="text-grey-darkest mb-6 leading-normal one-p-1 ">The Cool Quickie is on one of Cool FM’s most popular online show elements. The show, which has become a fans’ favourite, involves engaging top celebrities and guests with a range of random, fast paced questions within the course of a minute, hence, Quickie. </p>
                            </div>
                            <div className="w-full w-1/2 max-w-lg-2 m-auto mb-6 lg:mb-0">
                                <div className=" max-w-xl m-auto  flex flex-wrap items-center justify-start">
                                    <iframe className="mb-4" width="100%" height="500px" src="https://www.youtube.com/embed/RQbvFDFClyU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <h2 className="  mt-4 num-lg "><span className="mdi-play"></span>68,338+    </h2>
                                    <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                        <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BcxdjbogQKU/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                            <div style={{padding:'8px'}}>
                                                <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px', padding:'28.125% 0', textAlign:'center', width:'100%'}}>
                                                    <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                </div>
                                                <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                    <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                </p>
                                            </div>
                                        </blockquote>
                                        <script async defer src="http://platform.instagram.com/en_US/embeds.js"></script>
                                        <p className="  mt-4 num-lg-sm "><span className="mdi-play"></span>25,754+ </p>
                                    </div>
                                    <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                        <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BiKb9aVgKRs/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                            <div style={{padding:'8px'}}>
                                                <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px',  padding:'28.125%' , textAlign:'center', width:'100%'}}>
                                                    <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                </div>
                                                <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                    <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                </p>
                                            </div>
                                        </blockquote>
                                        <script async defer src="http://platform.instagram.com/en_US/embeds.js"></script>
                                        <p className="  mt-4 num-lg-sm "><span className="mdi-play"></span>47,097+ </p>
                                    </div>
                                    <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                        <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BiHvQajgPbZ/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                            <div style={{padding:'8px'}}>
                                                <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px', padding:'28.125% 0', textAlign:'center', width:'100%'}}>
                                                    <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                </div>
                                                <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                    <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                </p>
                                            </div>
                                        </blockquote>
                                        <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                                        <p className="  mt-4 num-lg-sm "><span className="mdi-play"></span>50,142+ </p>
                                    </div>
                                </div>
                                <div className="container mb-4">
                                    <h2 style={{fontSize: '1em', color: '#666'}} className="font-bold border-b-2">Activations</h2>
                                    <h2 className="Title  mt-4 " style={{color: '#0053b4'}}>Cool Tour </h2>
                                    <p className="text-grey-darkest mb-6 leading-normal one-p-1 ">The Cool FM Tour is an activation tour targeted at using entertainment & innovation to penetrate the minds of a diverse community of millennial (18-34) who are core demographic of the Cool FM brand. The aim of the tour is to register our brand in the minds of the millennials by taking them through experiences that reflect the ideology and brand identity of Cool FM and also gather a database of information that can be used for future planning and accessing past performance. </p>
                                </div>
                                <div className="w-full w-1/2 max-w-lg-2 m-auto mb-6 lg:mb-0">
                                    <div className=" max-w-xl m-auto  flex flex-wrap items-center justify-start">
                                        <iframe className="mb-4" width="100%" height="500px" src="https://www.youtube.com/embed/LvEbMAi7XTk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BmSe8lpg2JC/?tagged=coolnysctoursw/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                                <div style={{padding:'8px'}}>
                                                    <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px', padding:'28.125% 0', textAlign:'center', width:'100%'}}>
                                                        <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                    </div>
                                                    <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17pX', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                        <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                    </p>
                                                </div>
                                            </blockquote>
                                            <script async defer src="http://platform.instagram.com/en_US/embeds.js"></script>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BXqdITjgHJt/?tagged=coolnysctoursw/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                                <div style={{padding:'8px'}}>
                                                    <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px', padding:'28.125% 0', textAlign:'center', width:'100%'}}>
                                                        <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                    </div>
                                                    <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                        <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                    </p>
                                                </div>
                                            </blockquote>
                                            <script async defer src="http://platform.instagram.com/en_US/embeds.js"></script>
                                        </div>
                                        <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                            <blockquote className="instagram-media" data-instgrm-permalink="https://www.instagram.com/p/BXsys43gOnJ/?tagged=coolnysctoursw/?utm_source=ig_embed" data-instgrm-version="9" style={{background:'#FFF', border:'0',  margin: '1px', padding:'0', width:'100%'}}>
                                                <div style={{padding:'8px'}}>
                                                    <div style={{background:'#F8F8F8', lineHeight:'0', marginTop:'40px', padding:'28.125% 0', textAlign:'center', width:'100%'}}>
                                                        <div style={{background:'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC)', display:'block', height:'44px', margin:'0 auto -44px', position:'relative', top:'-22px', width:'44px'}}></div>
                                                    </div>
                                                    <p style={{color:'#c9c8cd', fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px', marginBottom:'0', marginTop:'8px', overflow:'hidden', padding:'8px 0 7px', textAlign:'center', textOverflow:'ellipsis', whiteSpace:'nowrap'}}><a href="https://www.instagram.com/p/BgdkXzpgf8V/?utm_source=ig_embed" style={{color:'#c9c8cd', fontFamily:'FuturaHeavy,sans-serif', fontSize:'14px', fontStyle:'normal', fontWeight:'normal', lineHeight:'17px', textDecoration:'none'}} target="_blank" rel="noopener noreferrer">A post shared by 96.9 Cool FM Lagos (@coolfmlagos)</a> on
                                                        <time style={{fontFamily:'Arial,sans-serif', fontSize:'14px', lineHeight:'17px'}} datetime="2018-03-18T10:59:28+00:00">Mar 18, 2018 at 3:59am PDT</time>
                                                    </p>
                                                </div>
                                            </blockquote>
                                            <script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
                                        </div>
                                    </div>
                                    <section className="bg-blue text-grey-darkest py-4 lg:px-4" role="alert">
                                        <div className="items-center flex leading-none">
                                            <span className="text-center font-semibold m-auto text-white" style={{lineHeight: '1.5'}}>Hey there! Interested in any of our digital services or activations ? <a href="mailto:digital@coolfm.ng?" className="ml-2 uppercase bg-white p-1 rounded  text-black text-sm">Contact us</a></span>
                                        </div>
                                    </section>
                                </div>
                                <div className="container mb-4">
                                    <h2 style={{fontSize: '1em', color: '#666'}} className="font-bold border-b-2">Visual Radio</h2>
                                    <h2 className="Title  mt-4 " style={{color: '#0053b4'}}>Visual Radio </h2>
                                    <p className="text-grey-darkest mb-6 leading-normal one-p-1 ">With the launch of this technology, another dimension has been introduced to the country’s broadcasting landscape as radio broadcasting becomes more interactive for mobile radio listeners. Our listeners can now enjoy the premium visual experience from everything that is going on in the studio. We have succeeded in providing mobile television to millions of fans world. </p>
                                </div>
                                <div className="w-full w-1/2 max-w-lg-2 m-auto mb-6 lg:mb-0">
                                    <div className=" max-w-xl m-auto  flex flex-wrap items-center justify-start">
                                        <iframe className="mb-4" width="100%" height="500px" src="https://www.youtube.com/embed/ZxKJDhDkpQY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        <h2 className="  mt-4 num-lg "><span className="mdi-play"></span>190,992+    </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="mdl-tabs__panel" id="tab4-panel">
                            <div className="w-full sm:w-1/  px-6 pt-6 text-left flex flex-col justify-center">
                                <div className="container">
                                    <h2 className="Title mb-1">Lagos</h2>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.6953496628635!2d3.435452451410499!3d6.433165395322836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x103bf5222eafbff1%3A0x1189b5fbeac356d4!2sCool+FM+96.9!5e0!3m2!1sen!2sng!4v1534507294473" width="600" height="450" frameborder="0" style={{border:'0'}} allowfullscreen></iframe>
                                </div>
                                <div className="container">
                                    <h2 className="Title mb-1 mt-8">Abuja</h2>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.2279694184913!2d7.474027015874955!3d9.042958091257502!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0b7454088207%3A0x6b06f4bf67649131!2sCool+FM!5e0!3m2!1sen!2sng!4v1534598721849" width="600" height="450" frameborder="0" style={{border:'0'}} allowfullscreen></iframe>
                                </div>
                                <div className="container">
                                    <h2 className="Title mb-1 mt-8">Port-Harcourt</h2>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3975.655897027705!2d7.0127252514465725!3d4.829013341856249!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1069cddb009eb069%3A0x6e6db9cbfc520091!2sCool+95.9+FM!5e0!3m2!1sen!2sng!4v1534599376586" width="600" height="450" frameborder="0" style={{border:'0'}} allowfullscreen></iframe>
                                </div>
                                <div className="container">
                                    <h2 className="Title mb-1 mt-8">Kano</h2>
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.9884296832033!2d8.54809655144985!3d11.975302239150013!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x11ae81bd000117a7%3A0xa95ef1760e316032!2sCool+FM!5e0!3m2!1sen!2sng!4v1534599547055" width="600" height="450" frameborder="0" style={{border:'0'}} allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>

      

                        
                        <div className="mdl-tabs__panel" id="tab5-panel">


                                        <section style={{marginTop: '-2.5em'}} class="bg-white p-2 m-auto  ">
                                                <p class="text-center uppercase p-2 mt-8 text-xl text-black font-druk tracking-wid  ">  PRESENTERS</p>
                                            <p class="text-center   font-bold text-grey-darker  mb-8"> All the hot interviews from your  favorite shows</p>
                                                  <div class="container m-auto max-100">
                                                    <section class="  flex flex-col  "  >
                                                      <div class="row">
                                                        <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                          <div class="container max-100  m-auto  flex flex-wrap items-center justify-start">
                                                            <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="/img/frz.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Ifedayo Olarinde (Daddy Freeze)</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            The Road Show, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div>

                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="/img/new-mannie.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Emmanuel Essien (Mannie)</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            Good Morning Nigeria, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div>

                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="/img/temi.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Temilola B Akinmuda</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            Good Morning Nigeria, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div>
                                                              
                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="/img/dotun.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Oladotun Ojuolape (DO2DTUN)</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            Midday Oasis, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div><div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="img/kemi.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Kemi Smallz</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            Midday Oasis, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div><div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="img/3173.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Kaylah Oniwo</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            The Road Show, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div><div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="img/3173.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Rotimi Alakija (Dj Xclusive)</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            The Road Show, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div><div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                              <div class="overflow-hidden ">
                                                                <img class="w-full" src="img/joyceo.jpg" alt="Sunset in the mountains"/>
                                                                    <div class="p-6 flex flex-col justify-between ">
                                                                   
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Joyce Onyemuwa</h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                            <span class="font-blue mb-2">
                                                                            The Night Cafe, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>
                                                                           <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                </div>
                                                              </div>
                                                            
                                                             
                                                                  <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                    <div class="overflow-hidden   ">
                                                                      <img class="w-full" src="img/n6.jpg" alt="Sunset in the mountains"/>
                                                                        <div class="p-6 flex flex-col justify-between ">
                                                               
                                                                     
                                                                          <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Nnamdi Nwabasili (N6) </h3>
                                                                          <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide mb-2 ">
                                                                            <span class="font-blue">
                                                                            The Night Cafe, Lagos
                                                                                      
                                                                            </span>
                                                                          </p>

                                                                          <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                            <span >
                                                                             read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                      
                                                                            </span>
                                                                          </a>

                                                                        </div>
                                                                      </div>
                                                                    </div>

                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                        <div class="overflow-hidden ">
                                                                          <img class="w-full" src="img/june.jpg" alt="Sunset in the mountains"/>
                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                             
                                                                               
                                                                                    <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">June Ubi</h3>
                                                                                    <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                                      <span class="font-blue mb-2">
                                                                                      The Overnight Lounge
                                                                                                
                                                                                      </span>
                                                                                    </p>
                                                                                     <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                                      <span >
                                                                                       read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                                
                                                                                      </span>
                                                                                    </a>
          
                                                                                  </div>
                                                                          </div>
                                                                        </div>

                                                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                            <div class="overflow-hidden ">
                                                                              <img class="w-full" src="img/lavoca.jpg" alt="Sunset in the mountains"/>
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                 
                                                                                   
                                                                                        <h3 class="text-black  text-xl mb-2 font-druk uppercase leading-normal">Esther John(La Voca)</h3>
                                                                                        <p class="inline-flex text-grey-dark font-bold text-m uppercase tracking-wide  ">
                                                                                          <span class="font-blue mb-2">
                                                                                          Saturday Afternoon Groove
                                                                                                    
                                                                                          </span>
                                                                                        </p>
                                                                                         <a href="/oap" class="inline-flex text-grey-dark font-normal text-m   ">
                                                                                          <span >
                                                                                           read bio <span class="mdi-arrow-top-right text-blue"></span>
                                                                                                    
                                                                                          </span>
                                                                                        </a>
              
                                                                                      </div>
                                                                              </div>
                                                                            </div>



                                                                  </div>
                                                                </div>
                                                                </div>
                                                              </section>
                                                            </div>
                                                          </section>


                                        <section  class="bg-grey-lightest p-2 m-auto ">
                                            <p class="text-center uppercase p-2 mt-2 text-xl  text-black font-druk tracking-wid  ">  Interviews</p>
                                            <p class="text-center   font-bold text-grey-darker  mb-8"> All the hot interviews from your  favorite shows</p>
                                            <div class="container m-auto max-100">
                                              <section class="  flex flex-col  "  >
                                                <div class="row">
                                                  <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                    <div class="container max-100  m-auto  flex flex-wrap items-center justify-start">
                                                      <div class="w-full md:w-1/2 lg:w-1/3  flex flex-col mb-8 px-3">
                                                        <div class="overflow-hidden shadow bg-white ">
                                                          <div class="bg-white p-3 text-center">
                                                            <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                              <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                  GOOD MORNING NIGERIA                                                                
                                                              </span>
                                                              </p>
                                                            </div>
                                                            <div style={{ background: 'url(img/x.jpg)', height: '350px', backgroundSize: 'cover', boxshadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>
                                                            <div class="p-6 flex flex-col justify-between bg-white ">
                                                              <p class="inline-flex items-center">
                                                                <a href="" class=" cate-sp pull-up">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </p>
                                                              <div class="bg-white">
                                                                <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                              </div>
                                                            </div>
                                                            <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image" >
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>



                                                            </div> <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image" >
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>



                                                            </div>




                                                          </div><div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                        </div> 
                                                       


                                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                          <div class="overflow-hidden bg-white  shadow hover:shadow-raised hover:translateY-2px transition">
                                                           <div class="bg-white p-3 text-center">
                                                            <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                              <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                MIDDAY OASIS
                                                               </span>
                                                                                
                                                              </p>
                                                            </div>
                                                            <div style={{ background: 'url(img/d2.jpg)', height: '350px', backgroundSize: 'cover', boxShadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>
                                                            <div class="p-6 flex flex-col justify-between bg-white ">
                                                              <p class="inline-flex items-center">
                                                                <a href="" class=" cate-sp pull-up">
                                                                  <span style={{fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </p>
                                                              <div class="bg-white">
                                                                <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                              </div>
                                                            </div>
                                                            <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>



                                                            </div> <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>
                                                              </div>
                                                        </div>
                                                        <div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                        </div> 
                                                        


                                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                          <div class="overflow-hidden bg-white  shadow hover:shadow-raised hover:translateY-2px transition">
                                                           <div class="bg-white p-3 text-center">
                                                            <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                              <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                    THE SANCTUARY
                                                              </span>    
                                                                                
                                                              </p>
                                                            </div>
                                                            <div style={{ background: 'url(img/x.jpg)', height: '350px', backgroundSize: 'cover', boxShadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>
                                                            <div class="p-6 flex flex-col justify-between bg-white ">
                                                              <p class="inline-flex items-center">
                                                                <a href="" class=" cate-sp pull-up">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </p>
                                                              <div class="bg-white">
                                                                <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                              </div>
                                                            </div>
                                                            <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image" >
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>



                                                            </div> <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                              <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                <a href="" class=" cate-sp pull-up-norm">
                                                                  <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                </a>
                                                              </h4>
                                                              <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                This 10th-anniversary collection gathers loose tracks from the band.
                                                              </h4>
                                                              </div>
                                                        </div>
                                                        <div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                        </div> 
                                                        
                                                          </div>
                                                          </div>
                                                        </div>
                                                        </section>
                                                    </div>
                                                </section>

                                                <section  class="bg-grey-lightest p-2 m-auto ">
                                                    <div class="container m-auto max-100">
                                                        <section class="  flex flex-col  "  >
                                                            <div class="row">
                                                                <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                                    <div class="container max-100  m-auto  flex flex-wrap items-center justify-start">
                                                                        <div class="w-full md:w-1/2 lg:w-1/3  flex flex-col mb-8 px-3">
                                                                            <div class="overflow-hidden shadow bg-white ">
                                                                                <div class="bg-white p-3 text-center">
                                                                                    <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                                                    <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                                        THE TRUTH 
                                                                                    </span>
                                                                                    </p>
                                                                                </div>
                                                                                <div style={{ background: 'url(img/s.jpg)', height: '350px', backgroundSize: 'cover', boxShadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>
                                                                                <div class="p-6 flex flex-col justify-between bg-white ">
                                                                                    <p class="inline-flex items-center">
                                                                                        <a href="" class=" cate-sp pull-up">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </p>
                                                                                    <div class="bg-white">
                                                                                        <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                                        <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div> 
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                            <div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                                        </div> 
                                                                    


                                                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                            <div class="overflow-hidden bg-white  shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                <div class="bg-white p-3 text-center">
                                                                                    <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                                                    <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                                        ROAD SHOW                                                            
                                                                                    </span>
                                                                                    </p>
                                                                                </div>
                                                                                <div style={{ background: 'url(img/n6.jpg)', height: '350px', backgroundSize: 'cover', boxShadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>
                                                                                <div class="p-6 flex flex-col justify-between bg-white ">
                                                                                    <p class="inline-flex items-center">
                                                                                        <a href="" class=" cate-sp pull-up">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </p>
                                                                                    <div class="bg-white">
                                                                                        <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                                        <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div> 
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                            <div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                                        </div> 
                                                                        


                                                                        <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                            <div class="overflow-hidden bg-white  shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                <div class="bg-white p-3 text-center">
                                                                                    <p class="inline-flex sh text-black font-normal text-sm mb-2 ">
                                                                                    <span  class="font-bold tracking-wid uppercase text-blue  mx-auto text-center mt-2 ">
                                                                                        OVERNIGHT LOUNGE
                                                                                    </span>
                                                                                    </p>
                                                                                </div>
                                                                                <div style={{ background: 'url(img/june.jpg)', height: '350px', backgroundSize: 'cover', backgroundPosition: 'top', boxShadow: 'inset -8px -112px 92px -38px #ffffff' }}></div>

                                                                                <div class="p-6 flex flex-col justify-between bg-white ">
                                                                                    <p class="inline-flex items-center">
                                                                                        <a href="" class=" cate-sp pull-up">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </p>
                                                                                    <div class="bg-white">
                                                                                        <h3 class="text-black  text-xl mb-2 font-bold leading-normal mb-4">Beach House Are the Chainsmokers’ Type of Thing, and I Kind of Want to Die</h3>
                                                                                        <p class="text-grey-darker  text-m mb-2 font-lora leading-normal">This 10th-anniversary collection gathers loose tracks from the band’s first four years, as they explored disparate genres and song structures amid the bedlam.</p>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '.3em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 class="text-black  bg-white  text-m mb-2 font-bold respo leading-normal inline-block sub-topic" style={{ width: '80%', marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div> 
                                                                                <div class="bg-white" style={{ borderTop: '1px solid #ddd', marginTop: '1em' }}>
                                                                                    <h4 class="text-black  bg-white w-30  text-m mb-2 font-bold  leading-normal inline-block post-image">
                                                                                        <a href="" class=" cate-sp pull-up-norm">
                                                                                        <span style={{ fontSize: '1.2em' }} class="mdi-play"></span>
                                                                                        </a>
                                                                                    </h4>
                                                                                    <h4 id="respo-2" class="text-black  bg-white  text-m mb-2 font-bold  leading-normal respo  sub-topic" style={{ marginTop: '1.2em', paddingRight: '1em', fontSize: '100%' }}>
                                                                                        This 10th-anniversary collection gathers loose tracks from the band.
                                                                                    </h4>
                                                                                </div>
                                                                            </div>
                                                                            <div style={{ borderTop: 'none' }} class="bg-white border"> <a href=""><p class="text-center  p-6 text-sm font-bold sub-topic uppercase font-blue">See all </p></a></div>
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </section>

                                            <div>
                                                <section>
                                                    <div>
                                                        <div  class=" font-sans container max-100 m-auto flex flex-col md:flex-row sm:items-center ">
                                                            <div style={{ background: 'url(img/IMG_8968.jpg)', backgroundSize: 'cover', backgroundPosition: 'center', boxShadow: 'inset 0px 42px 13px 356px #2f2f2f8a', height: '700px', margin: '0 auto' }} class="w-full text-center  flex flex-col justify-center items-start px-6 py-0 md:py-8 md:px-8 lg:items-start">
                                                                <label style={{ opacity: '.8', textAlign: 'center' }} htmlFor="tagline" class="uppercase text-center mx-auto  text-white font-bold on-air">
                                                                    <span class="mdi-radio-tower"></span> NOW ON AIR 
                                                                </label>
                                                                <h1  class="mt-2 mb-4 font-druk topik m-auto text-center text-white mt-6"> DOTUN & KEMI SMALLZZ</h1>
                                                                <p class="inline-flex text-white font-normal text-lg text-center mx-auto  ">
                                                                    <span class="uppercase">MIDDAY OASIS</span>
                                                                </p>
                                                                <p class="inline-flex text-white font-normal text-lg text-center mx-auto time mt-6 ">
                                                                    <span>
                                                                        <span class="ion ion-md-time"></span> 11:30am - 12:00am 
                                                                    </span>
                                                                </p>
                                                                <p  style={{ paddingLeft: '2em', paddingRight: '2em' }} class="inline-flex text-white font-bold text-lg text-center mx-auto h-auto mt-6 btn-fr ">
                                                                    <span>
                                                                    <span class="ion-md-wifi"> STREAM LIVE</span>
                                                                    </span>
                                                                </p>
                                                                <p  style={{ paddingLeft: '2em', paddingRight: '2em' }} class="inline-flex text-white font-bold text-lg text-center mx-auto h-auto mt-6 btn-fr ">
                                                                    <span>
                                                                    <span class="ion-md-play"> LISTEN LIVE</span>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                                              <div class="tab_container tab ">
                                                                <input id="tab1" type="radio" name="tabs" checked/>
                                                                  <label htmlFor="tab1">
                                                                    <span>Monday - Friday</span>
                                                                  </label>
                                                                  <input id="tab2" type="radio" name="tabs"/>
                                                                    <label htmlFor="tab2">
                                                                      <span>Saturday</span>
                                                                    </label>
                                                                    <input id="tab3" type="radio" name="tabs"/>
                                                                      <label htmlFor="tab3">
                                                                        <span>Sunday</span>
                                                                      </label>
                                                                      <section  id="content1" class="tab-content">
                                                                        <div class="row">
                                                                          <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                                            <div class="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm mb-2 f-m uppercase tracking-wid ">good morning nigeria  </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2 ion ion-md-time">  3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }} class="text-white text-sm mb-2 f-m uppercase tracking-wid ">Oga Madam Office </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ion ion-md-time "> 3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm mb-2 f-m uppercase tracking-wid">Oga Madam Office </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2 ion ion-md-time "> 3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm mb-2 f-m uppercase tracking-wid">Oga Madam Office </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2 ion ion-md-time "> 3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm mb-2 f-m uppercase tracking-wid ">Oga Madam Office </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ion ion-md-time "> 3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                  <div class="p-6 flex flex-col justify-between ">
                                                                                    <p class="inline-flex items-center"></p>
                                                                                    <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm mb-2 f-m uppercase tracking-wid">Oga Madam Office </h3>
                                                                                    <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                    <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2 ion ion-md-time "> 3:00PM </h3>
                                                                                  </div>
                                                                                  <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                </div>
                                                                              </div>
                                                                              </div>
                                                                              </div>
                                                                              </div>
                                                                            </section>
                                                                            <section id="content2" class="tab-content">
                                                                              <div class="row">
                                                                                <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                                                  <div class="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }} class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-sm uppercase mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }} class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                      <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                        <div class="p-6 flex flex-col justify-between ">
                                                                                          <p class="inline-flex items-center"></p>
                                                                                          <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                          <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                          <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                        </div>
                                                                                        <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                      </div>
                                                                                    </div>
                                                                                    </div>
                                                                                    </div>
                                                                                    </div>
                                                                                  </section>
                                                                                  <section id="content3" class="tab-content">
                                                                                    <div class="row">
                                                                                      <div class="w-full w-1/2  m-auto mb-2 lg:mb-0">
                                                                                        <div class="container max-w-full  m-auto  flex flex-wrap items-center justify-start">
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}   class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          <div class="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3">
                                                                                            <div style={{ background: 'url(img/s.jpg)', backgroundSize: 'cover', height: '200px', boxShadow: 'inset 0px 42px 13px 319px #2f2f2f8a', backgroundPosition: 'center' }} class="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
                                                                                              <div class="p-6 flex flex-col justify-between ">
                                                                                                <p class="inline-flex items-center"></p>
                                                                                                <h3 style={{ borderBottom: '1px solid #fff', paddingBottom: '1em' }}  class="text-white text-xl mb-2 f-m ">Oga Madam Office </h3>
                                                                                                <span style={{ opacity: '.8' }} class="text-white">Weekdays</span>
                                                                                                <h3 style={{ fontSize: '2em' }}  class="text-white text-xl mb-2 f-m mt-2  ">3:00PM </h3>
                                                                                              </div>
                                                                                              <span style={{ opacity: '.8', paddingRight: '1em', float: 'right' }} class="text-white">#OgaMadam</span>
                                                                                            </div>
                                                                                          </div>
                                                                                          </div>
                                                                                          </div>
                                                                                          </div>
                                                                                        </section>
                                                                                      </div>
                                                                                      


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <Footer/>
      </div>
    );
  }
}

