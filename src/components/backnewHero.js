import React, { Component } from 'react';

import './herostyle.css'; 
import moment       from 'moment';
import settings from './config';
import HeroPreloader  from './preloaders/heroloader';

class Hero extends Component {
  
     constructor(props) {

            super(props);
           
            this.state = {
              posts: [],
              loading: true,
          };       
     }
   

    componentDidMount() {
        window.performance.now();
      fetch(settings.api_url + 'allposts')
        .then(response => response.json())
        .then(data =>  this.setState({ posts: data, loading: false }) );
    }
      
   
  render() {

     const slug_0 = this.state.posts[0] ? this.state.posts[0].slug : "#"
     const slug_1 = this.state.posts[1] ? this.state.posts[1].slug : "#"
     const slug_2 = this.state.posts[2] ? this.state.posts[2].slug : "#"
     const slug_3 = this.state.posts[3] ? this.state.posts[3].slug : "#"

     const posted_at_0 = this.state.posts[0] ? this.state.posts[0].created_at : "";
     const posted_at_1 = this.state.posts[1] ? this.state.posts[1].created_at : "";
     const posted_at_2 = this.state.posts[2] ? this.state.posts[2].created_at : "";
     const posted_at_3 = this.state.posts[3] ? this.state.posts[3].created_at : "";

     const timme_0 = moment(posted_at_0).fromNow();
     const timme_1 = moment(posted_at_1).fromNow();
     const timme_2 = moment(posted_at_2).fromNow();
     const timme_3 = moment(posted_at_3).fromNow();
       
 
    return (     
       <div>

       { this.state.loading ? 

          <HeroPreloader /> :
        
       <section  className="font-sans flex flex-col lg:flex-row mt-8">
          <div style={{background:`url(${this.state.posts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi"+this.state.posts[0].image : ""}) center center / cover`, height: '750px', boxShadow: 'rgba(47, 47, 47, 0.46) -173px -583px 56px -219px inset'}} className="boxx w-full lg:w-1/2">

            <div style={{marginTop: '19em'}} className=" p-6 hedd">
                 <p className="mb-4 inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  " >
                  <span className="font-sans uppercase text-white "> 

                    <a className='font-sans uppercase text-white' href={'shows/' + this.state.posts[0].category.name.toLowerCase().replace(/ /g, "-")}>
                    
                      {this.state.posts[0].category.name}
                    
                    </a>

                  </span>

                </p>
                <h1 className="text-3xl text-white leading-tight uppercase" style={{fontWeight: '100', fontFamily: 'Termina-Demi' }}>
                <a style={{color:'white'}} className="uppercase" href={"post/" + slug_0}>
                {this.state.posts[0] ? this.state.posts[0].title : ' '}
                </a>
</h1>
                                                  <p className=" text-white font-normal text-lg  time mt-4  " style={{fontFamily: 'FreightSansProMedium-Regular'}}>
                                                    <span style={{fontFamily: 'FreightSansProMedium-Regular'}}>
                                                      <span  className="ion ion-md-time"></span> {timme_0}
                                                                                              
                                                                            
                                                    </span>
                                                  </p>
                                                </div>

          </div>

          <div className="w-full lg:w-1/2 flex flex-col  justify-center text-left p-8 bg-white">
          <p  className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2 cattt-title " style={{fontFamily: 'Termina-Demi'}}>Latest</p>

          <div className="container max-w-xl m-auto flex flex-wrap items-center justify-start">

             <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col px-3  height-smt">
                <div className="overflow-hidden bg-white hover:shadow-raised hover:translateY-2px transition" style={{height: '610px'}}>
                  <div style={{background: `url(${this.state.posts[1] ? "https://ik.imagekit.io/hypb55a1e/ngi"+this.state.posts[1].image : ""}) 0% 0% / cover`, height: '250px'}}></div>
                  <div className="mt-4 flex flex-col justify-between ">
                    <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  " >
                      <span className="font-sans uppercase  text-green"> 

                      <a className='font-sans uppercase text-green' href={'shows/' + this.state.posts[1].category.name.toLowerCase().replace(/ /g, "-")}>
                    
                        {this.state.posts[1].category.name}
                  
                      </a>
                      </span>

                    </p>
                    <h3 className="text-black  text-m font-bold leading-normal mb-4">
                    
                    <a className='uppercase' style={{color:'black'}} href={"post/" + slug_1}>
                    {this.state.posts[1] ? this.state.posts[1].title : ' '}
                    </a>

                    </h3>
                    <p className=" text-black font-normal text-lg  time   ">
                      <span style={{opacity: '.4',fontFamily: 'FreightSansProMedium-Regular'}} >
                        <span  className="ion ion-md-time"></span> {timme_1} 
                                                          
                                        
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col px-3  height-smt">
                <div className="overflow-hidden bg-white hover:shadow-raised hover:translateY-2px transition" style={{height: '610px'}}>
                  <div style={{background: `url(${this.state.posts[2] ? "https://ik.imagekit.io/hypb55a1e/ngi"+this.state.posts[2].image : ""}) 0% 0% / cover`, height: '250px' }}></div>
                  <div className="mt-4 flex flex-col justify-between ">
                    <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  " >
                      <span className="font-sans uppercase text-green ">  <a className='font-sans uppercase text-green' href={'shows/' + this.state.posts[2].category.name.toLowerCase().replace(/ /g, "-")}>
                    
                    {this.state.posts[2].category.name}
              
                  </a></span>
                    </p>
                    <h3 className="text-black  text-m  font-bold leading-normal mb-4">
                    <a className='uppercase' style={{color:'black'}} href={"post/" + slug_2}>
                      {this.state.posts[2] ? this.state.posts[2].title : ' '}
                      </a>
                    </h3>
                    <p className=" text-black font-normal text-lg  time  ">
                      <span style={{opacity: '.4',fontFamily: 'FreightSansProMedium-Regular'}} >
                        <span  className="ion ion-md-time"></span> {timme_2} 
                                                          
                                        
                      </span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="w-full md:w-1/2 lg:w-1/3 flex flex-col px-3  height-smt">
                <div className="overflow-hidden bg-white hover:shadow-raised hover:translateY-2px transition" style={{height: '610px'}}>
                  <div style={{background: `url(${this.state.posts[3] ? "https://ik.imagekit.io/hypb55a1e/ngi"+this.state.posts[3].image : ""}) 0% 0% / cover`, height: '250px' }}></div>
                  <div className="mt-4 flex flex-col justify-between ">
                    <p className="inline-flex text-black font-sans text-sm tracking-wide  mb-2 uppercase  " >
                      <span className="font-sans uppercase  text-green"> 

                      <a className='font-sans uppercase text-green' href={'shows/' + this.state.posts[3].category.name.toLowerCase().replace(/ /g, "-")}>
                    
                    {this.state.posts[3].category.name}
              
                  </a>

                      </span>
                    </p>
                    <h3 className="text-black  text-m mb-2 font-bold leading-normal mb-4">
                      <a className='uppercase' style={{color:'black'}} href={"post/" + slug_3}>
                       {this.state.posts[3] ? this.state.posts[3].title : ' '}
                       </a>
                    </h3>
                    <p className=" text-black font-normal text-lg  time   ">
                      <span style={{ opacity: '.4',fontFamily: 'FreightSansProMedium-Regular'}} >
                        <span  className="ion ion-md-time"></span> {timme_3} 
                                                          
                                        
                      </span>
                    </p>
                  </div>
                </div>
              </div>
          </div>
          </div>
        </section>
      }
      </div>

    );
   
  }
  
}

export default Hero;

