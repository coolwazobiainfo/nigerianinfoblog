import React, { Component } from 'react';

import './herostyle.css';
import moment       from 'moment';
import settings from './config';
import HeroPreloader  from './preloaders/heroloader';

class Hero extends Component {
  
     constructor(props) {

            super(props);
           
            this.state = {
              posts: [],
              loading: true,
          };       
     }
   

    componentDidMount() {
        window.performance.now();
      fetch(settings.api_url + 'allposts')
        .then(response => response.json())
        .then(data =>  this.setState({ posts: data, loading: false }) );
    }
      
   
  render() {

     const slug = this.state.posts[0] ? this.state.posts[0].slug : "#"
     const posted_at = this.state.posts[0] ? this.state.posts[0].created_at : "";

     const timme = moment(posted_at).fromNow();
       
 
    return (      
       <div style={{height: "auto"}} className="font-sans container max-100 m-auto flex flex-col md:flex-row sm:items-center ">

        { this.state.loading ? 

          <HeroPreloader /> :
                    
          <div style={{background: `url(${this.state.posts[0] ? "https://ik.imagekit.io/hypb55a1e/ngi"+this.state.posts[0].image : ""}) 0% 0% / cover`,
           backgroundPosition: "center", height: "700px", backgroundSize: "100%", boxShadow: "inset 0px 42px 13px 354px #2f2f2f8a",   
          margin: "0 auto"}} className="w-full text-center flex flex-col justify-center items-start px-6 py-0 md:py-8 md:px-8 lg:items-start">

          <label style={{opacity: ".8", textAlign: "center"}} htmlFor="tagline" className="uppercase text-center mx-auto tracking-wide  text-white font-bold">

          <a style={{color:'white'}} href={"shows/" +  this.state.posts[0].category.name.toLowerCase().replace(/ /g, "-")}>

          {this.state.posts[0] ? this.state.posts[0].category.name : ""}        

          </a>                                       
         
          </label>
          <h1 style={{wordBreak: "break-word"}} className="mt-2 mb-4 uppercase tracking-wid  topik m-auto text-center text-white font-druk">{this.state.posts[0] ? this.state.posts[0].title : ""}</h1>
          <p className="inline-flex text-white font-normal text-lg text-center mx-auto time ">
            <span>
              <span className="ion ion-md-time"></span> {timme}                                                                    
            </span>
          </p>
           <p  style={{paddingLeft: "1em",paddingRight: "1em"}} className="inline-flex text-white font-bold text-lg text-center mx-auto mt-6 btn-fr ">
            <span>
           
              <span ><a href={"post/"+slug} style={{color: "white"}}> Read more</a></span>
           
            </span>
          </p>
        </div>
      }


      </div>

    );
   
  }
  
}

export default Hero;

