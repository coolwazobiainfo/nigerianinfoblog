import React, { Component } from 'react';
import settings from './config';
import './style.css';
import Search from './search';
import data from './../data';
import moment from 'moment';
import _ from 'lodash';
import { slug } from './../helpers';

class MenuInner extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      showSearchBar: false,
      schedule: '',
      location: localStorage.getItem('location'), 
    }

  }

  between = (start, end) => {
        let format = 'hh:mm:ss'
        // var time = moment() gives you current time. no format required.
        let time = moment(),
            beforeTime = moment(start, format),
            afterTime = moment(end, format);
  
        if (time.isBetween(beforeTime, afterTime)) {
          return 'NOW';
        } else if(beforeTime.isAfter(time)) {
  
          return 'Later';
  
        }else{

          return 'Ended';
          
        }
  }

  now_playing = (schedule) => {

    if (schedule[`${this.state.location}`]) {
            let myDate = new Date();
            let schedule_arry = schedule[`${this.state.location}`];
            if (myDate.getDay() === 6) {
              //saturday
              //filter by saturday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "saturday";
              });
    
            } else if (myDate.getDay() === 0) {
              //sunday
              //filter by sunday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "sunday";
              });
    
            } else {
              //otherdays
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period !== "sunday" && sch.period !== "saturday";
              });
            }
            for (let i = 0; i < schedule_arry.length; i++) {
              let schedu = schedule_arry[i];
              if (this.between(schedu.start, schedu.end) === "NOW") {
                return schedu;
              }
            }
         }
  }
  

  receiveChildValue = (showSearchBar) => {
    this.setState({ showSearchBar });
  };


  componentDidMount() {

      window.performance.now();
    
      fetch(settings.api_url + 'schedules')
        .then(response => response.json())
        .then(data => { let sch = _.groupBy(data, function(city) {
              return city.state;
            }); 

        this.setState({ schedule: this.now_playing(sch) }); }); 

    fetch(settings.api_url + 'allcategories') 
      .then(response => response.json())
      .then(data => this.setState({ categories: data }));

  }

  renderCategories() {
    let categories = this.state.categories;
    return categories.map((item, index) => {
      return (
        <li key={index}>
          <a href={"/shows/" + item.name.toLowerCase().replace(/ /g, "-")}>
            {item.name}
          </a>
        </li>
      )
    });
  }

    openradio = (e) =>  {
      e.preventDefault();
      window.open ("https://radio.nigeriainfo.fm","mywindow","menubar=1,resizable=1,width=550,height=950");
    }


  render() {

    // const { location } = this.props;

    // const homeClass = location.pathname === "/" ? "active" : "";
    // const aboutClass = location.pathname.match(/^\/about/) ? "active" : "";
    // const contactClass = location.pathname.match(/^\/contact/) ? "active" : "";
    
    return (

      <div>
      

      <header id="sp-header">
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-2">
                            <a href="/">
                                <img src={data.location[data.currentLocation].logo} alt="Nigeria Info Logo" style={{width: '100%'}} />
                               
                            </a>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-7">
                            <div className="sp-megamenu-wrapper" style={{color: '#000'}}>
                                <a id="offcanvas-toggler" className="visible-sm visible-xs" aria-label="Menu" href="#"><i className="zmdi zmdi-menu" aria-hidden="true" title="Menu"></i></a>
                                <ul className="sp-megamenu-parent menu-fade-up hidden-sm hidden-xs">
                                    <li className="sp-menu-item"><a href="/">Home</a></li>
                                    <li className="sp-menu-item"><a href="/" style={{ color: '#00b3ff' }}>News</a></li>
                                    <li className="sp-menu-item"><a href="/" style={{ color: '#642483' }}>Talk</a></li>
                                    <li className="sp-menu-item"><a href="/">Sport</a></li>
                                    <li className="sp-menu-item"><a href="/">Podcast</a></li>
                                    <li className="sp-menu-item"><a href="/">oaps</a></li>
                                    <li className="sp-menu-item"><a href="/">contact us</a></li>
                                    <li className="sp-menu-item"><a href="http://radio.nigeriainfo.fm/" target="_blank" style={{color: 'red'}}><span className="ion ion-md-wifi"> </span> Listen Live</a></li>
                                </ul>
                            </div>
                        </div> 

                        <div className="col-sm-3 col-md-3 hidden-sm hidden-xs">
                            <div className="sp-column sp-search">
                                                              
                                <div className="dropdown open dropdown-search">
                                    <a href="javascript:void(0)" data-toggle="dropdown"><i className="zmdi zmdi-search"></i></a>
                                    <form action="#" method="post" className="dropdown-menu dropdown-menu-right">
                                        <input type="text" name="searchword" placeholder="Search..." />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>  
                    
                </header>


      </div>

    );
  }
}

export default MenuInner;
