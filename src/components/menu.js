import React, { Component } from 'react';
import settings from './config';
import './style.css';
import Search from './search';
import data from './../data';
import moment from 'moment';
import _ from 'lodash';
import { slug } from './../helpers';
import Mobileswitch  from  './mobileswitch';

class Menu extends Component {

  constructor(props) {

    super(props);

    this.state = {
      categories: [],
      showSearchBar: false,
      schedule: '',
      location: localStorage.getItem('location'), 
      mobile: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.mobilePopUp = this.mobilePopUp.bind(this);

  }

  between = (start, end) => {
        let format = 'hh:mm:ss'
        // var time = moment() gives you current time. no format required.
        let time = moment(),
            beforeTime = moment(start, format),
            afterTime = moment(end, format);
  
        if (time.isBetween(beforeTime, afterTime)) {
          return 'NOW';
        } else if(beforeTime.isAfter(time)) {
  
          return 'Later';
  
        }else{

          return 'Ended';
          
        }
  }

  now_playing = (schedule) => {

    if (schedule[`${this.state.location}`]) {
            let myDate = new Date();
            let schedule_arry = schedule[`${this.state.location}`];
            if (myDate.getDay() === 6) {
              //saturday
              //filter by saturday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "saturday";
              });
    
            } else if (myDate.getDay() === 0) {
              //sunday
              //filter by sunday
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period === "sunday";
              });
    
            } else {
              //otherdays
              schedule_arry = _.filter(schedule_arry, (sch) => {
                return sch.period !== "sunday" && sch.period !== "saturday";
              });
            }
            for (let i = 0; i < schedule_arry.length; i++) {
              let schedu = schedule_arry[i];
              if (this.between(schedu.start, schedu.end) === "NOW") {
                return schedu;
              }
            }
         }
  }
  

  receiveChildValue = (showSearchBar) => {
    this.setState({ showSearchBar });
  };


  componentDidMount() {

      window.performance.now();
    
      fetch(settings.api_url + 'schedules')
        .then(response => response.json())
        .then(data => { let sch = _.groupBy(data, function(city) {
              return city.state;
            }); 

        this.setState({ schedule: this.now_playing(sch) }); }); 

    fetch(settings.api_url + 'allcategories') 
      .then(response => response.json())
      .then(data => this.setState({ categories: data }));

  }


    openradio = (e) =>  {
      // e.preventDefault();
      window.open ("https://radio.nigeriainfo.fm","mywindow","menubar=1,resizable=1,width=550,height=950");
    }

    handleChange(event) {
      this.setState({value: event.target.value});
    }
  
    handleSubmit(event) {
      alert('A name was submitted: ' + this.state.value);
      // event.preventDefault();
    }

    mobilePopUp(){

      // event.preventDefault();
      console.log(this.state.mobile);
      if(this.state.mobile){
        this.setState({mobile: false});
      }else{
        this.setState({mobile: true});
      }
  
      
    }

    // closePopUp(event){
    //   event.preventDefault();
    //   $('.off-canvas-menu-init').removeClass('offcanvas');
    // }

  render() {

    // const { location } = this.props;

    // const homeClass = location.pathname === "/" ? "active" : "";
    // const aboutClass = location.pathname.match(/^\/about/) ? "active" : "";
    // const contactClass = location.pathname.match(/^\/contact/) ? "active" : "";
    let className = 'offcanvas-menu';
    let iconClassName = 'zmdi zmdi-menu';
    if(this.state.mobile){
        className = '';
        iconClassName = 'zmdi zmdi-close-circle-o';
    }

    
    return (
      

      <div>
      

        <header id="sp-header">
                    <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-2">
                            <a href="/" className="">
                                <img src={data.location[data.currentLocation].logo} alt="Nigeria Info Logo" style={{width: '100%'}}  />
                               
                            </a>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-7">
                            <div className="sp-megamenu-wrapper" style={{color: '#000'}}>
                                <a id="offcanvas-toggler" onClick={this.mobilePopUp} className="visible-sm visible-xs" aria-label="Menu" href="#"><i className={iconClassName} aria-hidden="true" title="Menu"></i></a>
                                <ul className="sp-megamenu-parent menu-fade-up hidden-sm hidden-xs">
                                    <li className="sp-menu-item"><a href="/">Home</a></li>
                                    <li className="sp-menu-item"><a href={`/category/${slug('News','-',' ')}`} style={{ color: '#00b3ff' }}>News</a></li>
                                    <li className="sp-menu-item"><a href={`/category/${slug('Talk','-',' ')}`} style={{ color: '#642483' }}>Talk</a></li>
                                    <li className="sp-menu-item"><a href={`/category/${slug('sports','-',' ')}`}>Sport</a></li>
                                    <li className="sp-menu-item"><a href="/">Podcast</a></li>
                                    <li className="sp-menu-item"><a href="/oaps">oaps</a></li>
                                    <li className="sp-menu-item"><a href="/witness"> <span style={{background: '#ffd10b',  padding: '0.4em'}}> 
                                    <span className="ion ion-ios-videocam"></span> <span style={{ textTransform: 'lowercase'}}>i</span>Witness </span></a></li>
                                    <li className="sp-menu-item"><a href="/contact">contact us</a></li>
                                    <li className="sp-menu-item"><a href="http://radio.nigeriainfo.fm/" target="_blank" style={{color: 'red'}}><span className="ion ion-md-wifi"> </span> Listen Live</a></li>
                                </ul>
                            </div>
                        </div> 

                        <div className="col-sm-3 col-md-3 hidden-sm hidden-xs">
                            <div className="sp-column sp-search">
                                                              
                                <div className="dropdown open dropdown-search">
                                    <button href="javascript:void(0)" data-toggle="dropdown"><i className="zmdi zmdi-search"></i></button>
                                    <form action="#" method="post" className="dropdown-menu dropdown-menu-right">
                                        <input type="text" name="searchword" value={this.state.value} onChange={this.handleChange} placeholder="Search..." />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
      
                <Mobileswitch />
                </header>

                <div className={className} style={{border: 'solid', width: '70%',marginTop: '-25px',marginLeft: '31%', textDecoration: 'none'}}>
                  <a href="#" className="close-offcanvas" onClick={this.closePopUp}><i className="zmdi zmdi-close-circle-o"></i></a>
                  <div className="offcanvas-inner">
                      <div className="sppb-addon">
                          <ul className="sppb-menu">
                              <li><a href="/">Home</a></li>
                              <li><a href={`/category/${slug('News','-',' ')}`}>News</a></li>
                              <li><a href={`/category/${slug('Talk','-',' ')}`}>Talk</a></li>
                              <li><a href={`/category/${slug('sports','-',' ')}`}>Sport</a></li>
                              <li><a href="/witness">Witness</a></li>
                              <li><a href="/contact">Contact Us</a></li>
                              <li><a href="http://radio.nigeriainfo.fm/" target="_blank">Live</a></li>
                          
                          </ul>
                      </div>

                      <div className="sppb-addon-social">
                          <a href="https://themeforest.net/user/saihoai"><i className="zmdi zmdi-facebook"></i></a>
                          <a href="https://themeforest.net/user/saihoai"><i className="zmdi zmdi-twitter"></i></a>
                          <a href="https://themeforest.net/user/saihoai"><i className="zmdi zmdi-instagram"></i></a>
                          <a href="https://themeforest.net/user/saihoai"><i className="zmdi zmdi-pinterest"></i></a>
                          <a href="https://themeforest.net/user/saihoai"><i className="zmdi zmdi-youtube"></i></a>
                      </div>

                      <div className="copyright">&copy; Copyright 2019</div>

                  </div>
              </div>

      </div>

    );
  }
}

export default Menu;
