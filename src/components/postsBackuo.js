import React, { Component } from 'react';
import Ad from './ad';
import ReactGA from 'react-ga';
import Homemobileswitch from './homemobileswitch';
import settings from './config';
import  PostPreloader from './preloaders/postloader';

export default class Posts extends Component {

	constructor(props) {

		super(props);

		this.state = {
			posts: [],
			visible: 12,
			loading: true,
		};

		this.loadMore = this.loadMore.bind(this);
	}

	loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 3 };
		});
	}


	componentDidMount() {

			window.performance.now();
		
		fetch(settings.api_url+'allposts')
			.then(response => response.json())
			.then(data => this.setState({ posts: data, loading:false }));
	}


	handleClick() {

		ReactGA.initialize('UA-103305032-1');

		ReactGA.pageview(window.location.pathname);

		ReactGA.event({
			category: 'Navigation',
			action: 'Clicked Link',
		});
	}


	render() {
		
		return (


			<div style={{ margin: "0 auto" }} className="bg-white">
				<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center my-8">

					<div className="w-full max-100 m-auto mb-6 lg:mb-0">

						<p className="text-center uppercase p-2  text-black font-druk tracking-wid text-xl">NEWS</p>
						<p style={{ fontFamily: 'Cabin !important', fontSize: "1.1em" }} className="text-center font-bold text-grey-darker  mb-8">Read up on all latest on trending topics </p>

						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.loading ? 
								
								<PostPreloader />
								

							: this.state.posts.slice(0, this.state.visible).map((post, index) =>


								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3" key={index}>
									<div style={{ height: "620px" }} className="overflow-hidden bg-white rounded-lg shadow hover:shadow-raised hover:translateY-2px transition">
										<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image})`, height: "350px", backgroundSize: "cover" }}></div>

										<div className="p-6 flex flex-col justify-between">
											<p className="inline-flex text-blue font-bold text-sm mb-4 uppercase" style={{ marginTop: "-2.7em", fontFamily: "cabin" }}>
												<span className="font-bold text-gren but "><a href={"shows/" + post.category.name.toLowerCase().replace(/ /g, "-")} style={{ color: "white", fontFamily: "cabin"}}>{post.category.name}</a></span>
												{!post.review_score ?
													""
													: <span className="font-bold text-gren but-rw ">{post.review_score}</span>
												}
											</p>

											<div className="bg-white" style={{ padding: "1em", borderRadius: "7px" }}>
												<h3 className="text-black text-xl mb-2 font-bold leading-normal mb-4 "><a className="text-black" style={{ fontFamily: "cabin", fontWeight: "700" }} href={"post/" + (post.slug == null ? post.id : post.slug)}>{post.title}</a></h3>
												<p style={{ fontSize: "1em" }} className="text-grey-darker text-m mb-2 font-lora leading-normal">{post.excerpt}</p>
											</div>
										</div>
									</div>
								</div>
							)}
						</div>
						<div className="text-center">
							<div className="m-auto">
								{this.state.visible < this.state.posts.length &&
									<button onClick={this.loadMore} className="bg-black p-3 rounded-lg  w-100 font-bold  text-white uppercase tracking-wide">Load More</button>
								}
							</div>
						</div>
					</div>

					<Ad />

				</section>
				<Homemobileswitch />
			</div>
		);
	}
}

