import React, { Component } from 'react';

import Menu from './menu';
import Newsletter from './newsletter';
import settings from './config';
import HelmetMeta from './helmet';
import Footer from './footer';

class Digital extends Component {

	componentDidMount() {
      	window.performance.now();
    }


	render() {
		return (

			<div>
		        <HelmetMeta 
		          title="Nigeria Info FM | News, Talk & Sports Station!" 
		          description="Nigeria Info FM | News, Talk & Sports Station" 
		          canonical={settings.app_url}
		        />
		        <Menu />
		         <div role="main">
	             
	                <div className="region region-content">
	                  <article>
	                  	<div className="content">
	                      <div>
	                        <div className="field--item">
	                          <div>
	                          	<section style={{background:'url(img/dig.jpg)', boxShadow: 'rgba(47, 47, 47, 0.54) 0px 42px 13px 354px inset', backgroundSize: 'cover', backgroundPosition: 'center'}} className="hero-block">                                       
	                              <div className="container" >
	                                <div className="inner-line">
	                                  <div className="row">
	                                    <div className="text-center m-auto "> <p style={{fontSize: '3em'}} className="font-bold"> We are everything digital. </p><p style={{fontSize: '1.5em', width: '80%', margin:'0 auto'}} className="font-sans text-center leading-normal "> We are at the forefront of building the bridge between tradition radio and everything digital in Africa. We invite you to come with us on this adventure. </p> </div>
	                                  </div><div className="m-auto text-center"><button className="bg-white text-blue p-4 uppercase font-bold mt-8 rounded"><span className="ion-ios-chatboxes"></span> Let's talk</button></div>
	                                </div>
	                              </div>
	                            </section>
	                            <div className="field field--name-field-facts field--type-entity-reference-revisions field--label-hidden field--item">
                                  <section  className="paragraph facts-module module paragraph--type--paragraph-facts paragraph--view-mode--default">
                                    <div className="container">
                                      <div className="inner-line">
                                        <div className="row">
                                        	<div className="field field--name-field-fact field--type-entity-reference-revisions field--label-visually_hidden">                                               
	                                            <div className="field__items">
	                                              <div className="field--item">
	                                                <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
	                                                  <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
	                                                    <div className="row">
	                                                      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2 ">
	                                                       <span style={{fontSize: '2.5em',color: '#c1bfbf'}} className="ion-ios-cellular"></span>
	                                                        </div>
	                                                        <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
	                                                          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
	                                                            <p className="copy-cta">Cities where we have stations (in Nigeria)</p>
	                                                          </div>
	                                                          <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
	                                                            <span className="text-blue">4</span>
	                                                          </div>
	                                                        </div>
	                                                      </div>
	                                                    </div>
	                                                  </div>
	                                                </div>
	                                                <div className="field--item">
                                                      <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
                                                        <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
                                                          <div className="row">
                                                            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2">
                                                             <span style={{fontSize: '2.5em',color: '#c1bfbf'}} className="ion-ios-eye"></span>
                                                              </div>
                                                              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
                                                                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
                                                                  <p className="copy-cta">Visual radio audience across Periscope and Facebook (per day)</p>
                                                                </div>
                                                                <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
                                                                  <span className="text-blue">5000</span>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                      <div className="field--item">
                                                        <div  className="paragraph paragraph--type--fact paragraph--view-mode--default">
                                                          <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 fact">
                                                            <div className="row">
                                                              <div className="col-lg-12 col-md-12 col-sm-12 col-xs-2 ">
                                                               <span className="ion-ios-globe" style={{fontSize: '2.5em',color: '#c1bfbf'}} ></span>
                                                                </div>
                                                                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-10 content-container">
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-8 description">
                                                                    <p className="copy-cta">Total Digital Audience (in millions)</p>
                                                                  </div>
                                                                  <div className="col-lg-6 col-md-6 col-sm-12 col-xs-4 value text-center">
                                                                    <span className="text-blue">3.5M</span>
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                             <div className="m-auto text-center"><button className="bg-blue text-white p-4 uppercase font-bold mt-8 rounded"><span className="ion-ios-download"></span> Download kit</button></div>
                                            </section>
                                          </div>
                                        </div>
                                      </div>
                                     <br />
                                   	<section  className="font-sans flex flex-col lg:flex-row mt-8">
	                                  <div style={{background:'url(https://via.placeholder.com/600)', height: '700px', backgroundSize: 'cover', backgroundPosition: 'center'}} className="w-full lg:w-1/2"></div>
	                                  	<div style={{background:'#000'}} className="w-full lg:w-1/2 flex flex-col  justify-center text-left p-8 ">
                                			<p style={{marginLeft: '-6.5em', marginBottom: '-3em'}} className="text-left"><img width="400px" src="https://aux.ng/img/AUX%20LOGO_white.svg" /> </p>
                                               <p style={{fontSize: '2.5em', lineHeight: '1.2'}} className="leading-normal mb-4  mr-6 text-white font-bold">A platform to discover and become the new sound of Africa  </p>
                                                 <a target="new" href="https://aux.ng/">
                                					<p style={{fontSize: '1.3em', textDecoration: 'underline', color: '#fff'}} className="leading-normal mb-4 ">Visit site</p>
                              					</a>
                              			</div>
                                     </section>
                                     <section  className="font-sans flex flex-col lg:flex-row ">
                                                 <div className="w-full lg:w-1/2 flex flex-col items-center justify-center text-center p-8">
                          <h1 style={{fontSize: '1em', letterSpacing: '2px', fontFamily: 'Averta-Bold'}} className="my-4 font-normal"> LISTEN TO </h1>
                          <h1 className="my-4 font-normal">
                            <img style={{height: '100px'}} width="300px" src="https://aux.ng/img/auxradio_black.svg" />
                            </h1>
                            <p style={{textTransform: 'uppercase', letterSpacing: '2px', fontFamily: 'Averta-Bold'}} className="leading-normal mb-4  text-grey-darker">With AjAY SINCLAIR ON </p>
                            <p style={{textTransform: 'uppercase', letterSpacing: '2px', fontFamily: 'Averta-Bold'}} className="leading-normal mb-4  text-grey-darker">Saturdays on Cool FM  
                              
                              
                              <span style={{opacity: '.4'}}>|</span> 12PM - 1PM  
                            
                            
                            </p>

                            <p className="leading-normal mb-4 w-2/3 text-grey-darker">
                              <img width="150px" src="https://aux.ng/img/cool.png" />
                              </p>
                              <a target="new" href="https://soundcloud.com/auxng">
                                <p style={{fontSize: '1.3em', textDecoration: 'underline', color: '#fb6518'}} className="leading-normal mb-4 ">Listen on SoundCloud
               
                                </p>
                              </a>

                            </div>
                            <div style={{background:'url(https://via.placeholder.com/600)', height: '700px', backgroundSize: 'cover', backgroundPosition: 'center'}} className="w-full lg:w-1/2"></div>
                                                 
                        </section>
                        <section>
                          <div className="region region-content">
                           
                              <span property="schema:name" content="Careers" className="hidden"></span>
                              <div className="content">
                                <div>
                                  <div className="field--item">
                                    <div>
                                      <section style={{background: 'url(img/lens.jpg)', boxShadow: 'rgba(0, 79, 183, 0.69) 0px 42px 13px 354px inset', backgroundSize: 'cover'}} className="hero-block">
                                      
                                        <div className="container " >
                                          <div className="inner-line">
                                            <div className="row">
                                              <div className="text-center m-auto "> <p style={{fontSize: '3em'}} className="font-bold"> Visual Radio </p><p style={{fontSize: '1.5em', width: '80%', margin:'0 auto'}} className="font-sans text-center leading-normal ">A Visual Radio allows listeners the opportunity to watch their favourite local FM radio presenters in action on computers and mobile devices. This is a new form of radio broadcasting we pioneered in the Nigerian digitial landscape. </p> </div>
                                            </div><div className="m-auto text-center"><button className="bg-white text-blue p-4 uppercase font-bold mt-8 rounded"><span style={{fontSize: '1.3em',  verticalAlign: 'middle'}} className="ion-ios-mail"></span> Advertise on VR</button> <button className="bg-white text-blue p-4 uppercase font-bold mt-8 rounded">  Download rates <span style={{fontSize: '1.3em', verticalAlign: 'middle'}} className="ion-ios-download"></span> </button></div>
                                          </div>
                                        </div>
                                      </section>
                                      <section  className="font-sans flex flex-col lg:flex-row ">
                                        <div style={{background:'url(https://via.placeholder.com/600)', height: '700px', backgroundSize: 'cover', backgroundPosition: 'center'}} className="w-full lg:w-1/2"></div>
                                        <div style={{background:'url(img/frzz.jpg)', backgroundSize: 'cover'}} className="w-full lg:w-1/2 flex flex-col  justify-center text-left p-8 ">
                                    
                                        </div>
                                      </section>
                                      <section>
                                        <div className="region region-content">
                                          <article >
                                      
                                            <div className="content">
                                              <div>
                                                <div className="field--item">
                                                  <div>
                                                    <section style={{background:'url(img/hire.jpg)', backgroundSize: 'cover', backgroundPosition: 'center',  boxShadow: 'rgba(0, 79, 183, 0.69) 0px 42px 13px 354px inset'}} className="hero-block">
                                        
                                                      <div className="container " >
                                                        <div className="inner-line">
                                                          <div className="row">
                                                            <div className="text-center m-auto "> <p style={{fontSize: '3em'}} className="font-bold"> Join the pack </p><p style={{fontSize: '1.5em', width: '80%', margin:'0 auto'}} className="font-sans text-center leading-normal "> We are always on the lookout for new additions to our pack. Do you believe you are innovative, creative and crazy enough to join our digital team ? </p> </div>
                                                          </div><div className="m-auto text-center"><button className="bg-white text-blue p-4 uppercase font-bold mt-8 rounded"><span className="ion-ios-chatboxes"></span> Let's talk</button></div>
                                                        </div>
                                                      </div>
                                                    </section>

                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              </article>
                                            </div>
                                            </section>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                           
                          </section>
                                                                         
                        </div>
</div>
</article>
</div>
</div>
	        </div> 
		)
	}


}

export default Digital;