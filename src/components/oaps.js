import React, { Component } from 'react';
import Menu       from './menu';
import settings from './config';
import HelmetMeta from './helmet';
import  CategoryPreloader  from './preloaders/categoryloader';
import { slug } from './../helpers';

export default class Oaps extends Component {

    constructor(props){
        super(props);

        this.state = {
            location: localStorage.getItem('location'),
            presenters: []
        }
    }

    componentDidMount() {
            window.performance.now();

            fetch(settings.api_url + 'oaps/')
            .then(response => response.json())
            .then(data =>  this.setState({ presenters: data }));
        }
    render() {
        return (  
            <div className="body-wrapper">
                <div className="body-innerwrapper">

                <HelmetMeta
                    title="Nigeria Info FM | News, Talk & Sports Station"
                    description="Nigeria Info FM | News, Talk & Sports Station" 
                    canonical={settings.app_url+`oaps`}
                />
                <Menu />
               
                <div id="latest" className="sppb-section">
			<div className="container">
				<div className="sppb-section-title">
					<h4 style={{ background: '#652483', textAlign: 'center', color: 'white', borderRadius: '4px 1px 11px 0px', paddingLeft: '.8em' }}><strong>{this.state.catname}</strong> </h4>
					<p className="sppb-title-subheading"><a href="#">View all</a></p>
				</div>
				
			{this.state.loading ?

				<CategoryPreloader /> :

				<div className="row">
                    <div className="col-md-full">

				{ this.state.presenters.map((presenter, index) => 
                       
					<div className="col-md-3 col-sm-6" key={index}>
						<div className="sppb-addon-feature mb30-sm">
							
							<img src={`https://ik.imagekit.io/hypb55a1e/ngi/${presenter.image}`} alt={presenter.name} className="img-responsive post-thumb" />
							<div className="sppb-addon-content">
								<a href={`${"/oap/"+slug(presenter.name)}`}><h4 className="sppb-addon-title">{presenter.name}</h4></a>
								<div className="meta">									
									{/* <span className="date">{moment(post.created_at).fromNow()}</span> */}
								</div>
							</div>
						</div>
					</div>
			
			   )}
			   		</div>
				</div>  

			}
			</div>				
		</div>


            </div>
              
        </div>
        )
    }
}