import React, { Component } from 'react';
import moment       from 'moment';
import settings from './config';
import './style.css';
import MenuInner from './menu-inner';
import HelmetMeta from './helmet';
import ReactHtmlParser from 'react-html-parser';
import  CategoryPreloader  from './preloaders/categoryloader';
import { slug } from './../helpers';

export default class Post extends Component {

    
      constructor(props) {
           
            super(props);

		    this.state = {
		      post:     [],
              loading: true,
              catname: '',
              cats: [],
              visible: 3
		    };
		    
       }



	  componentDidMount( ) {

	  	window.performance.now();

		const { id } = this.props.match.params;
        let catid = "";
        let old_slug = "";
               		
	    fetch(settings.api_url+'posts/' + id)
	      .then(response => response.json())
	      .then((data) => {

            catid = data.category.id;
            old_slug = data.slug;

            this.setState({ post: data })
             
          }).then(()=>{

                fetch(settings.api_url + 'category/' + catid)
                .then(response => response.json())
                .then(data => { 
                   
                    data.filter((el,index)=>{
                        if(el.slug === old_slug){
                            data.splice(index,1);
                        }
                    })

                    this.setState({ cats: data, loading: false })
                  
                }
            );

          });
          
         
      }

    render() {

        const { post } = this.state;        
        
        const cat_id =  post.category ? post.category.id : post.id 

        const timme = moment(post.created_at).fromNow();
        const category_name = post.category ? post.category.name.toLowerCase().replace(/ /g, "-") : '';
       

        return ( 

			<div className="sticky-header home off-canvas-menu-init">
					<HelmetMeta 
							title={ post.category ? "Nigeria Info FM | " + post.category.name + " - " + post.title : "Nigeria Info FM | News, Talk & Sports Station - Post" }   
							description={post.excerpt} 
							image={ post.image } 
							canonical={settings.app_url+`post/`+post.slug}
						/>   
	   <div className="body-wrapper">
            <div className="body-innerwrapper">

			
			    <MenuInner /> 
			{

            }
			
				<div id="sp-main-body">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">

                                <article className="sppb-addon-feature icon-mid">
                                    <h2 className="sppb-addon-title h1">{post.title}</h2>
                                    <div className="meta">
                                       
                                        <span className="date">{timme} </span>
                                        <span className="cate">&nbsp; in <a href="#">{category_name}</a></span>
                                    </div>

                                    <a href="#">
                                        <div className="sppb-image">
                                            <img src={`https://ik.imagekit.io/hypb55a1e/ngi${post.image}`} alt={post.title} className="img-responsive post-thumb img-fitw" />
                                            <div className="caption">{post.excerpt}</div>
                                        </div>
                                    </a>

                                    <div className="sppb-addon-content">
                                        <div className="sppb-addon-text">
                                           
												{ ReactHtmlParser(post.content) }
                                          
                                          
                                        </div>
                                        
                                    </div> 

                                   

                                </article>
                                
                          

                            </div>

                        </div>

                        <div id="related" className="sppb-section">
                    <div className="sppb-section-title">
                        <h4>You May Also <strong>Like</strong></h4>
                    </div>

                    <div className="row">

                    {this.state.loading ?

                    <CategoryPreloader /> :
                            <div className="row">
                                <div className="col-md-full">

                        { this.state.cats.slice(0, this.state.visible).map((post, index) => 
                                                 

                                <div className="col-md-4 col-sm-4">
                                    <div className="sppb-addon-feature mb30-xs wow fadeInRight">
                                        <a href={`${post.slug}`}>   
                                            <img src={`${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image}`} alt="" className="img-responsive post-thumb img-fitw" />
                                        </a>
                                        <div className="sppb-addon-content">
                                            <a href={`${post.slug}`}><h4 className="sppb-addon-title">{post.title}</h4></a>
                                            <div className="meta">                                                
                                                <span className="date">{moment(post.created_at).fromNow()}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>   

                          )}  
                        
                            </div>
                        </div>
                        }
                       
                    </div>   
                </div>

                    </div>




                </div>
			 
	       </div>
</div>
</div>
        );
    }
}


