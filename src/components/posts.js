import React, { Component } from 'react';
import Ad from './ad';
import ReactGA from 'react-ga';
import Homemobileswitch from './homemobileswitch';
import settings from './config';
import  PostPreloader from './preloaders/postloader';

export default class Posts extends Component {

	constructor(props) {

		super(props);

		this.state = {
			posts: [],
			visible: 12,
			loading: true,
		};

		this.loadMore = this.loadMore.bind(this);
	}

	loadMore() {
		this.setState((prev) => {
			return { visible: prev.visible + 3 };
		});
	}


	componentDidMount() {

			window.performance.now();
		
		fetch(settings.api_url+'allposts')
			.then(response => response.json())
			.then(data => this.setState({ posts: data, loading:false }));
	}


	handleClick() {

		ReactGA.initialize('UA-103305032-1');

		ReactGA.pageview(window.location.pathname);

		ReactGA.event({
			category: 'Navigation',
			action: 'Clicked Link',
		});
	}


	render() {
		
		return (


			<div style={{ margin: "0 auto" }} className="bg-white">
				<section className="font-sans container max-100 m-auto flex flex-col lg:flex-row justify-center">

					<div className="w-full  max-100 m-auto mb-4 lg:mb-0">

						<p className="text-left uppercase p-2 ml-2  text-black font-druk  text-xl mb-4 mt-2 cattt-title ">LOCAL  NEWS</p>
					

						<div className="container max-100 m-auto  flex flex-wrap items-center justify-start">
							{ this.state.loading ? 
								
								<PostPreloader />
								

							: this.state.posts.slice(0, this.state.visible).map((post, index) =>


								<div className="w-full md:w-1/2 lg:w-1/3 flex flex-col mb-8 px-3 height-s" key={index}>
									<div className="overflow-hidden bg-white news-bottom shadow hover:shadow-raised hover:translateY-2px transition">
										<div style={{ background: `url(${"https://ik.imagekit.io/hypb55a1e/ngi" + post.image})`, height: "350px", backgroundSize: "cover" }}></div>

										<div className="p-6 flex flex-col justify-between">
											<p className="inline-flex text-black font-bold text-sm tracking-wide  mb-4 uppercase">
												<span className="font-bold text-green uppercase"><a href={"category/" + post.category.name.toLowerCase().replace(/ /g, "-")} style={{ color: "white", fontFamily: "cabin"}}>{post.category.name}</a></span>
												{!post.review_score ?
													""
													: <span className="font-bold text-gren but-rw ">{post.review_score}</span>
												}
											</p>

											
												<h3 className="text-black  text-xl mb-2 font-bold leading-normal mb-4"><a className="text-black" href={"post/" + (post.slug == null ? post.id : post.slug)}>{post.title}</a></h3>
												<p style={{ fontSize: "1em" }} className="text-grey-darker text-m mb-2 font-lora leading-normal">{post.excerpt}</p>

												<p className="text-black font-normal text-lg  time mt-2">
                                                                            <span style={{opacity: '.4'}} >
                                                                              <span  className="ion ion-md-time"></span>2hrs ago 
                                                                                              
                                                                            </span>
                                                                          </p>
											
										</div>
									</div>
								</div>
							)}
						</div>
						<div className="text-center">
							<div className="m-auto">
								{this.state.visible < this.state.posts.length &&
									<button onClick={this.loadMore} className="bg-black p-3 rounded-lg  w-100 font-bold  text-white uppercase tracking-wide">Load More</button>
								}
							</div>
						</div>
					</div>

					<Ad />

				</section>
				<Homemobileswitch />
			</div>
		);
	}
}

