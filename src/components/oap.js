import React, { Component } from 'react';
import Menu       from './menu';
import Footer     from './footer';
import HelmetMeta from './helmet';
import settings from './config';
import ReactHtmlParser from 'react-html-parser';


export default class Oaps extends Component {
    constructor(props) {
        super(props);

        this.state = {
            presenter: [],
            loading: true
        }

    }

    componentDidMount() {

        window.performance.now();
       
        const { id } = this.props.match.params; 
          
	    fetch(settings.api_url + 'thisoap/' + id)
	      .then(response => response.json())
	      .then(data => {
            this.setState({ presenter: data })
            console.log(data);
          }  );	    
     }
     
    //  createDescription() {
    //      return {__html: `${this.state.presenter.description}`}
    //  }

    //  showDescription() {
    //      return <div dangerouslySetInnerHTML={this.createDescription()} />
    //  }

    render() {
        const { presenter } = this.state;

        // const name = (string) => {
        //     return string.toLowerCase();
        // }

        return (
            <div className="sticky-header home off-canvas-menu-init">

                <HelmetMeta 
                    title="Nigeria Info FM | News, Talk & Sports Station! - OAP" 
                    description="Nigeria Info FM | News, Talk & Sports Station" 
                    canonical={settings.app_url+`oap/`+`${presenter.fullname}`}
                />   
               
                <div className="body-wrapper">
                    <div className="body-innerwrapper">

                        <Menu />

                <div id="sp-main-body">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-12">

                                <article className="sppb-addon-feature icon-mid">
                                    <h2 className="sppb-addon-title h1">{presenter.fullname}</h2>
                                 
                                    <a href="#">
                                        <div className="sppb-image">
                                            <img src={`https://ik.imagekit.io/hypb55a1e/ngi${presenter.image}`} alt={presenter.fullname} className="img-responsive post-thumb img-fitw" />
                                            {/* <div className="caption">{presenter.description}</div> */}
                                        </div>
                                    </a>

                                    <div className="sppb-addon-content">
                                        <div className="sppb-addon-text">
                                           
												{ ReactHtmlParser(presenter.description) }
                                          
                                          
                                        </div>
                                        
                                    </div> 

                                   

                                </article>
                          

                            </div>

                        </div>
                    </div>
                </div>

                    </div>
                </div>
               
                    

         
            </div>
        );
    }
}