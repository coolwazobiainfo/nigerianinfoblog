import React, { Component } from 'react';
//App specific components

import Menu from './menu';

import Mainscheduler from './mainscheduler';
import Showschedule from './showschedule';
import settings from './config';
import HelmetMeta from './helmet';


import Footer from './footer';

class Schedules extends Component {

componentDidMount() {
      window.performance.now();
    }

  render() {
    console.log(this.props)
    return (
      <div>
        <HelmetMeta 
          title="Nigeria Info FM | News, Talk & Sports Station!" 
          description="Nigeria Info FM | News, Talk & Sports Station" 
          canonical={settings.app_url+`schedules`}
        />
        <Menu />
        
        <Mainscheduler />
        <Showschedule />
        <Footer />
      </div>
    );
  }
}

export default Schedules;
