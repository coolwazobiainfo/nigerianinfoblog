import React, { Component } from 'react';
import ReactGA from 'react-ga';
import axios from 'axios';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
//App specific components
import Main from './components/main';
import Post from './components/post';
import Category from './components/category';
import About from './components/about';
import Contact from './components/contact';
import Witness from './components/witness';
import NotFound from './components/404';
import Music from './components/music';
import Oaps from './components/oaps';
import Oap from './components/oap.js';
import settings from './components/config';
import Maintenance from './components/maintenance';
import Locations from  './components/locations';
import Schedules from './components/schedules';
import Shows from './components/shows';
import Electionupdate from './components/electionupdate';
import Company from './components/company';
import Digital from './components/digital';
import NewHome from './components/NewHome';
// import Local from './components/local';


function fireTracking() {
	ReactGA.initialize('UA-103305032-1');

	ReactGA.pageview(window.location.hash);
}



class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading: 1,
      onMaintenance: false,
    }
  }
  componentWillMount(){

      this.setState({loading: 2})
      axios.get(settings.api_url+'maintenance')
        .then(res => {
          this.setState({loading: 3, onMaintenance: res.data});
 
        })
        .catch(err => {
          console.log(err.response);
          this.setState({loading: 3});
        });
  }

  renderComponent(){
    if(this.state.loading < 3){
      return (<div></div>);
    }else{
      if(this.state.onMaintenance){
        return <Maintenance/>
      }else{
        return (
          <Router onUpdate={fireTracking}>
            <Switch>
              <Route exact path='/' component={NewHome}/>
              <Route exact path='/new-home' component={NewHome}/>
              <Route exact path='/post/:id' component={Post}/>
              <Route exact path='/category/:id' component={Category}/>	    		    
              <Route exact path='/contact' component={Contact}/>
              <Route exact path='/witness' component={Witness}/>
              <Route exact path='/about' component={About}/>
              <Route exact path='/oaps' component={Oaps}/>
              <Route exact path='/oap/:id' component={Oap}/>
              <Route exact path='/music/submit' component={Music} />
              <Route exact path='/locations' component={Locations} />
              <Route exact path='/schedules' component={Schedules} />
              <Route exact path='/shows' component={Shows} />
              <Route exact path='/company' component={Company} />
              <Route exact path='/digital' component={Digital} />
              <Route exact path='/electionupdate' component={Electionupdate} />
              <Route exact path={'*'} component={NotFound} />
          </Switch>
        </Router>
       
        );
      }
    }
  }

  render() {
   
    return (
      <div>
        {this.renderComponent()}
      </div>
       );
  }
}

export default App;
