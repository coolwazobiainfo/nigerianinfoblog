import _ from 'lodash';

export async function getcategory(id) {

   
    let response = await fetch('https://office.coolfm.ng/bcategory/' + id);
    let data = await response.json();

    let  c =  _.filter(data, function(num){ return num.name; });


     // _.groupBy(data, function(city) {
     //          return city.name;
     //       });

    return c;
   

}

export const slug = (string, target = ' ', replacement = '-') => {
  const pattern = new RegExp(`[${target}]+`, 'gi');

  return string.toLowerCase().replace(pattern, replacement);
}
