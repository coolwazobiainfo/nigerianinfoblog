import express from "express";
import path from "path";

import React from "react";
import { renderToString } from "react-dom/server";
import Layout from "./App";

const app = express();

app.use( express.static( path.resolve( __dirname, "../build" ) ) );

app.get( "/*", ( req, res ) => {
    const jsx = ( <Layout /> );
    const reactDom = renderToString( jsx );

    res.writeHead( 200, { "Content-Type": "text/html" } );
    res.end( htmlTemplate( reactDom ) );
} );

app.listen( 2048 );

function htmlTemplate( reactDom ) {
    return `
    <!DOCTYPE html>
    <html lang="en">
    
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
      <!-- <link rel="icon" href="./img/Favicon.png" type="image/png" sizes="16x16"> -->
    
      <meta name="keywords" content="music, videos, blogs, writing, latest-news, radio, entertainment, best-music, aux, best talk, celebrity-interviews, coolfm, hitmusic, news, breaking-news etc.">
      <meta name="description" content="Nigeria Info FM | News, Talk & Sports Station">
      <meta name="og:title" property="og:title" content="Nigeria Info FM | News, Talk & Sports Station">
      <meta property="og:image" content="https://res.cloudinary.com/xyluz/image/upload/v1550096120/nir_whyowr.jpg">
      <meta name="robots" content="index, follow">
    
      <!--
          manifest.json provides metadata used when your web app is added to the
          homescreen on Android. See https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/
        -->
       <script src="https://go.arena.im/public/js/arenalib.js?p=nigerian-info-fm&e=1550254865783"></script>
    
        
        <link href="https://fonts.googleapis.com/css?family=Orbitron:700,900" rel="stylesheet">
      <link rel="manifest" href="%PUBLIC_URL%/manifest.json">
    
      <link rel="stylesheet" href="https://cdn.plyr.io/3.3.21/plyr.css">
      
      <link rel="stylesheet" href="https://ionicons.com/css/ionicons.min.css">
    
      <link rel="stylesheet" type="text/css" href="/style.css" />
      <link rel="stylesheet" type="text/css" href="/css/menu.css" />
      <link rel="stylesheet" type="text/css" href="/css/company.css" />
    
      <link rel="stylesheet" href="/font/FreightSansPro/Freight.css">
    
      <link href="https://fonts.googleapis.com/css?family=Lusitana" rel="stylesheet">
      <link rel="stylesheet" href="/MaterialDesign-Webfont-master/css/materialdesignicons.css">
    
    
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-103305032-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
    
        gtag('config', 'UA-103305032-1');
      </script>
    
      <title>Nigeria Info FM | News, Talk & Sports Station</title>
    </head>
    
    <body>
    
      <noscript>
        You need to enable JavaScript to run this app.
      </noscript>
            <div id="app">${ reactDom }</div>
            <script src="./app.bundle.js"></script>
            <script>
            let disqus_config = function () {
              this.page.url = window.location.href;
              this.page.identifier = Math.floor((Math.random() * 100) + 1);
            };
        
            (function () {
        
              let d = document, s = d.createElement('script');
              s.src = 'https://http-nigeriainfo-fm.disqus.com/embed.js';
              s.setAttribute('data-timestamp', +new Date());
              (d.head || d.body).appendChild(s);
        
            })();
          </script>
        
          <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
          <script src="/js/main.js"></script> <!-- Resource jQuery -->
          <script src="/js/modernizr-custom.js"></script>
        
          <script src="/js/classie.js"></script> 
          
          <script src="/js/highlight.js"></script>
          <script src="/js/app.js"></script>
        
          <script src='https://storage.googleapis.com/code.getmdl.io/1.1.0/material.min.js'></script>
          
          <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
         
          <script src="/js/preheader.js"></script>
          <script>
            var OneSignal = window.OneSignal || [];
            OneSignal.push(function () {
              OneSignal.init({
                appId: "69463998-1615-4d91-88fa-a2e81192f083",
              });
            });
          </script>
        
        </body>
        
        
        </html>
    `;
}