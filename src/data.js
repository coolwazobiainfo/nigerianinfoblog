
let currentLocation = localStorage.getItem('location');
if(!currentLocation){
    localStorage.setItem('location', 'Lagos');
}
currentLocation = localStorage.getItem('location');

let data = {
    currentLocation,
    location:{
        Abuja: {
            backdrop: 'https://res.cloudinary.com/xyluz/image/upload/v1550732435/abuja_f3qlag.jpg',
            socials: {
                facebook: 'https://web.facebook.com/NigeriaInfoAbuja', 
                twitter: 'https://twitter.com/nigeriainfoabj', 
                instagram: 'https://www.instagram.com/nigeriainfofmlagos/', 
                youtube: 'https://www.youtube.com/user/NigeriaInfoFM', 
                chat: ''
            },
            logo: 'https://res.cloudinary.com/xyluz/image/upload/v1561125008/logo-n_t9asri.svg'
        },

        Lagos: {
            backdrop: 'https://res.cloudinary.com/xyluz/image/upload/v1561113037/banner_g12ieo.jpg',
            socials: {
                facebook: 'https://web.facebook.com/NigeriaInfoFM', 
                twitter: 'https://twitter.com/nigeriainfofm', 
                instagram: 'https://www.instagram.com/nigeriainfofmlagos/', 
                youtube: 'https://www.youtube.com/user/NigeriaInfoFM', 
                chat: ''
            },
            logo: 'https://res.cloudinary.com/xyluz/image/upload/v1561125008/logo-n_t9asri.svg'
        },

        Ph:{
            backdrop: 'https://res.cloudinary.com/xyluz/image/upload/v1550732430/ph_auplqy.jpg',
            socials: {
                facebook: 'https://www.facebook.com/NigeriaInfoPH/', 
                twitter: 'https://twitter.com/nigeriainfoph', 
                instagram: 'https://www.instagram.com/nigeriainfofmlagos/', 
                youtube: 'https://www.youtube.com/user/NigeriaInfoFM', 
                chat: ''
            },
            logo: 'https://res.cloudinary.com/xyluz/image/upload/v1561125008/logo-n_t9asri.svg'
        },
        Kano:{
            backdrop: 'https://res.cloudinary.com/xyluz/image/upload/v1549323096/brrrrrrr_xh6qvo.jpg',
            socials: {
                facebook: 'https://web.facebook.com/coolfmnigeria', 
                twitter: 'https://twitter.com/CoolFMNigeria', 
                instagram: 'https://www.instagram.com/coolfmlagos/', 
                youtube: 'https://www.youtube.com/user/NigeriaInfoFM', 
                chat: ''
            },
            logo: 'https://res.cloudinary.com/xyluz/image/upload/v1561125008/logo-n_t9asri.svg'
        }
    }
}



export default data;